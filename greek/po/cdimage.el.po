# Emmanuel Galatoulas <galas@tee.gr>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Debian webwml organization\n"
"PO-Revision-Date: 2019-07-14 01:51+0200\n"
"Last-Translator: Emmanuel Galatoulas <galas@tee.gr>\n"
"Language-Team: Greek <debian-www@lists.debian.org>\n"
"Language: el\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Lokalize 2.0\n"

#. Translators: string printed by gpg --fingerprint in your language, including spaces
#: ../../english/CD/CD-keys.data:4 ../../english/CD/CD-keys.data:8
#: ../../english/CD/CD-keys.data:12
msgid "      Key fingerprint"
msgstr "      Key fingerprint"

#: ../../english/devel/debian-installer/images.data:89
msgid "ISO images"
msgstr "εικόνες ISO"

#: ../../english/devel/debian-installer/images.data:90
msgid "Jigdo files"
msgstr "Αρχεία Jigdo"

#. note: only change the sep(arator) if it's not good for your charset
#: ../../english/template/debian/cdimage.wml:10
msgid "&middot;"
msgstr "&middot;"

#: ../../english/template/debian/cdimage.wml:13
msgid "<void id=\"dc_faq\" />FAQ"
msgstr "<void id=\"dc_faq\" />Συχνές Ερωτήσεις"

#: ../../english/template/debian/cdimage.wml:16
msgid "Download with Jigdo"
msgstr "Μεταφόρτωση με Jigdo"

#: ../../english/template/debian/cdimage.wml:19
msgid "Download via HTTP/FTP"
msgstr "λήψη μέσω HTTP/FTP"

#: ../../english/template/debian/cdimage.wml:22
msgid "Buy CDs or DVDs"
msgstr "Αγορά CD ή DVD"

#: ../../english/template/debian/cdimage.wml:25
msgid "Network Install"
msgstr "Δικτυακή Eγκατάσταση"

#: ../../english/template/debian/cdimage.wml:28
msgid "<void id=\"dc_download\" />Download"
msgstr "<void id=\"dc_download\" />Μεταφόρτωση"

#: ../../english/template/debian/cdimage.wml:31
msgid "<void id=\"dc_misc\" />Misc"
msgstr "<void id=\"dc_misc\" />Διάφορα"

#: ../../english/template/debian/cdimage.wml:34
msgid "<void id=\"dc_artwork\" />Artwork"
msgstr "<void id=\"dc_artwork\" />Καλιτεχνικά"

#: ../../english/template/debian/cdimage.wml:37
msgid "<void id=\"dc_mirroring\" />Mirroring"
msgstr "<void id=\"dc_mirroring\" />Καθρεπτισμοί"

#: ../../english/template/debian/cdimage.wml:40
msgid "<void id=\"dc_rsyncmirrors\" />Rsync Mirrors"
msgstr "<void id=\"dc_rsyncmirrors\" />Καθρεπτισμοί Rsync"

#: ../../english/template/debian/cdimage.wml:43
msgid "<void id=\"dc_verify\" />Verify"
msgstr "<void id=\"dc_verify\" />Επαληθεύστε"

#: ../../english/template/debian/cdimage.wml:46
msgid "<void id=\"dc_torrent\" />Download with Torrent"
msgstr "<void id=\"dc_torrent\" />Μεταφόρτωση με Torrent"

#: ../../english/template/debian/cdimage.wml:49
msgid "Debian CD team"
msgstr "Ομάδα CD του Debian"

#: ../../english/template/debian/cdimage.wml:52
msgid "debian_on_cd"
msgstr "debian_σε_cd"

#: ../../english/template/debian/cdimage.wml:55
msgid "<void id=\"faq-bottom\" />faq"
msgstr "<void id=\"faq-bottom\" />συχνές ερωτήσεις"

#: ../../english/template/debian/cdimage.wml:58
msgid "jigdo"
msgstr "jigdo"

#: ../../english/template/debian/cdimage.wml:61
msgid "http_ftp"
msgstr "http_ftp"

#: ../../english/template/debian/cdimage.wml:64
msgid "buy"
msgstr "αγορά"

#: ../../english/template/debian/cdimage.wml:67
msgid "net_install"
msgstr "δικτυακή_εγκατάσταση"

#: ../../english/template/debian/cdimage.wml:70
msgid "<void id=\"misc-bottom\" />misc"
msgstr "d id=\"misc-bottom\" />διάφορα"

#: ../../english/template/debian/cdimage.wml:73
msgid ""
"English-language <a href=\"/MailingLists/disclaimer\">public mailing list</"
"a> for CDs/DVDs:"
msgstr ""
"<a href=\"/MailingLists/disclaimer\">Δημόσια λίστα αλληλογραφίας</a> στα "
"Αγγλικά για CD/DVD:"

#~ msgid "<void id=\"dc_relinfo\" />Image Release Info"
#~ msgstr "<void id=\"dc_relinfo\" />Πληροφορίες έκδοσης της εικόνας"
