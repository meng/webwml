#use wml::debian::template title="Documentazione" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="0352601c5e0d6e960ffa0c05a4c5dc054d1897ba" maintainer="Luca Monducci"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#quick_start">Per iniziare</a></li>
  <li><a href="#manuals">Manuali</a></li>
  <li><a href="#other">Other (shorter) Documents</a></li>
</ul>

<p>
La creazione di un sistema operativo libero di alta qualità include la
redazione di manuali tecnici che descrivono le operazioni e l'uso dei
programmi. Il progetto Debian si impegna a fornire la miglior
documentazione a tutti gli utenti in maniera facilmente accessibile.
Questa pagina contiene una raccolta di collegamenti che portano a guide
all'installazione, HOWTO, FAQ, note di rilascio, wiki e altro ancora.
</p>


<h2><a id="quick_start">Per iniziare</a></h2>

<p>
Se non si conosce già Debian è raccomandabile iniziare leggendo:
</p>

<aside class="light">
  <span class="fas fa-fast-forward fa-5x"></span>
</aside>

<ul>
  <li><a href="$(HOME)/releases/stable/installmanual">Guida all'installazione</a></li>
  <li><a href="manuals/debian-faq/">Debian GNU/Linux FAQ</a></li>
</ul>

<p>
Sono da tenere a portata di mano durante la prima installazione,
probabilmente risolveranno molti dubbi ed aiuteranno a lavorare con il
proprio nuovo sistema Debian.
</p>

<p>
Dopo si dovrebbe proseguire con:
</p>

<ul>
  <li><a href="manuals/debian-reference/">Debian Reference (La guida Debian)</a>,
      una guida utente sintetica con focus sulla riga di comando della shell</li>
  <li><a href="$(HOME)/releases/stable/releasenotes">Note di rilascio</a>,
      utili a chi sta effettuando un aggiornamento</li>
  <li><a href="https://wiki.debian.org/">Debian Wiki</a>, una buona fonte
      di informazioni per i nuovi utenti</li>
</ul>

<p style="text-align:center"><button type="button"><span class="fas fa-print fa-2x"></span> 
<a href="https://www.debian.org/doc/manuals/refcard/refcard">Stampa la
Debian GNU/Linux Reference Card</a></button></p>

<h2><a id="manuals">Manuali</a></h2>

<p>
Gran parte della documentazione inclusa in Debian è stata scritta per
GNU/Linux in generale, c'è anche della documentazione scritta specificatamente
per Debian. Questi documenti sono divisi principalmente in queste categorie:
</p>

<ul>
  <li>Manuali: sembrano dei libri poiché descrivono in maniera esaustiva
  argomenti corposi. Molti dei manuali elencati qui sono disponibili sia
  online che in pacchetti Debian; infatti, la maggior parte dei manuali
  presenti sul sito sono estratti dai rispettivi pacchetti Debian.</li>
  <li>HOWTO (come fare): come dice il loro nome i <a
  href="https://tldp.org/HOWTO/HOWTO-INDEX/categories.html">documenti
  HOWTO</a> descrivono <em>come fare</em> una particolare cosa, cioè
  danno consigli dettagliati e pratici su come fare qualcosa.</li>
  <li>FAQ (domande frequenti): sono stati redatti molti documenti con le
  risposte alle <em>domande frequenti</em>. Le risposte alle domande che
  riguardano Debian sono raccolte nelle <a href="manuals/debian-faq/">Debian
  FAQ</a>. Oltre a queste, un documento a parte, <a href="../CD/faq/">FAQ
  sulle immagini dei CD/DVD Debian</a> raccoglie le risposte a ogni tipo di
  domanda riguardo ai supporti per l'installazione.</li>
  <li>Altri documenti (brevi): consultare l'<a href="#other">elenco</a>
  dei documenti più brevi.</li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Per l'elenco completo
dei manuali Debian e altra documentazione, visitare la pagina del <a
href="ddp">Debian Documentation Project</a>. Inoltre molti manuali per
gli utenti di Debian GNU/Linux sono disponibili come <a
href="books">libri stampati</a>.</p>
</aside>

<p>Molti dei manuali elencati qui sono disponibili sia online che in
pacchetti Debian; infatti, la maggior parte dei manuali presenti sul sito
sono estratti dai rispettivi pacchetti Debian. Sotto sono riportati per
ogni manuale il nome del suo pacchetto e/o il collegamento alla versione
online.</p>

<h3>Manuali specifici Debian</h3>

<div class="line">
  <div class="item col50">
    <h4><a href="user-manuals">Manuali utente</a></h4>
    <ul>
      <li><a href="user-manuals#faq">Debian GNU/Linux FAQ</a></li>
      <li><a href="user-manuals#install">Debian Installation Guide</a></li>
      <li><a href="user-manuals#relnotes">Debian Release Notes</a></li>
      <li><a href="user-manuals#refcard">Debian Reference Card</a></li>
      <li><a href="user-manuals#debian-handbook">The Debian Administrator's Handbook</a></li>
      <li><a href="user-manuals#quick-reference">Debian Reference</a></li>
      <li><a href="user-manuals#securing">Securing Debian Manual</a></li>
      <li><a href="user-manuals#aptitude">aptitude user's manual</a></li>
      <li><a href="user-manuals#apt-guide">APT User's Guide</a></li>
      <li><a href="user-manuals#apt-offline">Using APT Offline</a></li>
      <li><a href="user-manuals#java-faq">Debian GNU/Linux and Java FAQ</a></li>
      <li><a href="user-manuals#hamradio-maintguide">Debian Hamradio Maintainer's Guide</a></li>
    </ul>
  </div>

  <div class="item col50 lastcol">

    <h4><a href="devel-manuals">Manuali per gli sviluppatori</a></h4>
    <ul>
      <li><a href="devel-manuals#policy">Debian Policy Manual</a></li>
      <li><a href="devel-manuals#devref">Debian Developer's Reference</a></li>
      <li><a href="devel-manuals#debmake-doc">Guide for Debian Maintainers</a></li>
      <li><a href="devel-manuals#packaging-tutorial">Introduction to Debian packaging</a></li>
      <li><a href="devel-manuals#menu">Debian Menu System</a></li>
      <li><a href="devel-manuals#d-i-internals">Debian Installer internals</a></li>
      <li><a href="devel-manuals#dbconfig-common">Guide for database using package maintainers</a></li>
      <li><a href="devel-manuals#dbapp-policy">Policy for packages using databases</a></li>
    </ul>

  </div>
</div>

<p style="text-align:center"><button type="button"><span class="fas
fa-book-open fa-2x"></span> <a href="misc-manuals#history">Consultare la
Storia del Progetto Debian</a></button></p>


<h2><a id="other">Altri documenti (brevi)</a></h2>

<p>I seguenti documenti forniscono informazioni veloci e brevi:</p>

<p>
<dl>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Se hai verificato tutto
quanto qui descritto senza trovare le risposte alle domande che poni o
soluzione ai tuoi problemi con Debian, allora dai uno sguardo alle nostre
<a href="../support">pagine di supporto</a>.</p>
</aside>

  <dt><strong><a href="https://tldp.org/docs.html#man">pagine man</a></strong></dt>
    <dd>Per tradizione tutti i programmi Unix sono documentati con delle
    <em>pagine di manuale</em>, vale a dire il manuale di riferimento che
    è accessibile tramite il comando <tt>man</tt>. In genere non sono
    pensate per chi si sta avvicinando a Unix. Si possonoricercare e
    leggere le pagine man disponibili in Debian su <a
    href="https://manpages.debian.org/cgi-bin/man.cgi">https://manpages.debian.org/</a>.
    </dd>

  <dt><strong><a href="https://www.gnu.org/software/texinfo/manual/texinfo/html_node/index.html">file info</a></strong></dt>
    <dd>Molto del software GNU ha la propria documentazione contenuta nei
    <em>file info</em> invece che nelle pagine man. Questi file contengono
    informazioni dettagliate sul programma stesso, sulle opzioni ed esempi
    d'uso e sono disponibili tramite il comando <tt>info</tt>.
    </dd>

  <dt><strong>file README</strong></dt>
    <dd>I file <em>read me (leggimi)</em> sono molto diffusi &mdash; sono
    dei semplici file di testo che descrivono una sola cosa, normalmente
    un pacchetto. Se ne trovano parecchi nelle sottodirectory di
    <tt>/usr/share/doc/</tt> del proprio sistema Debian. Ogni pacchetto
    software ha una sottodirectory contenente i suoi file readme e che
    potrebbe contenere anche degli esempi di configurazioni. Si noti come,
    per i programmi di maggior dimensione, la documentazione è
    solitamente fornita in un pacchetto separato (con lo stesso nome del
    pacchetto originale e l'aggiunta del suffisso <em>-doc</em>).
    </dd>

  <dt><strong><a href="https://www.debian.org/doc/manuals/refcard/refcard">Debian GNU/Linux Reference Card</a></strong></dt>
    <dd>Le <q>reference card</q> sono dei sommari molto brevi di alcuni
        (sotto)sistemi, solitamente elencano la maggior parte dei comandi
        più usati su un singolo foglio. La Debian GNU/Linux Reference Card
        fornisce un elenco dei più importanti comandi di un sistema Debian.
        È richiesta una conoscenza di base di computer, file, directory e
        della riga di comando. I novizi dovrebbero leggere prima la <a
        href="user-manuals#quick-reference">Debian Reference</a>.
    </dd>

</dl>
</p>
