#use wml::debian::template title="Installazione di Debian &ldquo;bookworm&rdquo;" NOHEADER="true"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bookworm/release.data"
#use wml::debian::translation-check translation="1f438c4027761acce77e56dc1e4a4fc8e3353760" maintainer="Luca Monducci"

<h1>Installazione di Debian <current_release_bookworm></h1>

<if-stable-release release="trixie">
<p><strong>Debian 12 è stata sostituita da
<a href="../../trixie/">Debian 13 (<q>trixie</q>)</a>.
Alcune delle immagini per l'installazione potrebbero non essere più
disponibili o non più funzionanti; si raccomanda di installare
trixie.</strong></p>
</if-stable-release>

<if-stable-release release="bullseye">
<p>
Per le immagini per l'installazione e la documentazione su come installare
<q>bookworm</q> (l'attuale distribuzione Testing) consultare la
<a href="$(HOME)/devel/debian-installer/">pagina Debian-Installer</a>.
</p>
</if-stable-release>

<if-stable-release release="bookworm">
<p>
<strong>Per installare Debian</strong> <current_release_bookworm>
(<em>bookworm</em>), scaricare una delle seguenti immagini (tutte le
immagini dei CD/DVD per i386 e amd64 possono essere usate anche su
chiavette USB):
</p>

<div class="line">
<div class="item col50">
	<p><strong>Immagine del CD <q>netinst</q></strong></p>
		<netinst-images />
</div>


</div>

<div class="line">
<div class="item col50">
	<p><strong>Set completo di CD</strong></p>
		<full-cd-images />
</div>

<div class="item col50 lastcol">
	<p><strong>Set completo di DVD</strong></p>
		<full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (via <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-cd-torrent />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (via <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>)</strong></p>
<full-dvd-torrent />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>DVD (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-dvd-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<p><strong>Blu-ray  (via <a href="$(HOME)/CD/jigdo-cd">jigdo</a>)</strong></p>
<full-bluray-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>Altre immagini (netboot, chiavetta USB, ecc.)</strong></p>
<other-images />
</div>
</div>

<p>
<strong>Note</strong>
</p>

<ul>
<li>
	Per scaricare le immagini complete di CD e DVD si raccomanda l'uso
	di BitTorrent o di jigdo.
</li><li>
	Per le architetture meno diffuse è disponibile un numero limitato di
	immagini di CD e DVD come file ISO o via BitTorrent. I set completi
	sono disponibili solo tramite jigdo.
</li><li>
	I file per verificare le immagini (<tt>SHA256SUMS</tt>,
	<tt>SHA512SUMS</tt>, ecc.) sono disponibili nella stessa directory
	delle immagini.
</li>
</ul>


<h1>Documentazione</h1>

<p>
<strong>A chi vuole leggere un solo documento</strong> prima di procedere
con l'installazione, si consiglia l'<a href="../i386/apa">Installation
Howto</a>, una rapida guida passo-passo al processo d'installazione.
Altri documenti utili sono:
</p>

<ul>
<li><a href="../installmanual">Guida all'installazione di Bookworm</a><br />
istruzioni dettagliate per l'installazione.</li>

<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">FAQ su
Installatore Debian</a> e <a href="$(HOME)/CD/faq/">FAQ su
Debian-CD</a><br />, le risposte alle domande più frequenti.</li>

<li><a href="https://wiki.debian.org/DebianInstaller">Wiki
dell'Installatore Debian</a><br />
documentazione gestita dalla comunità.
</li>
</ul>


<h1 id="errata">Errata</h1>

<p>
Questo è l'elenco dei problemi conosciuti dell'installatore rilasciato
insieme a Debian <current_release_bookworm/>. Chi riscontrasse un problema
con l'installazione di Debian che non è presente in questa pagina è
invitato a segnalare il problema inviando un <a
href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">resoconto
d'installazione</a> oppure a verificare <a
href="https://wiki.debian.org/DebianInstaller/BrokenThings">sul
wiki</a> che il problema non sia già presente tra i problemi noti.
</p>

## Translators: copy/paste from devel/debian-installer/errata
<h3 id="errata-r0">Errata del rilascio  12.0</h3>

<dl class="gloss">

<!--
     <dt>Desktop installations may not work using CD#1 alone</dt>

     <dd>Due to space constraints on the first CD, not all of the
     expected GNOME desktop packages fit on CD#1. For a successful
     installation, use extra package sources (e.g. a second CD or a
     network mirror) or use a DVD instead.

     <br /> <b>Status:</b> It is unlikely more efforts can be made to
     fit more packages on CD#1. </dd>
-->
</dl>

<p>
Delle versioni migliorate del sistema di installazione sono in sviluppo
per il prossimo rilascio di Debian; tali versioni possono essere usate
anche per installare bookworm. Per i dettagli consultare la
<a href="$(HOME)/devel/debian-installer/">pagina dell'Installatore
Debian</a>.
</p>
</if-stable-release>
