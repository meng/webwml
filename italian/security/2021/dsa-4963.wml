#use wml::debian::translation-check translation="6485968c0f180fd4720e21f7f0bdc48b3c3bf4f0" maintainer="Giuseppe Sacco"
<define-tag description>aggiornamento di sicurezza</define-tag>
<define-tag moreinfo>
<p>Sono state scoperte varie vulnerabilità in OpenSSL, uno strumento per il Secure
Sockets Layer.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3711">CVE-2021-3711</a>

    <p>John Ouyang ha riferito di una vulnerabilità di tipo «buffer overflow» nella
    decifratura SM2. Chi ha il permesso di presentare un
    contenuto cifrato con SM2 ad una applicazione, sfruttando questa vulnerabilità,
    può cambiare il comportamento dell'applicazione o causarne l'arresto
    (denial of service).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3712">CVE-2021-3712</a>

    <p>Ingo Schwarze ha riferito di un problema di tipo «buffer underrun» durante
    la gestione di stringhe ASN.1 nella funzione X509_aux_print() che può
    portare ad un blocco del servizio.</p></li>

</ul>

<p>Dettagli aggiuntivi possono essere trovati nell'avviso a monte:
<a href="https://www.openssl.org/news/secadv/20210824.txt">https://www.openssl.org/news/secadv/20210824.txt</a></p>

<p>Per la vecchia distribuzione stabile (buster), questi problemi sono stati risolti
nella versione 1.1.1d-0+deb10u7.</p>

<p>Per la distribuzione stabile (bullseye), questi problemi sono stati risolti nella
versione 1.1.1k-1+deb11u1.</p>

<p>Raccomandiamo di aggiornare i propri pacchetti openssl.</p>

<p>Per lo stato dettagliato della sicurezza di openssl, fare riferimento a
<a href="https://security-tracker.debian.org/tracker/openssl">https://security-tracker.debian.org/tracker/openssl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4963.data"
# $Id: $
