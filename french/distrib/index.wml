#use wml::debian::template title="Obtenir Debian"
#use wml::debian::translation-check translation="e2b3956e193e35640f906e022d18f79d5522a104" maintainer="Jean-Paul Guillonneau"
#include "$(ENGLISHDIR)/releases/images.data"

<p>Debian est distribuée <a href="../intro/free">librement</a>
sur Internet. Vous pouvez la télécharger entièrement depuis n'importe
lequel de nos <a href="ftplist">miroirs</a>.

Le <a href="../releases/stable/installmanual">Manuel d'installation</a>
fournit les instructions détaillées et les notes de publication peuvent être
consultées <a href="../releases/stable/releasenotes">ici</a>.

</p>

<p>Cette page propose des options pour installer la version stable de Debian.
Si vous êtes intéressé par les versions de test (testing) ou en cours de
développement (unstable), veuillez consulter la page à propos des
<a href="../releases/">versions de Debian</a>.</p>

<div class="line">
<div class="item col50">
<h2><a href="netinst">Télécharger une image d'installation</a></h2>
<p>
Suivant la connexion Internet disponible,
vous pouvez télécharger une des images suivantes :
</p>
<ul>
<li>
une <a href="netinst"><strong>image d'installation
de taille réduite</strong></a>, rapide à télécharger, à enregistrer sur
disque amovible.

Une connexion à Internet sera nécessaire
sur la machine où vous installerez Debian ;

<ul class="quicklist downlist">
<li><a
  title="Télécharger l'installateur pour PC 64 bits (Intel et AMD)"
  href="<stable-images-url/>/amd64/iso-cd/debian-<current-tiny-cd-release-filename/>-amd64-netinst.iso">\
  image ISO <q>netinst</q> pour PC 64 bits</a>
</li>
<li><a
  title="Télécharger l'installateur pour PC 32 bits (Intel et AMD)"
  href="<stable-images-url/>/i386/iso-cd/debian-<current-tiny-cd-release-filename/>-i386-netinst.iso">\
  image ISO <q>netinst</q> pour PC 32 bits</a>
</li>
</ul>
</li>

<li>
une <a href="../CD/"><strong>image d'installation complète</strong></a>
de taille plus importante, qui contient plus de paquets,
facilitant l'installation sur des machines sans accès à Internet.

<ul class="quicklist downlist">
<li><a
  title="Télécharger les torrents de DVD pour PC 64 bits (Intel et AMD)"
  href="<stable-images-url/>/amd64/bt-dvd/">torrents pour PC 64 bits (DVD)</a>
</li>
<li><a
  title="Télécharger les torrents de DVD pour PC 32 bits (Intel et AMD)"
  href="<stable-images-url/>/i386/bt-dvd/">torrents pour PC 32 bits (DVD)</a>
</li>
<li><a
  title="Télécharger les torrents de CD pour PC 64 bits (Intel et AMD)"
  href="<stable-images-url/>/amd64/bt-cd/">torrents pour PC 64 bits (CD)</a>
</li>
<li><a
  title="Télécharger les torrents de CD pour PC 32 bits (Intel et AMD)"
  href="<stable-images-url/>/i386/bt-cd/">torrents pour PC 32 bits (CD)</a>
</li>
</ul>
</li>
</ul>
</div>
<div class="item col50 lastcol">
<h2><a href="https://cloud.debian.org/images/cloud/">Utiliser une image Debian pour l'informatique dématérialisée</a></h2>

  <p>Une <a href="https://cloud.debian.org/images/cloud/"><strong>image officielle pour
   l’informatique dématérialisée</strong></a>, construite par l’équipe Debian pour l'informatique dématérialisée, peut
   être utilisée directement sur :</p>
  <ul>
      <li>votre fournisseur OpenStack, aux formats qcow2 ou raw ;
      <ul class="quicklist downlist">
	   <li>AMD/Intel 64 bits (<a title="OpenStack image for 64-bit AMD/Intel qcow2" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-amd64.qcow2">qcow2</a>, <a title="OpenStack image for 64-bit AMD/Intel raw" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-amd64.raw">raw</a>)</li>
       <li>ARM 64 bits (<a title="OpenStack image for 64-bit ARM qcow2" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-arm64.qcow2">qcow2</a>, <a title="OpenStack image for 64-bit ARM raw" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-arm64.raw">raw</a>)</li>
	   <li>PowerPC 64 bits petit-boutiste (<a title="OpenStack image for 64-bit Little Endian PowerPC qcow2" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-ppc64el.qcow2">qcow2</a>, <a title="OpenStack image for 64-bit Little Endian PowerPC raw" href="https://cloud.debian.org/images/cloud/bullseye/latest/debian-11-generic-ppc64el.raw">raw</a>)</li>
      </ul>
      </li>
      <li>Amazon EC2, soit une image de machine ou avec AWS Marketplace ;
	   <ul class="quicklist downlist">
	    <li><a title="Amazon Machine Images" href="https://wiki.debian.org/Cloud/AmazonEC2Image/">Images de machine d’Amazon</a></li>
	    <li><a title="AWS Marketplace" href="https://aws.amazon.com/marketplace/seller-profile?id=4d4d4e5f-c474-49f2-8b18-94de9d43e2c0">AWS Marketplace</a></li>
	   </ul>
      </li>
      <li>Microsoft Azure, sur Azure Marketplace.
	   <ul class="quicklist downlist">
	    <li><a title="Debian 11 on Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-11?tab=PlansAndPrice">Debian 11 (« Bullseye »)</a></li>
        <li><a title="Debian 10 on Azure Marketplace" href="https://azuremarketplace.microsoft.com/en-us/marketplace/apps/debian.debian-10?tab=PlansAndPrice">Debian 10 (« Buster »)</a></li>
	   </ul>
      </li>
    </ul>
 </div>
</div>

<div class="line">
<div class="item col50">
<h2><a href="../CD/vendors/">Acheter un jeu de CD ou
DVD auprès d'un des distributeurs de CD Debian</a></h2>

  <p>
     Beaucoup de distributeurs vendent la distribution pour moins de
     5&nbsp;dollars américains (moins de 5&nbsp;euros) plus les frais
     de port (voyez sur leurs pages web s'ils livrent à l'étranger).
     <br />
     Certains des <a href="../doc/books">livres sur Debian</a> contiennent
     aussi des CD.
  </p>

<p>
Voici les avantages fondamentaux des CD :
</p>

  <ul>
    <li>l'installation à partir d'un jeu de CD est plus immédiate ;</li>
    <li>vous pouvez installer Debian sur des machines sans connexion
    	Internet ;</li>
    <li>vous pouvez installer Debian sur autant de machines que vous voulez
    sans devoir télécharger à nouveau les paquets ;</li>
    <li>le CD peut être utilisé pour réparer plus facilement un
        système Debian endommagé.</li>
  </ul>

<h2><a href="pre-installed">Acheter un
ordinateur avec Debian préinstallée</a></h2>

   <p>Il y a de nombreux avantages à cela&nbsp;:</p>

   <ul>
    <li>vous n'avez pas besoin d'installer Debian ;</li>
    <li>l'installation est préconfigurée pour correspondre au matériel ;</li>
    <li>le marchand peut vous fournir une aide technique.</li>
   </ul>
</div>

 <div class="item col50 lastcol">
<h2><a href="../CD/live/">Essayer Debian en autonome avant l'installation</a></h2>
<p>
Vous pouvez essayer Debian en amorçant un système autonome à partir d'un CD,
d'un DVD ou d'une clef USB sans installer un seul fichier sur l'ordinateur.

Une fois prêt, vous pouvez exécuter l'installateur inclus (à partir de Debian 10
Buster, il s’agit de l’<a href="https://calamares.io">installateur
Calamares</a> convivial).

Si l'image est compatible avec vos désirs en terme de taille, langue
et sélection de paquets, cette méthode pourrait vous convenir.

Consultez les <a href="../CD/live#choose_live">renseignements
à propos de cette méthode</a> pour vous aider à décider.
</p>

<ul class="quicklist downlist">
<li><a title="Télécharger les torrents pour systèmes autonomes PC 64 bits (Intel et AMD)"
href="<live-images-url/>/amd64/bt-hybrid/">torrents pour systèmes autonomes PC 64 bits</a></li>
<li><a title="Télécharger les torrents pour systèmes autonomes PC 32 bits (Intel et AMD)"
href="<live-images-url/>/i386/bt-hybrid/">torrents pour systèmes autonomes PC 32 bits</a></li>
</ul>
</div>
</div>
