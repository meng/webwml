#use wml::debian::template title="Listes de diffusion concernant les évènements Debian" BARETITLE=true
#use wml::debian::translation-check translation="bc1e65e7c361128c9d1f693b442e2de21c11a736" maintainer="Baptiste Jammet"

<p>Il y a un nombre assez important de listes de diffusion pour les évènements 
concernant Debian. Elles sont généralement organisées par zone géographique.
La liste ci-dessous est triée de la plus spécifique à la plus générale — vous devriez 
donc commencer à lire en partant du haut jusqu'à ce que vous trouviez une zone qui
corresponde à l'évènement auquel vous pensez.</p>

<ul>
  <li> <a href="mailto:debian-dug-by@lists.debian.org">Biélorussie</a>, veuillez
  envoyer aussi un courriel à <a
  href="mailto:debian-events-eu@lists.debian.org">la liste européenne</a></li>
  <li> <a href="mailto:debian-br-eventos@alioth-lists.debian.net">Brésil</a></li>
  <li> <a href="mailto:debian-dug-in@lists.debian.org">Inde</a></li>
  <li> <a href="mailto:debian-events-nl@lists.debian.org">Pays-Bas</a></li>
  <li> <a href="mailto:debian-events-apac@lists.debian.org">Asie/Pacifique</a></li> 
  <li> <a href="mailto:debian-events-eu@lists.debian.org">Europe (Union Européenne ou non)</a></li>
  <li> <a href="mailto:debian-events-ha@lists.debian.org">Amérique Latine</a></li>
  <li> <a href="mailto:debian-events-na@lists.debian.org">Amérique du nord</a></li>
 
  <li> <a href="mailto:events@debian.org">partout ailleurs</a>, mais nous 
  utiliserons sûrement la <a
  href="mailto:debian-events-eu@lists.debian.org">liste européenne</a>
  pour nous coordonner.</li>
</ul>

<p>Vous pouvez visiter les pages du wiki consacrées aux <a
href="https://wiki.debian.org/DebianLocations">localisations de Debian</a> et aux <a
href="https://wiki.debian.org/LocalGroups">groupes locaux de Debian</a> pour trouver
d'autres moyens de contacter des personnes de Debian organisant des événements dans
votre région.</p>

<p>Pour information : nous serions ravis de voir se développer des communautés
organisant des évènements sur les autres continents. Si cela arrivait, n'hésitez
pas à envoyer un courriel (en anglais) à 
<a href="mailto:events@debian.org">l'équipe des évènements</a>. Nous essaierons
de vous aider.</p>
