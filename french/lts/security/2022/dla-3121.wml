#use wml::debian::translation-check translation="fae0bc320e0f2880603f2c62ba071acefe469a04" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans le navigateur
web Firefox de Mozilla, qui pouvaient éventuellement avoir pour
conséquences l'exécution de code arbitraire, le contournement du <q>Content
Security Policy</q> (CSP) ou une fixation de session.</p>

<p>Cette mise à jour rétablit la prise en charge des architectures i386 et
arm64. La prise en charge de l'architecture armhf est encore absente, en
attente de modifications du réseau de serveurs d'empaquetage.</p>

<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
102.3.0esr-1~deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets firefox-esr.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de firefox-esr,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/firefox-esr">\
https://security-tracker.debian.org/tracker/firefox-esr</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3121.data"
# $Id: $
