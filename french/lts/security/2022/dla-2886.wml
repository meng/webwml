#use wml::debian::translation-check translation="554719f868f39488252e4634a2ef2d0fdb5a7ec7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes de sécurité ont été découverts dans SLURM (« Simple
Linux Utility for Resource Management »), un système de gestion de
ressources et un ordonnanceur de tâches pour grappe, qui pourraient avoir
pour conséquences un déni de service, la divulgation d'informations ou une
élévation de privilèges.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-12838">CVE-2019-12838</a>

<p>SchedMD Slurm permet des attaques d'injection SQL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-12693">CVE-2020-12693</a>

<p>Dans les rares cas où Message Aggregation est activé, Slurm permet un
contournement d'authentification à l'aide d'un chemin ou d'un canal
alternatif. Une situation de compétition permet à un utilisateur de charger
un processus en tant qu'utilisateur arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27745">CVE-2020-27745</a>

<p>Dépassement de tampon RPC dans le greffon PMIx MPI.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31215">CVE-2021-31215</a>

<p>SchedMD Slurm permet l'exécution de code distant en tant que SlurmUser
parce que l'utilisation d'un script PrologSlurmctld ou EpilogSlurmctld
conduit à un mauvais traitement de l'environnement.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 16.05.9-1+deb9u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets slurm-llnl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de slurm-llnl, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/slurm-llnl">\
https://security-tracker.debian.org/tracker/slurm-llnl</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2886.data"
# $Id: $
