#use wml::debian::translation-check translation="1f3a9ebc23f3cab20044d6e7ffb40350f7a9304b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pourraient conduire à une élévation des privilèges, un déni de service ou une
fuite d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2978">CVE-2022-2978</a>

<p><q>butt3rflyh4ck</q>, Hao Sun et Jiacheng Xu ont signalé un défaut dans le
pilote de système de fichiers nilfs2 qui pourrait conduire à une
utilisation de mémoire après libération. Une utilisation locale pourrait
exploiter cela pour provoquer un déni de service (plantage ou corruption de
mémoire) ou éventuellement pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3521">CVE-2022-3521</a>

<p>L’outil syzbot a trouvé une situation de compétition dans le sous-système KCM
qui pourrait conduire à plantage.</p>

<p>Ce sous-système n’est pas activé dans les configurations officielles du
noyau dans Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3524">CVE-2022-3524</a>

<p>L’outil syzbot a trouvé une situation de compétition dans la pile IPv6 qui
pourrait conduire à une fuite de mémoire. Un utilisateur local pourrait
exploiter cela pour provoquer un déni de service (épuisement de mémoire).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3564">CVE-2022-3564</a>

<p>Un défaut a été découvert dans le sous-système Bluetooth L2CAP qui pourrait
aboutir à une utilisation de mémoire après libération. Cela pouvait être
exploitable pour provoquer un déni de service (plantage ou corruption de mémoire)
ou éventuellement pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3565">CVE-2022-3565</a>

<p>Un défaut a été découvert dans le pilote mISDN qui pourrait aboutir à
une utilisation de mémoire après libération. Cela peut être exploitable pour
provoquer un déni de service (plantage ou corruption de mémoire) ou éventuellement
pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3594">CVE-2022-3594</a>

<p>Andrew Gaul a signalé que le pilote r8152 Ethernet pourrait journaliser un
nombre excessif de messages dans la réponse aux erreurs de réseau. Un attaquant
distant pourrait éventuellement exploiter cela pour provoquer un déni de service
(épuisement de ressources).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3621">CVE-2022-3621</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-3646">CVE-2022-3646</a>

<p>L’outil syzbot a trouvé des défauts dans le pilote de système de fichiers
nilfs2 qui pourraient conduire à un déréférencement de pointeur NULL ou à une
fuite de mémoire. Un utilisateur autorisé à monter des images de système de
fichiers arbitraires pourrait utiliser cela pour provoquer un déni de service
(plantage ou épuisement de ressources).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3628">CVE-2022-3628</a>

<p>Dokyung Song, Jisoo Jang et Minsuk Kang ont signalé un potentiel dépassement de
tampon de tas dans le pilote brcmfmac Wi-Fi. Un utilisateur capable de connecter
un périphérique USB malveillant pourrait exploiter cela pour provoquer un déni
de service (plantage ou corruption de mémoire) ou éventuellement pour une
élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3640">CVE-2022-3640</a>

<p>Un défaut a été découvert dans le sous-système Bluetooth L2CAP qui pourrait
conduire à une utilisation de mémoire après libération. Cela pourrait être
exploitable pour provoquer un déni de service (plantage ou corruption de
mémoire) ou éventuellement pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3643">CVE-2022-3643</a> (XSA-423)

<p>Un défaut a été découvert dans le pilote de dorsal réseau Xen qui pourrait
aboutir à la génération de tampons de paquets mal formés. Si ces paquets étaient
retransmis vers certains autres périphériques réseau, un client Xen pourrait
exploiter cela pour provoquer un déni de service (plantage ou réinitialisation
de périphérique).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3649">CVE-2022-3649</a>

<p>L’outil syzbot a trouvé des défauts dans le pilote de système de fichiers
nilfs2 qui pourraient conduire à une utilisation de mémoire après libération. Un
utilisateur autorisé à monter des images de système de fichiers arbitraires
pourraient les utiliser pour provoquer un déni de service (plantage ou
corruption de mémoire) ou éventuellement pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-4378">CVE-2022-4378</a>

<p>Kyle Zeng a trouvé un défaut dans procfs qui pourrait provoquer un
dépassement de pile. Un utilisateur local autorisé à écrire sur un sysctl
pourrait utiliser cela pour provoquer un déni de service (plantage ou corruption
de mémoire) ou éventuellement pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-20369">CVE-2022-20369</a>

<p>Un défaut a été découvert dans le pilote de média v4l2-mem2mem qui pourrait
conduire à une écriture hors limites. Un utilisateur local avec accès à un tel
périphérique pourrait exploiter cela pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29901">CVE-2022-29901</a>

<p>Johannes Wikner et Kaveh Razavi ont signalé que pour les processeurs Intel (Intel
Core génération 6, 7 et 8), les protections contre les attaques par injection
spéculative dans des cibles de branche étaient insuffisantes dans certaines
circonstances, ce qui pouvait permettre une exécution spéculative de code sous
certaines conditions dépendantes de la microarchitecture.</p>

<p>Plus d’informations sont disponibles sur
<a href="https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/advisory-guidance/return-stack-buffer-underflow.html">https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/advisory-guidance/return-stack-buffer-underflow.html</a></p></li>


<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40768">CVE-2022-40768</a>

<p><q>hdthky</q> a signalé que le pilote de l’adaptateur stex SCSI n’initialisait
pas complètement une structure qui est copiée en espace utilisateur. Un
utilisateur local avec accès à un tel périphérique pourrait exploiter cela pour
divulguer des informations sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41849">CVE-2022-41849</a>

<p>Une situation de compétition a été découverte dans le pilote graphique
smscufx qui pourrait conduire à une utilisation de mémoire après
libération. Un utilisateur capable de retirer le périphérique physique tout
en accédant aussi au nœud de périphérique pourrait exploiter cela pour provoquer
un déni de service (plantage ou corruption de mémoire) ou éventuellement pour
une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-41850">CVE-2022-41850</a>

<p>Une situation de compétition a été découverte dans le pilote d’entrée
hid-roccat qui pourrait conduire à une utilisation de mémoire après libération.
Un utilisateur local capable d’accéder à un tel périphérique pourrait exploiter
cela pour provoquer un déni de service (plantage ou corruption de mémoire) ou
éventuellement pour une élévation des privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42328">CVE-2022-42328</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-42329">CVE-2022-42329</a> (XSA-424)>

<p>Yang Yingliang a signalé que le pilote de dorsal réseau Xen n’utilisait pas
la fonction correcte pour libérer les tampons de paquet dans un cas, ce qui
pourrait conduire à un interblocage. Un client Xen pourrait exploiter cela pour
provoquer un déni de service (blocage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42895">CVE-2022-42895</a>

<p>TamÃ¡s Koczka a signalé un défaut dans le sous-système Bluetooth L2CAP qui
pourrait aboutir à la lecture de mémoire non réinitialisée. Un attaquant proche
capable de se connecter en Bluetooth pourrait exploiter cela pour divulguer des
informations sensibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42896">CVE-2022-42896</a>

<p>TamÃ¡s Koczka a signalé des défauts dans le sous-système Bluetooth L2CAP qui
pourraient conduire à une utilisation de mémoire après libération. Un attaquant
proche capable de se connecter en SMP Bluetooth pourrait exploiter cela pour
provoquer un déni de service (plantage ou corruption de mémoire) ou
éventuellement pour une exécution de code à distance.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-43750">CVE-2022-43750</a>

<p>L’outil syzbot a trouvé que le pilote de surveillance USB monitor (usbmon)
permettait à des programmes en espace utilisateur d’écraser les structures de
données du pilote. Un utilisateur local avec accès à un périphérique de
surveillance USB pourrait exploiter cela pour provoquer un déni de service
(corruption de mémoire ou plantage) ou éventuellement pour une élévation des
privilèges. Cependant, par défaut, seul le superutilisateur peut accéder à de
tels périphériques.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 4.19.269-1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3245.data"
# $Id: $
