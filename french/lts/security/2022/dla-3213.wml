#use wml::debian::translation-check translation="207a721e982248df84a4874b35eac25dc4cc30f3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une attaque potentielle par déni de
service (DoS) à l’encontre de krb5, une suite d’outils mettant en œuvre le
système d’authentification Kerberos. Un dépassement d'entier dans l’analyse de
certificats PAC pourrait être exploité si une entité inter-domaine agissait
d’une manière malveillante.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42898">CVE-2022-42898</a>

<p>Vulnérabilité d’analyse de tampon de krb5_pac_parse().</p></li>

</ul>

<p>Pour Debian 10 « Buster », ce problème a été corrigé dans
la version 1.17-3+deb10u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets krb5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3213.data"
# $Id: $
