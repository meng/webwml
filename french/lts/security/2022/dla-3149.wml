#use wml::debian::translation-check translation="a2309c5e0522b188c4703bf2c6eb3035b7cd6ae3" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Nokogiri, un analyseur
HTML/XML/SAX/Reader pour le langage de programmation Ruby, menant à une
injection de commande, une injection d'entité externe XML (XXE) et à un
déni de service (DoS).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-5477">CVE-2019-5477</a>

<p>Une vulnérabilité d'injection de commande permettait l'exécution de
commandes dans un sous-processus au moyen de la méthode <q>Kernel.open</q>
de Ruby. Les processus ne sont vulnérables que si la méthode non documentée
<q>Nokogiri::CSS::Tokenizer#load_file</q> est appelée avec une entrée non
sûre de l'utilisateur comme nom de fichier. Cette vulnérabilité apparaît
dans le code généré par le gem Rexical versions v1.0.6 et antérieures.
Rexical est utilisé par Nokogiri pour générer le code d'analyse lexical
pour le traitement de requêtes CSS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26247">CVE-2020-26247</a>

<p>Une vulnérabilité d'injection d'entité externe XML (XXE) : les schémas
XML analysés par Nokogiri::XML::Schema étaient considérés comme fiables par
défaut, permettant l’accès à des ressources externes à travers le réseau,
autorisant éventuellement des attaques XXE ou SSRF. Ce comportement est
contraire à la politique de sécurité suivie par les responsables de
Nokogiri qui est de traiter toutes les entrées comme non fiables par défaut
chaque fois que c'est possible.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-24836">CVE-2022-24836</a>

<p>Nokogiri contient une expression rationnelle inefficace qui est
vulnérable à un retour sur trace excessif lorsqu'il tente de détecter
l'encodage dans les documents HTML.</p></li>

</ul>

<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
1.10.0+dfsg1-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-nokogiri.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ruby-nokogiri,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ruby-nokogiri">
https://security-tracker.debian.org/tracker/ruby-nokogiri</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3149.data"
# $Id: $
