#use wml::debian::translation-check translation="dcdcd755447c0ec9319d2893757a3f5c9abfeff5" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans BIND, une
implémentation de serveur DNS.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2795">CVE-2022-2795</a>

<p>Yehuda Afek, Anat Bremler-Barr et Shani Stajnrod ont découvert qu'un
défaut dans le code du solveur peut faire que <q>named</q> prend un temps
excessif lors du traitement de grandes délégations et dégrade
significativement les performances du solveur, ce qui a pour conséquence un
déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-38177">CVE-2022-38177</a>

<p>Le code de vérification de DNSSEC pour l'algorithme ECDSA est exposé à
un défaut de fuite de mémoire. Un attaquant distant peut tirer avantage de
ce défaut pour provoquer la consommation de ressources par BIND, avec pour
conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-38178">CVE-2022-38178</a>

<p>Le code de vérification de DNSSEC pour l'algorithme EdDSA est exposé à
un défaut de fuite de mémoire. Un attaquant distant peut tirer avantage de
ce défaut pour provoquer la consommation de ressources par BIND, avec pour
conséquence un déni de service.</p></li>

</ul>

<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
1:9.11.5.P4+dfsg-5.1+deb10u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bind9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de bind9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/bind9">\
https://security-tracker.debian.org/tracker/bind9</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3138.data"
# $Id: $
