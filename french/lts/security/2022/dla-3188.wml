#use wml::debian::translation-check translation="01a013e46fe770e95cb09c58a0257a613ef027b1" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Les quelques problèmes de sécurité suivants ont été découverts dans
sysstat, des outils de performance du système pour Linux :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16167">CVE-2019-16167</a>

<p>Les versions de sysstat inférieures à 12.1.6 ont une corruption de
mémoire due à un dépassement d'entier dans remap_struct() dans sa_common.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19725">CVE-2019-19725</a>

<p>sysstat jusqu'à la version 12.2.0 présente une double libération de zone
de mémoire dans chck_file_actlst dans sa_common.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39377">CVE-2022-39377</a>

<p>Sur les systèmes 32 bits, allocate_structures contient un dépassement de
size_t dans sa_common.c. La fonction allocate_structures vérifie
insuffisamment les limites avant les multiplications arithmétiques,
permettant un dépassement dans la taille allouée pour le tampon
représentant les activités du système. Ce problème pouvait conduire à une
exécution de code à distance (RCE).</p></li>

</ul>

<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
12.0.3-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets sysstat.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de sysstat, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/sysstat">\
https://security-tracker.debian.org/tracker/sysstat</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3188.data"
# $Id: $
