#use wml::debian::translation-check translation="6d8c34336f895f20340412980c445fdbf620e2e0" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été découvertes dans ruby-rack, un serveur web
répandu pour les applications Ruby :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30122">CVE-2022-30122</a>:

<p>Vulnérabilité de déni de service évitée dans le composant d'analyse de
données Multipart HTTP.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30123">CVE-2022-30123</a>:

<p>Vulnérabilité potentielle d'injection de séquence d'échappement shell
évitée, qui pouvait être déclenchée au moyen du système de journalisation.</p></li>

</ul>

<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
2.0.6-3+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-rack.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ruby-rack, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ruby-rack">\
https://security-tracker.debian.org/tracker/ruby-rack</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3095.data"
# $Id: $
