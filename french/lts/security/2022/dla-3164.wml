#use wml::debian::translation-check translation="51f7d5208ee4c3729e996e8ab383b834dd1966cc" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans Django, un cadriciel
populaire de développement web basé sur Python :</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24583">CVE-2020-24583</a>:
Correction de permissions incorrectes sur des répertoires de niveau
intermédiaire dans Python 3.7+. Le mode FILE_UPLOAD_DIRECTORY_PERMISSIONS
ne s'appliquait pas aux répertoires de niveau intermédiaire créés dans le
processus de téléversement de fichiers ni aux répertoires statiques
collectés de niveau intermédiaire lors de l'utilisation de la commande de
gestion collectstatic. Vous devrez examiner et corriger manuellement les
permissions des répertoires de niveau intermédiaire existants.</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-24584">CVE-2020-24584</a>:
Correction d'une vulnérabilité d'élévation de privilèges dans les
répertoires de niveau intermédiaire du cache du système de fichiers. Dans
les versions 3.7 et supérieures de Python, les répertoires de niveau
intermédiaire du cache du système de fichiers avaient l'umask standard du
système plutôt que 0o077 (pas de droits pour le groupe ou les autres
utilisateurs).</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3281">CVE-2021-3281</a>:
Correction d'une attaque possible de traversée de répertoires à l'aide
d'archive.extract(). La fonction django.utils.archive.extract(), utilisée
par <q>startapp --template</q> et <q>startproject --template</q>,
permettait une traversée de répertoires à l'aide d'une archive avec des
chemins absolus ou relatifs comportant des <q>.</q> et des <q>..</q>.</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23336">CVE-2021-23336</a>:
Le correctif empêche une attaque d'empoisonnement de cache web à l'aide
d'une dissimulation de paramètre. Django contient une copie
d'urllib.parse.parse_qsl() qui a été ajoutée pour rétroporter certains
correctifs de sécurité. Un correctif de sécurité supplémentaire a été
publié récemment de sorte que parse_qsl() ne permet plus par défaut
l'utilisation de <q>;</q> comme séparateur de paramètres de requête.</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-34265">CVE-2022-34265</a>:
Les fonctions de base de données Trunc() et Extract() étaient sujettes à
une attaque possible d'injection SQL si des données non sûres étaient
utilisées comme valeur pour les paramètres <q>kind</q> ou
<q>lookup_name</q>. Les applications qui restreignent le choix à une liste
de valeurs sûres connues ne sont pas affectées.</li>
</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans la
version 1:1.11.29-1+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-django.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3164.data"
# $Id: $
