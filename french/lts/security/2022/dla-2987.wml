#use wml::debian::translation-check translation="c1997253e3acf923d0a8dcfe9e975e1d728ef5f2" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Trois problèmes ont été découverts dans libarchive, une bibliothèque
d'archivage et de compression multiformat :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-31566">CVE-2021-31566</a>

<p>des liens symboliques suivis de manière incorrecte lors de changement de
mode, date, ACL et étiquettes pendant l'extraction d'une archive ;</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23177">CVE-2021-23177</a>

<p>l'extraction d'un lien symbolique avec des ACL modifie les ACL de la
cible ;</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19221">CVE-2019-19221</a>

<p>lecture hors limites à cause d'un appel de mbrtowc ou mbtowc incorrect.</p>


<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 3.2.2-2+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libarchive.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libarchive, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libarchive">\
https://security-tracker.debian.org/tracker/libarchive</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2987.data"
# $Id: $
