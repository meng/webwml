#use wml::debian::translation-check translation="5e09c7016983b6f8af780b9b5533679cc6e58b42" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans NTFS-3G, un pilote de
lecture et écriture NTFS pour FUSE. Un utilisateur local peut tirer avantage
de ces défauts pour une élévation locale de privilèges administrateur.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30783">CVE-2022-30783</a>

<p>Un code de retour non valable dans fuse_kern_mount permet l'interception
du trafic du protocole libfuse-lite entre NTFS-3G et le noyau lors de
l'utilisation de libfuse-lite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30784">CVE-2022-30784</a>

<p>Une image NTFS contrefaite peut provoquer une épuisement de tas dans
ntfs_get_attribute_value.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30785">CVE-2022-30785</a>

<p>Un gestionnaire de fichier créé dans fuse_lib_opendir puis utilisé dans
fuse_lib_readdir permet des opérations de lecture et d'écriture de mémoire
arbitraire lors de l'utilisation de libfuse-lite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30786">CVE-2022-30786</a>

<p>Une image NTFS contrefaite peut provoquer un dépassement de tas dans
ntfs_names_full_collate.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30787">CVE-2022-30787</a>

<p>Un dépassement d'entier par le bas dans fuse_lib_readdir permet des
opérations de lecture de mémoire arbitraire lors de l'utilisation de
libfuse-lite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30788">CVE-2022-30788</a>

<p>Une image NTFS contrefaite peut provoquer un dépassement de tas dans
ntfs_mft_rec_alloc.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30789">CVE-2022-30789</a>

<p>Une image NTFS contrefaite peut provoquer un dépassement de tas dans
ntfs_check_log_client_array.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 1:2016.2.22AR.1+dfsg-1+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ntfs-3g.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ntfs-3g, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ntfs-3g">\
https://security-tracker.debian.org/tracker/ntfs-3g</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3055.data"
# $Id: $
