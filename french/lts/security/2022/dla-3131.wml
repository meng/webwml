#use wml::debian::translation-check translation="32a132f04e40dd2b630709768cc7047af1cbeef0" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans le noyau Linux qui
pouvaient conduire à une élévation de privilèges, un déni de service ou des
fuites d'informations.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-4159">CVE-2021-4159</a>

<p>Un défaut a été découvert dans le vérificateur eBPF qui pouvait conduire
à une lecture hors limites. Si l'utilisation non privilégiée d'eBPF est
activée, cela pouvait divulguer des informations sensibles. Cette
utilisation a déjà été désactivée par défaut, ce qui pourrait atténuer
complètement la vulnérabilité.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33655">CVE-2021-33655</a>

<p>Un utilisateur doté d'un accès au pilote de tampon de trame de la
console pouvait provoquer une écriture de mémoire hors limites au moyen du
contrôle d'entrées et de sorties FBIOPUT_VSCREENINFO.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33656">CVE-2021-33656</a>

<p>Un utilisateur doté d'un accès au pilote de tampon de trame de la
console pouvait provoquer une écriture de mémoire hors limites au moyen de
certains contrôles d'entrées et de sorties de configuration de fontes. Ces
contrôles d'entrées et de sorties obsolètes ont été supprimés.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1462">CVE-2022-1462</a>

<p>一只狗 a signalé une situation de compétition dans le sous-système pty
(pseudo-terminal) qui pouvait conduire à une écriture hors limites de
tampon. Un utilisateur local pouvait exploiter cela pour provoquer un déni
de service (plantage ou corruption de mémoire) ou éventuellement une
élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-1679">CVE-2022-1679</a>

<p>L'outil syzbot a découvert une situation de compétition dans le pilote
ath9k_htc qui pouvait conduire à une utilisation de mémoire après
libération. Cela pourrait être exploitable pour provoquer un déni de
service (plantage ou corruption de mémoire) ou éventuellement une élévation
de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2153">CVE-2022-2153</a>

<p><q>kangel</q> a signalé un défaut dans l'implémentation de KVM pour les
processeurs x86 qui pouvait conduire à un déréférencement de pointeur NULL.
Un utilisateur local autorisé à accéder à /dev/kvm pouvait exploiter cela
pour provoquer un déni de service (plantage).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2318">CVE-2022-2318</a>

<p>Une utilisation de mémoire après libération dans la prise en charge du
protocole de radio amateur X.25 PLP (Rose) peut avoir pour conséquence un
déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2586">CVE-2022-2586</a>

<p>Une utilisation de mémoire après libération dans le sous-système
Netfilter peut avoir pour conséquence une élévation locale de privilèges
pour un utilisateur doté de la capacité CAP_NET_ADMIN dans n'importe quel
espace de noms utilisateur ou réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2588">CVE-2022-2588</a>

<p>Zhenpeng Lin a découvert un défaut d'utilisation de mémoire après
libération dans l'implémentation du filtre cls_route qui peut avoir pour
conséquence une élévation locale de privilèges pour un utilisateur doté de
la capacité CAP_NET_ADMIN dans n'importe quel espace de noms utilisateur ou
réseau.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2663">CVE-2022-2663</a>

<p>David Leadbeater a signalé des défauts dans le module du protocole de
connexion nf_conntrack_irc. Quand ce module est activé sur un pare-feu, un
utilisateur externe sur le même réseau IRC qu'un utilisateur interne
pouvait exploiter son analyse laxiste pour ouvrir des ports TCP arbitraires
dans le pare-feu, révéler son adresse IP publique ou pour bloquer sa
connexion IRC au pare-feu.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-3028">CVE-2022-3028</a>

<p>Abhishek Shah a signalé une situation de compétition dans le
sous-système AF_KEY qui pouvait conduire à une écriture ou une lecture hors
limites. Un utilisateur local pouvait exploiter cela pour provoquer un déni
de service (plantage ou corruption de mémoire) pour obtenir des
informations sensibles ou éventuellement pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26365">CVE-2022-26365</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-33740">CVE-2022-33740</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-33741">CVE-2022-33741</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2022-33742">CVE-2022-33742</a>

<p>Roger Pau Monne a découvert que les frontaux de périphériques bloc et
réseau paravirtuels de Xen ne mettaient pas à zéro les régions de mémoire
avant de les partager avec le dorsal, ce qui pouvait avoir pour conséquence
la divulgation d'informations. En complément, il a été découvert que la
granularité de <q>grant table</q> ne permettait pas de partager des pages
de moins de 4 Ko, ce qui pouvait aussi avoir pour conséquence la
divulgation d'informations.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26373">CVE-2022-26373</a>

<p>Sur certains processeurs dotés des capacités <q>Enhanced Indirect Branch
Restricted Speculation</q> (eIBRS) d'Intel, il y a des exceptions aux
propriétés documentées dans certaines situations qui peuvent avoir pour
conséquence la divulgation d'informations.</p>

<p>L'explication du problème par Intel peut être trouvée sur la page
<a href="https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/advisory-guidance/post-barrier-return-stack-buffer-predictions.html">\
https://www.intel.com/content/www/us/en/developer/articles/technical/software-security-guidance/advisory-guidance/post-barrier-return-stack-buffer-predictions.html</a></p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-33744">CVE-2022-33744</a>

<p>Oleksandr Tyshchenko a découvert que les clients Xen ARM peuvent
provoquer un déni de service pour le Dom0 aux moyens de périphériques
paravirtuels.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-36879">CVE-2022-36879</a>

<p>Un défaut a été découvert dans xfrm_expand_policies dans le sous-sytème
xfrm qui peut faire qu'un compte de références soit supprimé deux fois.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-36946">CVE-2022-36946</a>

<p>Domingo Dirutigliano et Nicola Guerrera ont signalé un défaut de
corruption de mémoire dans le sous-système Netfilter qui peut avoir pour
conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39188">CVE-2022-39188</a>

<p>Jann Horn a signalé une situation de compétition dans le traitement par
le noyau du <q>démappage</q> de certaines plages de mémoire. Quand un
pilote créait un mappage de mémoire avec l'indicateur VM_PFNMAP, ce que
font de nombreux pilotes GPU, le mappage de mémoire pouvait être supprimé
et libéré avant d'avoir été purgé des TLB du processeur. Cela pouvait avoir
pour conséquence une utilisation de mémoire de page après libération. Un
utilisateur local doté de l'accès à ce type de périphérique pouvait
exploiter cela pour provoquer un déni de service (plantage ou corruption de
mémoire) ou éventuellement pour une élévation de privilèges.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39842">CVE-2022-39842</a>

<p>Un dépassement d'entier a été découvert dans le pilote vidéo pxa3xx-gcu
qui pouvait conduire à une écriture de tas hors limites.</p>

<p>Ce pilote n'est pas activé dans les configurations du noyau officiel
de Debian.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-40307">CVE-2022-40307</a>

<p>Une situation de compétition a été découverte dans le pilote
capsule-loader d'EFI, qui pouvait conduire à une utilisation de mémoire
après libération. Un utilisateur local autorisé à accéder à ce périphérique
(/dev/efi_capsule_loader) pouvait exploiter cela pour provoquer un déni de
service (plantage ou corruption de mémoire) ou éventuellement pour une
élévation de privilèges. Cependant, ce périphérique est normalement
accessible uniquement au superutilisateur.</p></li>

</ul>

<p>Pour Debian 10 Buster, ces problèmes ont été corrigés dans la version
4.19.260-1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets linux.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de linux, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3131.data"
# $Id: $
