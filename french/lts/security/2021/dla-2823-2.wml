#use wml::debian::translation-check translation="d54e5700d52554f12dad377f3d6d51b152d1587d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La mise à jour de sécurité de Salt, gestionnaire d'exécution à distance,
pour corriger le
<a href="https://security-tracker.debian.org/tracker/CVE-2021-21996">CVE-2021-21996</a>
a introduit une régression dans salt/fileclient.py qui déclenche une
exception inattendue et fait échouer les états <q>file.managed</q>.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans la version
2016.11.2+ds-1+deb9u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets salt.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de salt, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/salt">\
https://security-tracker.debian.org/tracker/salt</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2823-2.data"
# $Id: $
