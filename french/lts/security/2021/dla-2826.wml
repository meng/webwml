#use wml::debian::translation-check translation="3bf4e11065ed406a7ef65591e9611f5133114df3" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans mbed TLS, une
bibliothèque légère de chiffrement et SSL/TLS, qui pourraient avoir pour
conséquences un déni de service, une divulgation d'informations ou des
attaques par canal auxiliaire.</p>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans la
version 2.4.2-1+deb9u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets mbedtls.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de mbedtls, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/mbedtls">\
https://security-tracker.debian.org/tracker/mbedtls</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2826.data"
# $Id: $
