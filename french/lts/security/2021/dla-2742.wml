#use wml::debian::translation-check translation="a4ac150decb05175348ac66d283f420f84a18e75" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs problèmes ont été découverts dans ffmpeg.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-21041">CVE-2020-21041</a>

<p>Une vulnérabilité de dépassement de tampon existe à l'aide de
apng_do_inverse_blend dans libavcodec/pngenc.c, qui pourrait permettre à un
utilisateur distant malveillant de causer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22015">CVE-2020-22015</a>

<p>Une vulnérabilité de dépassement de tampon dans mov_write_video_tag due
à une utilisation hors limites dans libavformat/movenc.c pourrait
permettre à un utilisateur distant malveillant d’obtenir des informations
sensibles, de causer un déni de service ou d’exécuter du code arbitraire.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22016">CVE-2020-22016</a>

<p>Une vulnérabilité de dépassement de tampon de tas dans libavcodec/get_bits.h
lors de l’écriture de fichiers .mov pourrait mener à une corruption de
mémoire et d’autres conséquences seraient possibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22020">CVE-2020-22020</a>

<p>Une vulnérabilité de dépassement de tampon dans la fonction build_diff_map
dans libavfilter/vf_fieldmatch.c pourrait permettre à un utilisateur
distant malveillant de causer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22021">CVE-2020-22021</a>

<p>Une vulnérabilité de dépassement de tampon dans la fonction filter_edges
dans libavfilter/vf_yadif.c pourrait permettre à un utilisateur distant
malveillant de causer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22022">CVE-2020-22022</a>

<p>Une vulnérabilité de dépassement de tampon de tas dans filter_frame
dans libavfilter/vf_fieldorder.c pourrait mener à une corruption de mémoire et
d’autres conséquences seraient possibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22023">CVE-2020-22023</a>

<p>Une vulnérabilité de dépassement de tampon de tas existe dans filter_frame
dans libavfilter/vf_bitplanenoise.c pourrait mener à une corruption de mémoire
et d’autres conséquences possibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22025">CVE-2020-22025</a>

<p>Une vulnérabilité de dépassement de tampon de tas existe dans gaussian_blur
dans libavfilter/vf_edgedetect.c pourrait mener à une corruption de mémoire et
d’autres conséquences seraient possibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22026">CVE-2020-22026</a>

<p>Une vulnérabilité de dépassement de tampon dans la fonction
config_input dans libavfilter/af_tremolo.c pourrait permettre à un utilisateur
distant malveillant de causer un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22028">CVE-2020-22028</a>

<p>Une vulnérabilité de dépassement de tampon dans filter_vertically_8 dans
libavfilter/vf_avgblur.c pourrait causer un déni de service distant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22031">CVE-2020-22031</a>

<p>Une vulnérabilité de dépassement de tampon de tas dans filter16_complex_low
pourrait mener à une corruption de mémoire et d’autres conséquences
seraient possibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22032">CVE-2020-22032</a>

<p>Une vulnérabilité de dépassement de tampon de tas dans gaussian_blur
pourrait mener à une corruption de mémoire et d’autres conséquences
seraient possibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22036">CVE-2020-22036</a>

<p>Une vulnérabilité de dépassement de tampon de tas dans filter_intra dans
libavfilter/vf_bwdif.c pourrait mener à une corruption de mémoire et d’autres
conséquences seraient possibles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3566">CVE-2021-3566</a>

<p>Le démultiplexeur tty ne possédait pas de fonction <q>read_probe</q>
assignée. En contrefaisant un fichier <q>ffconcat</q> valable qui référence
une image, suivi par un fichier qui déclenche le démultiplexeur tty, le
contenu d’un second fichier pourrait être copié dans le verbatim de fichier
produit (à condition que l’option « -vcodec copy » soit passée à ffmpeg).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-38114">CVE-2021-38114</a>

<p>libavcodec/dnxhddec.c ne vérifiait pas la valeur renvoyée par la fonction
init_vlc. Des données DNxHD contrefaites pourraient provoquer un impact non
précisé.</p></li>

</ul>

<p>Pour Debian 9 « Stretch », ces problèmes ont été corrigés dans
la version 7:3.2.15-0+deb9u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ffmpeg.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ffmpeg,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ffmpeg">\
https://security-tracker.debian.org/tracker/ffmpeg</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2742.data"
# $Id: $
