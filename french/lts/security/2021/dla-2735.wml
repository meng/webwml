#use wml::debian::translation-check translation="19654394032eac0381edfe4a1b76ea5446f48f73" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Ceph, un système de
 fichiers et de stockage distribué.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14662">CVE-2018-14662</a>

<p>Des utilisateurs ceph authentifiés autorisés seulement en lecture pourraient
dérober les clés dm-crypt utilisées pour le chiffrement de disque ceph.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16846">CVE-2018-16846</a>

<p>Des utilisateurs authentifiés de passerelle ceph RGW peuvent provoquer un
déni de service à l’encontre d’OMAP exploitant les indices de bucket.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-10753">CVE-2020-10753</a>

<p>Un défaut a été découvert dans le stockage Ceph RadosGW de Red Hat (Ceph
Object Gateway). La vulnérabilité concerne l’injection d’en-têtes HTTP
à l'aide d'une étiquette CORS ExposeHeader. Le caractère de nouvelle ligne
dans l’étiquette ExposeHeader dans le fichier de configuration CORS génère une
injection d’en-tête dans la réponse quand la requête CORS est faite.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1760">CVE-2020-1760</a>

<p>Un défaut a été découvert dans le Ceph Object Gateway, où il gère les
requêtes envoyées par un utilisateur anonyme dans Amazon S3. Ce défaut pourrait
conduire à des attaques XSS possibles à cause du manque de neutralisation
correcte d’entrée non fiable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-3524">CVE-2021-3524</a>

<p>Un défaut a été découvert dans le stockage Ceph RadosGW de Red Hat (Ceph
Object Gateway). La vulnérabilité concerne l’injection d’en-têtes HTTP à l'aide
d'une étiquette CORS ExposeHeader. Le caractère de nouvelle ligne dans
l’étiquette ExposeHeader dans le fichier de configuration CORS génère une
injection d’en-tête dans la réponse quand la requête CORS est faite. De plus,
la correction de bogue précédente pour le CVE-2020-10753 ne prenait pas en
compte l’utilisation de \r comme séparateur d’en-tête, par conséquent un
nouveau défaut était créé.</p></li>

</ul>

<p>Pour Debian 9 « Stretch », ces problèmes ont été corrigés dans
la version 10.2.11-2+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ceph.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ceph,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/ceph">\
https://security-tracker.debian.org/tracker/ceph</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2735.data"
# $Id: $
