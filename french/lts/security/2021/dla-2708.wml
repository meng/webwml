#use wml::debian::translation-check translation="7531cce0aebbc55f14b6ceb271ec23fbfa7879f9" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans php5, un langage de script
embarqué dans du HTML et côté serveur. Un attaquant pourrait causer un déni de
service (DoS), une corruption de mémoire et, éventuellement, une exécution de
code arbitraire, et une contrefaçon de requête coté serveur (SSRF).</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18218">CVE-2019-18218</a>

<p>fileinfo : cdf_read_property_info dans cdf.c ne limite pas le nombre
d’éléments CDF_VECTOR. Cela permet un dépassement de tampon de tas (écriture
hors limites de quatre octets).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7071">CVE-2020-7071</a>

<p>Lors de la validation d’URL avec des fonctions telles que filter_var($url,
FILTER_VALIDATE_URL), PHP accepte un localisateur avec un mot de passe non
valable comme URL valable. Cela peut conduire à des fonctions qui reposent
sur un URL valable à mal analyser un URL et produire des données fausses comme
composants de l’URL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21702">CVE-2021-21702</a>

<p>Lors de l’utilisation d’extension SOAP pour une connexion à un serveur SOAP,
un serveur SOAP malveillant pourrait renvoyer des données XML mal formées
comme réponse qui pourrait faire que PHP accède à un pointeur NULL et par
conséquent provoque un plantage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21704">CVE-2021-21704</a>

<p>Plusieurs problèmes firebird.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21705">CVE-2021-21705</a>

<p>Contournement SSRF dans FILTER_VALIDATE_URL.</p></li>

</ul>

<p>Pour Debian 9 « Stretch », ces problèmes ont été corrigés dans
la version 7.0.33-0+deb9u11.</p>

<p>Nous vous recommandons de mettre à jour vos paquets php7.0.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de php7.0, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/php7.0">\
https://security-tracker.debian.org/tracker/php7.0</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2708.data"
# $Id: $
