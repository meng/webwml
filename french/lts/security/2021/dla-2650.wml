#use wml::debian::translation-check translation="a0c9fc3802a965d0f7f38349b13603e864c053b3" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Qualys Research Labs a signalé plusieurs vulnérabilités dans Exim, un agent
de transport de courrier électronique, qui pourraient avoir pour conséquences
une élévation locale de privilèges et l'exécution de code à distance.</p>

<p>Plus de détails sont disponibles dans l'annonce de Qualys à l'adresse
<a href="https://www.qualys.com/2021/05/04/21nails/21nails.txt">https://www.qualys.com/2021/05/04/21nails/21nails.txt</a>.</p>


<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 4.89-2+deb9u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets exim4.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de exim4, veuillez
consulter sa page de suivi de sécurité à l'adresse : <a href="https://security-tracker.debian.org/tracker/exim4">\
https://security-tracker.debian.org/tracker/exim4</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2650.data"
# $Id: $
