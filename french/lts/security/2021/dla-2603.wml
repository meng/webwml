#use wml::debian::translation-check translation="a7c602a9d7a12f16d891dea3c5c8ef3bc885f12a" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un certain nombre de vulnérabilités dans
<tt>libmediainfo</tt>, une bibliothèque de lecture des métadonnées, telles que
les noms de piste, les tailles, etc., de fichiers de média.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11372">CVE-2019-11372</a>

<p>Une lecture hors limites dans MediaInfoLib::File__Tags_Helper::Synched_Test
dans Tag/File__Tags.cpp dans MediaInfoLib dans MediaArea MediaInfo 18.12 conduit
à un plantage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11373">CVE-2019-11373</a>

<p>Une lecture hors limites dans File__Analyze::Get_L8 dans
File__Analyze_Buffer.cpp dans MediaInfoLib dans MediaArea MediaInfo 18.12
conduit à un plantage.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15395">CVE-2020-15395</a>

<p>Dans MediaInfoLib dans MediaArea MediaInfo 20.03, il existe une lecture hors
limites de tampon de pile dans Streams_Fill_PerStream dans
Multiple/File_MpegPs.cpp (c'est-à-dire un décalage d'entier lors de l’analyse
de MpegPs).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26797">CVE-2020-26797</a>

<p>Mediainfo avant la version 20.08 avait une vulnérabilité de dépassement de
tampon de tas à travers MediaInfoLib::File_Gxf::ChooseParser_ChannelGrouping.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 0.7.91-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libmediainfo.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2603.data"
# $Id: $
