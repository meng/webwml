#use wml::debian::translation-check translation="45cd9cdd5db046382284b2aec5ea2f67a21dff78" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour inclut les modifications dans tzdata 2023b pour les
liaisons de Perl. Pour la liste des changements, veuillez consulter la
DLA-3366-1.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1:2.23-1+2023b.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libdatetime-timezone-perl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libdatetime-timezone-perl,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libdatetime-timezone-perl">\
https://security-tracker.debian.org/tracker/libdatetime-timezone-perl</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3367.data"
# $Id: $
