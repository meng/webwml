#use wml::debian::translation-check translation="6879f1715c9df4f7b8ec4c7a489b8799165d576f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Cette mise à jour inclut les modifications dans tzdata 2023c. Celles notables
sont :</p>

<ul>
<li>annulation des modifications d’heure d’été au Liban ;</li>
<li>mise à jour de la liste de secondes intermédiaires.
</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2021a-0+deb10u11.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tzdata.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de tzdata,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/tzdata">\
https://security-tracker.debian.org/tracker/tzdata</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3412.data"
# $Id: $
