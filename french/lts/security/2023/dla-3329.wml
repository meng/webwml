#use wml::debian::translation-check translation="f92cf68ec8467e81591a3a4f3139c59f8c7316fb" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité de déni de service dans
Django, un cadriciel de développement basé sur Python.</p>

<p>Le passage de certaines entrées à des formulaires multiparties pouvait
aboutir à l’ouverture de trop de fichiers ou à un épuisement de mémoire, et
fournissait un vecteur potentiel pour une attaque par déni de service.</p>

<p>Le nombre de parties de fichier analysées est désormais limité à l’aide d’un
nouveau réglage de DATA_UPLOAD_MAX_NUMBER_FILES.</p>


<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-24580">CVE-2023-24580</a>

<p>Un problème a été découvert dans l’analyse de requêtes multiparties dans
Django, versions 3.2 avant 3.2.18, 4.0 avant 4.0.10, et 4.1 avant 4.1.7. Le
passage de certaines entrées (par exemple, un nombre excessif de parties) à des
formulaires multiparties pouvait aboutir à l’ouverture de trop de fichiers ou
à un épuisement de mémoire, et fournissait un vecteur potentiel pour une attaque
par déni de service.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1:1.11.29-1+deb10u7.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python-django.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3329.data"
# $Id: $
