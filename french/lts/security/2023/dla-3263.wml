#use wml::debian::translation-check translation="0a09d771b95ab989f0eb9f01a29261201ccfc4a2" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait un problème de décalage unitaire dans la taille d'un
tableau dans libtasn1-6, une bibliothèque pour gérer des structures de données
génériques ASN.1.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-46848">CVE-2021-46848</a>

<p>GNU Libtasn1 avant la version 4.19.0 avait une erreur de décalage
unitaire ETYPE_OK dans la vérification de la taille d'un tableau qui affectait asn1_encode_simple_der.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 4.13-3+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libtasn1-6.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3263.data"
# $Id: $
