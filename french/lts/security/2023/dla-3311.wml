#use wml::debian::translation-check translation="dd702a5786c6e1a4149e57aceedc2125dab5fa68" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Helmut Grohne a découvert un défaut dans Heimdal, une implémentation de
Kerberos 5 qui vise à être compatible avec Kerberos du MIT. Le rétroportage
des corrections pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2022-3437">CVE-2022-3437</a>
inversait accidentellement d'importantes comparaisons de mémoire dans les
gestionnaires de vérification d'intégrité <code>arcfour-hmac-md5</code> et
<code>rc4-hmac</code> pour gssapi, avec pour conséquence la validation incorrecte
de codes d'intégrité de message.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 7.5.0+dfsg-3+deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets heimdal.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de heimdal,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/heimdal">\
https://security-tracker.debian.org/tracker/heimdal</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3311.data"
# $Id: $
