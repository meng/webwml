#use wml::debian::translation-check translation="e2eba9b0224ea8e753bc66ac485e83bca227d8eb" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Un problème a été découvert dans Glance, un service et démon d’images
d’OpenStack. En fournissant une image simple VMDK spécialement créée qui
référençait un chemin spécifique de fichier de sauvegarde, un utilisateur
authentifié pouvait persuader des systèmes de renvoyer une copie du contenu de
fichier par le serveur, aboutissant à un accès non autorisé à des données
éventuellement sensibles.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 2:17.0.0-5+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets glance.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de glance,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/glance">\
https://security-tracker.debian.org/tracker/glance</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3300.data"
# $Id: $
