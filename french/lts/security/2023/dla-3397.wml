#use wml::debian::translation-check translation="3f36d56431a23428895f0b695effebff2dc20540" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité de déni de service
potentiel dans connman, un gestionnaire de réseau en ligne de commande conçu
pour être utilisé dans les périphériques embarqués.</p>

<p>Des attaquants, proches du point de vue réseau, opérant un serveur DHCP
contrefait pouvaient provoquer un dépassement de pile, aboutissant à un déni
de service en interrompant le processus connman.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28488">CVE-2023-28488</a>

<p>client.c dans gdhcp dans ConnMan jusqu’à la version 1.41 pouvait être
utilisé par des attaquants proches du point de vue réseau (opérant un serveur
DHCP contrefait) pour provoquer un dépassement de pile et un déni de service en
interrompant le processus connman.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 1.36-2.1~deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets connman.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3397.data"
# $Id: $
