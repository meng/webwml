#use wml::debian::translation-check translation="2d054eab3367d1345c959903fc30c43cd5d2850d" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Des vulnérabilités de script intersite (XSS) ont été trouvées dans rainloop,
un client de courriel basé sur le web, qui pouvaient conduire à une divulgation
d'informations dont la phrase de passe.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13389">CVE-2019-13389</a>

<p>Il a été découvert que le portail de messagerie RainLoop manquait de
mécanismes de protections XSS tels que la validation <code>xlink:href</code>,
l’en-tête X-XSS-Protection et l’en-tête Content-Security-Policy.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-29360">CVE-2022-29360</a>

<p>Simon Scannell a découvert que l’afficheur de courriels de RainLoop
permettait un script intersite à l’aide d’un message de courriel text/html
contrefait.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 1.12.1-2+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets rainloop.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de rainloop,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/rainloop">\
https://security-tracker.debian.org/tracker/rainloop</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3435.data"
# $Id: $
