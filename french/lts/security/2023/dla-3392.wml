#use wml::debian::translation-check translation="a25f2168877bdbbce34a307f59ef551538896b19" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux problèmes dans ruby-rack ont été corrigés :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-27530">CVE-2023-27530</a>

<p>Description : limitations de toutes les parties <q>multipart</q>, pas
seulement des fichiers.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-27539">CVE-2023-27539</a>

<p>Description : découpage des en-têtes selon les virgules, puis dépouillement
des chaines pour éviter des problèmes de déni de service à l’aide d’expressions
rationnelles.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.0.6-3+deb10u3.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby-rack.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ruby-rack,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ruby-rack">\
https://security-tracker.debian.org/tracker/ruby-rack</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3392.data"
# $Id: $
