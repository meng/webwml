#use wml::debian::translation-check translation="83b3819e19db4d7805684dcae2e7b0490aeb7b34" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Il a été découvert qu’il existait une vulnérabilité potentielle de déni de
service à distance dans Redis, une base de données populaire clé-valeur
<q>non SQL</q>.</p>

<p>Des utilisateurs authentifiés pouvaient utiliser la commande
<code>HINCRBYFLOAT</code> pour créer un champ de hachage non valable qui
plantait le serveur Redis lors de l’accès.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28856">CVE-2023-28856</a>

<p>Redis est une base de données en mémoire, au code source ouvert, qui persiste
sur le disque. Des utilisateurs authentifiés pouvaient utiliser la commande
<q>HINCRBYFLOAT</q> pour créer un champ de hachage non valable qui plantait
Redis lors de l’accès dans les versions affectées. Ce problème est corrigé
dans les versions 7.0.11, 6.2.12, et 6.0.19. Il est conseillé de mettre à
niveau. Aucun contournement de ce problème n’est connu.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 5:5.0.14-1+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets redis.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3396.data"
# $Id: $
