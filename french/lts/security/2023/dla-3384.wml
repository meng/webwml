#use wml::debian::translation-check translation="5e83c6caec4bb5412fb5bbcc2a5edb307627c0d1" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités de sécurité ont été découvertes dans le moteur
de servlet et JSP Tomcat.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42252">CVE-2022-42252</a>

<p>Apache Tomcat a été configuré pour ignorer les en-têtes non valables à l’aide
du réglage rejectIllegalHeader à <q>false</q>. Tomcat ne rejetait pas les
requêtes contenant un en-tête Content-Length non valable, rendant une attaque
par dissimulation de requête possible si Tomcat était situé derrière un
mandataire inverse qui échouait à rejeter la requête avec l’en-tête non
valable.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28708">CVE-2023-28708</a>

<p>Lors de l’utilisation de RemoteIpFilter avec des requêtes reçues d’un
mandataire inverse à travers HTTP qui incluaient l’en-tête X-Forwarded-Proto
réglé à https, les cookies de session créés par Apache Tomcat n’incluaient pas
l’attribut de sécurité. Cela pouvait aboutir à ce que l’agent utilisateur
transmette le cookie de session à travers un canal non sécurisé.</p></li>

</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 9.0.31-1~deb10u8.</p>

<p>Nous vous recommandons de mettre à jour vos paquets tomcat9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de tomcat9,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/tomcat9">\
https://security-tracker.debian.org/tracker/tomcat9</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur : <a rel="nofollow
"href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3384.data"
# $Id: $
