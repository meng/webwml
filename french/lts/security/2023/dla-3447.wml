#use wml::debian::translation-check translation="cf7ca80be383e43ff25cf3a62ef2cea97cc2e249" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Deux problèmes de déni de service par expression rationnelle (ReDoS) ont été
découverts dans Ruby : le premier dans le composant URI et le second dans le
module Time. Chacun de ces problèmes pouvait aboutir dans une augmentation
considérable de temps d’exécution pour une entrée malveillante.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28755">CVE-2023-28755</a>

<p>Un problème de déni de service a été découvert dans le composant URI jusqu’à
la version 0.12.0 dans la version 3.2.1 de Ruby. L'analyseur d’URI gérait
incorrectement les URL non valables ayant des caractères spécifiques. Il
provoquait une augmentation de temps d’exécution d’analyse des chaines d’objets
d’URI. Les versions corrigées sont 0.12.1, 0.11.1, 0.10.2 et 0.10.0.1.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-28756">CVE-2023-28756</a>

<p>Un problème de déni de service a été découvert dans le composant Time jusqu’à
la version 0.2.1 dans Ruby 3.2.1. L'analyseur de Time gérait
incorrectement les URL non valables ayant des caractères spécifiques. Il
provoquait une augmentation de temps d’exécution d’analyse des chaines d’objets
d’URI. Les versions corrigées sont 0.1.1 et 0.2.2.</p></li>
</ul>

<p>Pour Debian 10 <q>Buster</q>, ces problèmes ont été corrigés dans
la version 2.5.5-3+deb10u5.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby2.5.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3447.data"
# $Id: $
