#use wml::debian::translation-check translation="053c955429e5b0b0e6c2f2c9f378ffa91f56317f" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>La mise à jour précédente d’ImageMagick provoquait une régression dans
quelques paquets de Perl due à un durcissement restrictif excessif dans une
mise à jour de politique (la lecture depuis /etc/ était interdite). Ce correctif
de durcissement a été retiré.</p>

<p>Pour Debian 10 <q>Buster</q>, ce problème a été corrigé dans
la version 8:6.9.10.23+dfsg-2.1+deb10u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets imagemagick.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de imagemagick,
veuillez consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/imagemagick">\
https://security-tracker.debian.org/tracker/imagemagick</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3357-2.data"
# $Id: $
