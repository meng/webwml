#use wml::debian::translation-check translation="a2d9cd8be3a6fafa24ac80e46ee4314460bc6713" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Il a été découvert que la fonction wcsnrtombs, dans toutes les versions de
libc de musl jusqu’à la version 1.2.1, comportait plusieurs bogues dans la
gestion de la taille du tampon de destination lors de la limitation du nombre de
caractères d’entrée. Cela pouvait conduire à une boucle infinie sans
progression (pas de débordement) ou à une écriture après la fin des tampons de
destination.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 1.1.16-3+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets musl.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de musl, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/musl">https://security-tracker.debian.org/tracker/musl</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2474.data"
# $Id: $
