#use wml::debian::translation-check translation="4cb1aff654b9eb7583cad54da851b00adc9af391" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Une vulnérabilité de dissimulation éventuelle de requête HTTP dans WEBrick
a été signalée.</p>

<p>WEBrick (fourni avec ruby2.3) était trop indulgent avec un en-tête
Transfer-Encoding non valable. Cela pouvait conduire à une interprétation
incompatible entre WEBrick et certains serveurs mandataires HTTP, et pouvait
permettre à un attaquant de passer subrepticement une requête.</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 2.3.3-1+deb9u9.</p>

<p>Nous vous recommandons de mettre à jour vos paquets ruby2.3.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de ruby2.3, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/ruby2.3">https://security-tracker.debian.org/tracker/ruby2.3</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2391.data"
# $Id: $
