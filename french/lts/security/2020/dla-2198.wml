#use wml::debian::translation-check translation="6b9da960d89b6748e51556ddcad380d48e90b7e8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans otrs2 (système au code
source ouvert de requêtes par tickets)</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1770">CVE-2020-1770</a>

<p>La prise en charge de fichiers produits par grappe pouvait contenir des
informations sensibles qui devraient demeurer confidentielles.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1772">CVE-2020-1772</a>

<p>Il était possible d’élaborer des requêtes de mot de passe perdu avec des
jokers dans la valeur du jeton. Cela permettait à un attaquant de récupérer un
ou des jetons valables, générés par des utilisateurs ayant déjà demandé un
nouveau mot de passe.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-1774">CVE-2020-1774</a>

<p>Lorsqu’un utilisateur téléchargeait un certificat ou une clé PGP ou S/MIME, un
fichier exporté possédait le même nom pour la clé publique et privée. Il était
donc possible de les combiner et d’envoyer la clé privée à la partie tierce au
lieu de la clé publique.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 3.3.18-1+deb8u15.</p>

<p>Nous vous recommandons de mettre à jour vos paquets otrs2.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2198.data"
# $Id: $
