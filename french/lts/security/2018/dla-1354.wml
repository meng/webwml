#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Deux vulnérabilités ont été trouvés dans la bibliothèque OpenCV, <q>Open
Computer Vision</q>.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5268">CVE-2018-5268</a>

<p>Dans OpenCV 3.3.1, un dépassement de tampon basé sur le tas se produit dans
cv::Jpeg2KDecoder::readComponent8u dans modules/imgcodecs/src/grfmt_jpeg2000.cpp
lors de l’analyse d’un fichier d’image contrefait.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5269">CVE-2018-5269</a>

<p>Dans OpenCV 3.3.1, un échec d’assertion se produit dans cv::RBaseStream::setPos
dans modules/imgcodecs/src/bitstrm.cpp à cause d’un forçage de type entier
incorrect.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 2.3.1-11+deb7u4.</p>
<p>Nous vous recommandons de mettre à jour vos paquets opencv.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1354.data"
# $Id: $
