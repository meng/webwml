#use wml::debian::template title="Comment nous rejoindre"
#use wml::debian::translation-check translation="5eef343480ec39f5bb8db5e4e69bbb9e8b926c9b" maintainer="Jean-Paul Guillonneau"
# Translators:
# Jérôme Schell, 2001, 2002.
# Nicolas Boullis, 2002.
# Philippe Batailler, 2002.
# Frederic Bothamy, 2004.
# Nicolas Bertolissio, 2005, 2006.
# David Prévot, 2010-2014.
# Jean-Paul Guillonneau 2019, 2021

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Nous sommes toujours à la
recherche de nouveaux contributeurs et bénévoles. Nous encourageons fortement
tout le monde à <a href="$(HOME)/intro/diversity">participer</a>. Prérequis :
un intérêt dans le logiciel libre et du temps libre.</p>
</aside>

<ul class="toc">
<li><a href="#reading">Lire</a></li>
<li><a href="#contributing">Contribuer</a></li>
<li><a href="#joining">Rejoindre</a></li>
</ul>


<h2><a id="reading">Lire</a></h2>

<p>
Si ce n'est déjà fait, vous pouvez parcourir les sites en lien sur la
<a href="$(HOME)">page d’accueil</a> de Debian. Cela peut vous aider à mieux
comprendre ce que nous sommes et ce que nous essayons de faire. En tant que
contributeur potentiel à Debian, prêtez particulièrement attention à ces
deux pages :
</p>

<ul>
  <li><a href="$(HOME)/social_contract#guidelines">principes du logiciel libre
   selon Debian</a> ;</li>
  <li><a href="$(HOME)/social_contract">contrat social de Debian</a>.</li>
</ul>

<p>
La communication dans le projet se passe en grande partie au sein de nos
<a href="$(HOME)/MailingLists/">listes de diffusion</a>. Si vous souhaitez
ressentir la vie interne du projet Debian, vous devriez vous abonner au moins à
<a href="https://lists.debian.org/debian-devel-announce/">debian-devel-announce</a>
et <a href="https://lists.debian.org/debian-news/">debian-news</a>. Ces deux
listes ont un faible trafic mais vous tiennent informé de ce qui se passe dans la
communauté. Les
<a href="https://www.debian.org/News/weekly/">nouvelles du projet Debian</a>,
publiées aussi sur la liste debian-news, synthétisent les discussions récentes
des listes de diffusion relatives à Debian et des blogs et fournissent aussi des
liens vers eux.
</p>

<p>
En tant que développeur éventuel, vous devriez souscrire à
<a href="https://lists.debian.org/debian-mentors/">debian-mentors</a>. Ici
vous pouvez poser des questions sur l’empaquetage et l’infrastructure du projet
ainsi que sur les autres sujets relatifs aux développeurs. Notez que cette
liste est destinée aux nouveaux contributeurs, mais pas aux utilisateurs. Les
autres listes particulièrement intéressantes sont
<a href="https://lists.debian.org/debian-devel/">debian-devel</a> (sujets
techniques concernant le développement), <a
href="https://lists.debian.org/debian-project/">debian-project</a> (discussions
à propos de problèmes non techniques dans le projet), <a
href="https://lists.debian.org/debian-release/">debian-release</a>
(coordination des publications de Debian), <a
href="https://lists.debian.org/debian-qa/">debian-qa</a> (assurance qualité).
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-envelope fa-2x"></span> <a href="$(HOME)/MailingLists/subscribe">Abonnement aux listes de diffusion</a></button></p>

<p>
<strong>Astuce</strong> : si vous voulez réduire le nombre de courriels reçus,
particulièrement pour les listes à haut trafic, nous proposons des résumés de
courrier (debian-user-digest) sous forme de courriels récapitulatifs au lieu de
messages individuels. Vous pouvez visiter les
<a href="https://lists.debian.org/">archives des listes de diffusion</a> pour
lire leurs messages dans un navigateur web.
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span>Il n’est nul besoin d’être
un développeur officiel de Debian (DD) pour contribuer. Par contre, un DD
existant peut agir comme <a href="newmaint#Sponsor">parrain</a> et aider à
incorporer votre travail dans le projet.</p>
</aside>

<h2><a id="contributing">Contribuer</a></h2>

<p>
Vous êtes intéressés dans l’entretien de paquets ? Alors regardez notre liste
<a href="$(DEVEL)/wnpp/">paquets en souffrance et paquets souhaités</a>. Ici
vous trouverez des paquets ayant besoin d’un (nouveau) responsable. Reprendre
un paquet abandonné est le meilleur moyen de commencer sa carrière de
responsable Debian. Non seulement vous aidez notre distribution, mais cela
vous donne l’opportunité d'apprendre grâce au travail du précédent responsable.
</p>

<p>
Voici quelques autres idées pour contribuer à Debian :
</p>

<ul>
  <li>Aidez à écrire de la <a href="$(HOME)/doc/">documentation</a> ;</li>
  <li>Aidez à entretenir le <a href="$(HOME)/devel/website/">site web</a> de
Debian, produire du contenu, éditer ou traduire le texte existant ;</li>
  <li>Rejoindre nos <a href="$(HOME)/international/">équipes de traduction</a> ;</li>
  <li>Améliorer Debian et rejoindre <a href="https://qa.debian.org/">l’équipe
d’assurance qualité (QA)</a>.</li>
</ul>

<p>
Bien sûr, vous pouvez faire plein d’autres choses et nous sommes constamment à
la recherche de personnes pour offrir une aide juridique ou pour rejoindre
notre <a href="https://wiki.debian.org/Teams/Publicity">équipe de Debian pour
la publicité</a>. Devenir membre d’une équipe de Debian est un excellent
moyen d’acquérir de l’expérience avant de commencer le processus de
<a href="newmaint">nouveau membre</a>. C’est aussi un bon point de départ pour
la recherche d’un parrain. Alors, trouvez une équipe et lancez-vous !
</p>

<p style="text-align:center"><button type="button"><span class="fa fa-users fa-2x"></span> <a href="https://wiki.debian.org/Teams">Liste des équipes de Debian</a></button></p>


<h2><a id="joining">Rejoindre</a></h2>

<p>
Ainsi vous avez contribué au projet Debian depuis un certain temps et vous
voulez obtenir un rôle plus officiel ? Principalement, deux options s’offrent
à vous :
</p>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Le rôle de responsable Debian
 (Debian Maintainer — DM) a été introduit en 2007. Auparavant, le seul rôle
officiel était développeur Debian (Debian Developer — DD). Actuellement, deux
processus indépendants existent pour postuler pour les deux rôles.</p>
</aside>

<ul>
  <li><strong>Responsable Debian (DM) :</strong> c’est la première étape. Comme
DM, vous pouvez téléverser vos propres paquets dans l’archive de Debian (avec
quelques restrictions). Contrairement aux responsables parrainés, les
responsables peuvent entretenir des paquets sans l’aide d’un parrain. <br>Plus
d’informations : <a href="https://wiki.debian.org/DebianMaintainer">wiki du
responsable Debian</a></li>
  <li><strong>Développeurs Debian (DD) :</strong> c’est le rôle traditionnel et
complet de participation dans Debian. Un DD peut participer à n'importe quelle
élection Debian. Le DD téléverseur peut téléverser n’importe quel paquet dans
l’archive. Avant d’exercer comme DD téléverseur, vous devez justifier de
l’entretien de paquets depuis au moins six mois, par exemple, téléversements
de paquets en tant que DM, aide à l’intérieur d’une équipe ou entretien de
paquets téléversés par un parrain. Un DD non téléverseur a les mêmes droits
d’empaquetage qu’un responsable Debian (DM). Avant de devenir un DD non
téléverseur, vous devez avoir travaillé de manière visible et significative
dans le projet. <br>Plus d’informations : <a href="newmaint">coin des nouveaux
membres Debian</a></li>
</ul>

<p>Indépendamment du rôle que vous choisirez, vous devez être familier avec
les procédures de Debian et il est donc recommandé de lire
<a href="$(DOC)/debian-policy/">la charte Debian</a> et
<a href="$(DOC)/developers-reference/">la référence du Développeur</a>
avant de postuler.</p>
