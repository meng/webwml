#use wml::debian::template title="Programme de Jonathan Carter" BARETITLE="true" NOHEADER="true"
#include "$(ENGLISHDIR)/vote/style.inc"
#use wml::debian::translation-check translation="cae583a87602d42ff8b184e925304aacb08c788d" maintainer="Jean-Pierre Giraud"

<DIV class="main">
<TABLE CLASS="title">
<TR><TD>
<BR>
<H1 CLASS="titlemain"><BIG><B>Jonathan Carter</B></BIG><BR>
    <SMALL>Programme de DPL</SMALL><BR>
    <SMALL>2019-03-17</SMALL>
</H1>
<H3>
<A HREF="mailto:jcc@debian.org"><TT>jcc@debian.org</TT></A><BR>
<A HREF="https://jonathancarter.org"><TT>https://jonathancarter.org</TT></A><BR>
<A HREF="https://wiki.debian.org/highvoltage"><TT>https://wiki.debian.org/highvoltage</TT></A>
</H3>
</TD></TR>
</TABLE>

<H2 CLASS="section">Résumé</H2>
<P>Bonjour, je suis <A HREF="https://wiki.debian.org/highvoltage">Jonathan Carter</A>, également connu sous le nom de <EM>highvoltage</EM> et ayant le compte Debian <EM>jcc</EM>, et je suis candidat aux élections de responsable du projet Debian (DPL). </P><DIV CLASS="summary">
<B>Objectifs généraux en tant que DPL</B>:
<UL CLASS="itemize">
<LI CLASS="li-itemize"> Mon but n'est pas de m'attaquer à des objectifs inaccessibles et ambitieux. Au lieu de cela, je compte améliorer la communication pour que notre communauté puisse régler les problèmes existants et pour favoriser l'émergence d'idées.</LI>
<LI CLASS="li-itemize"> Je souhaite promouvoir la recherche d'une meilleure qualité partout dans Debian, que ce soit dans nos produits finis ou dans nos processus communautaires.</LI>
<LI CLASS="li-itemize"> Je veux que Debian soit un projet attirant pour les nouveaux contributeurs. Chaque développeur Debian devrait être immensément fier d'être associé à ce projet.</LI>
<LI CLASS="li-itemize"> Il peut sembler que je joue la sécurité et que mon programme n'est pas très ambitieux, mais je pense que de grandes choses peuvent être accomplies avec de petites étapes et j'espère transmettre cette idée dans mon programme.</LI>
</UL>
</DIV>

<H2 CLASS="section">1. Introduction</H2>

<H3 CLASS="subsection">1.1. Qui je suis</H3>

<div style="min-height: 200px;">
<img style="float: left; padding-right: 10px;" src="jcc.jpg" />
<P>J'ai 37 ans et je vis au Cap, en Afrique du Sud, d'où je suis originaire.</P>
<P>Je travaille à temps partiel pour un institut éducatif où je suis administrateur système et où je travaille sur une distribution dérivée de Debian que le réseau institutionnel utilise dans tous ses centres en Afrique. Je travaille sur Debian le reste de mon temps en tant que travailleur indépendant.</P>
<P> Je suis un défenseur de longue date du logiciel libre ayant travaillé dans les secteurs commercial, associatif, public et éducatif au cours des 15 dernières années.
</div>

<H3 CLASS="subsection">1.2 Brève histoire avec Debian</H3>

<P>J'ai commencé à utiliser Linux en 1999. En 2003, mon enthousiasme m'a donné envie de l'enseigner à des enfants dans les écoles. J'ai appris qu'une association locale était en demande d'une telle initiative et j'ai commencé à échanger avec eux, à les aider bénévolement, à faire quelques missions payées avant de finalement y travailler à plein temps. Mon collègue de l'époque, Thomas Black, m'a de nouveau présenté Debian. Je n'avais pas été impressionné par Debian auparavant car son installateur, boot-floppies, ne fonctionnait presque jamais pour moi et tout était dépassé quand il fonctionnait. Thomas m'a présenté Debian unstable et j'en suis devenu accro. Il m'a dit que je devrais apprendre à créer des paquets et déposer ma candidature au statut de développeur Debian pour devenir « highvoltage@debian.org ». Quand il me l'a proposé, cela m'est paru comme un concept totalement étranger car je n'avais jamais envisagé de faire partie de quelque chose d'aussi cool qu'un projet de distribution Linux, mais l'idée est restée fermement implantée dans mon esprit.</P>

<P>Plus tard cette année, la première version d'Ubuntu est sortie et j'ai fait un petit détour de Debian vers Ubuntu pendant quelques années.</P>

<P>Pendant DebConf7, j'ai découvert les retransmissions vidéo en direct et je suis devenu accro. J'ai aussi lu le nouveau guide du responsable pour la première fois à cette époque. En 2012, je me suis rendu à ma première DebConf au Nicaragua. Cela a été une excellente expérience et en 2016 nous avons accueilli avec succès une DebConf au Cap. En 2017, je suis devenu membre du tout nouveau comité DebConf. J'ai fait diverses contributions à DebConf au fil des années, même des <a href="https://jonathancarter.org/files/debian/debconf/pollito-adventure.pdf">histoires</a> <a href="https://jonathancarter.org/files/debian/debconf/pollito-dc16-guide.pdf">graphiques</a> à propos du poulet de DebConf.</P>

<P>Je suis devenu responsable Debian en 2016 puis développeur Debian en 2017. Cela fait de moi un DD plutôt jeune en termes d'années d'appartenance au projet. J'entretiens activement plus de 60 paquets et j'ai récemment rejoint le projet debian-live pour aider à améliorer la qualité de nos images autonomes.</P>

<P>J'essaie de résumer mes activités liées à Debian sur ma <a href="https://wiki.debian.org/highvoltage">page de wiki</a>, bien qu'il ne soit pas toujours facile de la maintenir à jour. Cette page fait également le lien vers les récapitulatifs mensuels de mes activités dans le logiciel libre.</P>

<H3 CLASS="subsection"> 1.3. Confiance dans Debian</H3>

<P>J'aime le concept de « système d'exploitation universel ». Pour moi, cela signifie que Debian peut s'adapter à différentes technologies, situations et cas d'utilisation. Je suis sans cesse épaté par le travail formidable que les développeurs et tous les contributeurs Debian accomplissent au quotidien.</P>

<P>Quand Debian a été fondée, l'utilisateur classique d'ordinateur était occupé à migrer de MS-DOS vers Windows 3.1 et à apprendre à utiliser une souris. Windows 95 n'était pas encore sorti. Depuis, tant de choses ont changé. L'utilisateur classique d'internet sait ce qu'est un VPN. Nous sommes tous conscients des dangers de la surveillance de masse et la technologie qui était à la base prévue pour résoudre tous les problèmes du monde est maintenant souvent utilisée contre notre espèce dans son ensemble.</P>

<P>Des changements extrêmement positifs sont aussi en train de se produire. La <a href="https://riscv.org/">fondation RISC-V</a> a été formée, ce qui représente un grand pas vers le développement de processeurs matériels libres haut de gamme. Son travail a également conduit <a href="https://wavecomp.ai/mips-open/">MIPS à suivre le mouvement</a> en annonçant qu'ils publieront bientôt des versions ouvertes du jeu d'instructions MIPS. En même temps, Purism travaille à la publication d'un <a href="https://puri.sm/products/librem-5/">téléphone entièrement libre.</a></P>

<P>Avec toutes les bonnes et mauvaises choses sur notre radar, Debian est plus pertinente que jamais. Le monde a besoin d'un système entièrement libre proposant des versions stables avec des mises à jour de sécurité et faisant de ses utilisateurs sa priorité, tout en étant compatible avec les activités commerciales mais sans intention d'entreprise cachée. Debian est unique et belle et importante, et nous ne devrions pas laisser ce message se perdre dans tout le bruit qui nous entoure.</P>

<P>Je veux que nous nous concentrions sur notre passion commune et notre vision pour Debian et pas sur les petits détails pour lesquels nous ne sommes pas du même avis. Trop souvent, les petites choses prennent des proportions démesurées à cause d'une petite minorité ou des médias. Nous ne pouvons pas laisser cela noyer toute la positivité et tout le bien dans notre projet qui motivent nos contributeurs à produire un si bon travail au quotidien.</P>

<H2 CLASS="section">2. Ma déclaration de mission en tant que DPL</H2>

<H3> 2.1. Objectifs de haut niveau</H3>

<OL>
<LI><B>Je n'ai pas l'intention de résoudre tous les problèmes de Debian pendant ce mandat : </B>je crois qu'il est plus bénéfique de s'unir en tant que communauté et de travailler ensemble pour se concentrer sur nos problèmes principaux les plus urgents et de partir de ceux-ci.</LI>

<LI><B>Je ne veux pas que mon rôle de DPL soit purement administratif : </B>je pense qu'il est critique d'avoir quelqu'un dans le projet ayant une vue détachée du projet Debian dans son ensemble, car nous sommes souvent accaparés par nos propres problèmes en tant qu'individus dans le projet.</LI>

<LI><B>Être un DPL facilitateur : </B>certaines de mes idées peuvent être mises en œuvre sans être responsable du projet, mais être DPL apporte une certaine visibilité et une attention qui simplifient la conduite de certaines idées.</LI>

<LI><B>Faire des processus communautaires des citoyens de première classe dans Debian : </B>autant que le sont nos processus techniques.</LI>
<LI><B>Rendre le DPL plus accessible : </B>les précédents DPL ont été excellents pour communiquer sur leur travail avec le projet, je souhaite aussi favoriser la communication dans l'autre sens.</LI>
<LI><B>Promouvoir de nouvelles idées et l'expérimentation dans le projet : </B> suivre les idées les plus demandées dans Debian et les mettre en avant pour les équipes qui pourraient vouloir les implémenter.</LI>
<LI><B>Entretenir notre communauté : </B>il est important d'avoir plus de contributeurs, mais je pense qu'il est aussi important de rendre la contribution à Debian agréable pour les développeurs actuels. Quand les choses ne vont pas bien, il semble futile de recruter de nouveaux développeurs. Si nous faisons de Debian un projet auquel il est fantastique de participer pour tous nos développeurs, nous allons naturellement attirer plus de contributeurs.</LI>
<LI><B>Les petits détails comptent : </B>nous nous sommes trop habitués aux petites choses qui ne fonctionnent pas. Quand vous y êtes habitué, vous avez aussi l'habitude de les contourner. Du point de vue des nouveaux venus en revanche, l'expérience de Debian peut souvent devenir très désagréable à cause de cette multitude de petits problèmes.</LI>
</OL>


<h3> 2.2. Exécution</H3>

<P>Dans l'ensemble, je veux aider Debian à devenir meilleure pour aborder les problèmes qui nous affectent, sans retomber dans les célèbres discussions intimidantes que nous avons eues par le passé. Je veux dire que je préfère corriger notre communication et nos mécanismes internes de résolution de problèmes que nos problèmes eux-mêmes. J'espère promouvoir des façons de travailler qui seront encore bénéfiques à Debian bien après mon mandat de DPL.</P>

<P>Ci-dessous se trouvent certaines des idées que j'envisage de mettre en œuvre afin d'atteindre les objectifs décrits précédemment. Elles ne sont pas gravées dans le marbre et je vais consulter la communauté dans son ensemble quand je les mettrai en œuvre. Si elles sont jugées inappropriées, je n'aurai aucun problème à faire machine arrière sur n'importe quelle idée et nous pourrons trouver d'autres moyens de résoudre les problèmes.</P>

<OL>
<LI><B>Promouvoir #debian-meeting et l'utiliser comme salle de réunion :</B> beaucoup d'équipes ont des réunions IRC dans leurs propres canaux. Cela apporterait beaucoup de visibilité si les équipes de Debian utilisaient à la place un créneau horaire dans le canal IRC #debian-meeting. Non seulement cela simplifierait la tâche des parties intéressées pour suivre ce qu'il se passe dans l'historique, mais cela pourrait aussi attirer de nouveaux membres dans votre équipe en les familiarisant avec vos problèmes.</LI>
<LI><B>Rencontres hebdomadaires de la communauté :</B> avoir une rencontre communautaire sur tout le projet de façon hebdomadaire avec les DPL et ses assistants (quelle que soit la forme sous laquelle ils participent). Son heure varierait pour faciliter la participation des personnes dans des fuseaux horaires différents. L'idée est de mettre le projecteur sur les questions les plus pressantes concernant la communauté et travailler à des solutions. Je suis sûr que les gens redoutent cette idée à cause des trolls, mais nous pouvons modérer et/ou limiter le nombre de participants en cas de besoin.</LI>

<LI><B>Implémenter une campagne <q>100 papercuts</q> :</B> créer un projet pour identifier les 100 petits problèmes les plus ennuyeux de Debian. Ceux-ci seraient typiquement des éléments pouvant être résolus en une journée de travail. Il pourrait s'agir d'un bogue technique ou d'un problème dans le projet Debian lui-même. Le but serait de corriger cent de ces bogues d'ici la sortie de Buster (Debian 10). Un nouveau cycle peut aussi être démarré comme une partie d'un processus itératif basé sur le temps et/ou le nombre de bogues restants.</LI>
<LI><B>Projet de mise à disposition de matériel : </B>entre toutes les nouvelles architectures listées plus haut et les nouveaux périphériques comme les téléphones libres, la mise à disposition de matériel devient très importante. Je crois que nous devrions avoir un budget disponible pour fournir du matériel aux développeurs qui prennent soin de faire fonctionner Debian sur ces appareils. Je crois que le DPL pourrait aussi passer du temps avec les constructeurs de matériel pour obtenir quelques échantillons gratuits (ou prêtés) ainsi que des tarifs préférentiels pour tous les développeurs Debian.</LI>
<LI><B>Rencontre Debian Women :</B> en tant que DPL, je souhaite créer un projet où les femmes de toutes les villes du monde peuvent organiser des rencontres mensuelles et que Debian paie le prix des rafraîchissements (comme cela est fait pour les chasses aux bogues). Nous déclarons souvent à quel point nous prenons au sérieux le fait d'impliquer plus de femmes dans le projet, mais dans ce cas nous devons montrer que nous le pensons vraiment. Ces rencontres pourraient juste servir à parler de Debian et en faire des démonstrations. Dès qu'une personne s'intéresse à Debian et commence à l'utiliser, elle acquiert immédiatement des compétences qu'elle peut enseigner à quelqu'un d'autre. Nous avons une énorme base d'utilisateurs potentiels que nous ne ciblons pas assez.</LI>
<LI><B>Meilleure compréhension des finances :</B> par le passé, les gens ont demandé à mieux comprendre comment Debian dépense son argent et combien est disponible. La façon dont fonctionne la comptabilité de Debian à travers les organisations de confiance peut rendre la production de chiffre en temps réel très difficile, mais je pense que des rapports simples fournis régulièrement et faisant la liste de toutes les dépenses récentes (rencontres, DebConf, matériel, etc.) ainsi que les soldes récents suffiront à donner une vue d'ensemble de la façon dont Debian gère ses finances.</LI>
<LI><B>Pseudo-bogues DPL :</B> en tant que responsable du projet, je vais aussi m'efforcer de prendre des congés. Je prévois de ne pas passer mes week-ends à travailler sur les affaires de DPL à moins qu'une chose vraiment urgente (ou excitante) justifie d'avoir mon attention. Si une facette du rôle de DPL devient trop prenante, je considérerai qu'il s'agit d'un bogue et le rapporterai publiquement. Je recommande un pseudo-paquet DPL (<a href="https://www.debian.org/Bugs/pseudo-packages">plus d'informations sur ces paquets ici</a>) pouvant servir à enregistrer les bogues à propos du rôle de DPL, et aussi pour faire des requêtes au DPL (de la même façon qu'on peut ouvrir des bogues ITP/RFS/O/etc. dans le pseudo-paquet wnpp).</LI>
<LI><B>Clarifier le fonctionnement des processus et comment soumettre des retours :</B> je sais qu'il peut paraître redondant de dire cela, mais chaque processus communautaire devrait être bien documenté à un emplacement évident. En plus de cela, il devrait aussi exister une méthode claire pour signaler des bogues/objections/améliorations pour un processus, comme nous le faisons avec la Charte Debian (Policy Manual) pour l'empaquetage. Les projets comme DebConf pourraient à mon avis <I>vraiment</I> en bénéficier.</LI>
</OL>

<P>Rien de tout cela n'est gravé dans le marbre, et je vais travailler avec la communauté pour obtenir ses réactions. Les plans vont certainement changer, mais je pense que régler ce qui a été cité plus haut devrait mettre le projet en bonne position pour renforcer notre communauté et nous attaquer à de plus grandes idées. Je vais vous quitter avec une de mes citations préférées :</P>

<DIV CLASS="quote">
<P>« Les grands esprits discutent d'idées. Les esprits moyens discutent d'événements. Les esprits petits discutent des gens. » <BR> <I> – Eleanor Roosevelt</I></P>
</DIV>

<H2> 3. Les conséquences du travail existant </H2>

<p>Si je suis élu, je prévois, au moins dans une certaine mesure, de réduire les rôles actuels dans Debian pour optimiser la quantité d'énergie disponible pour le rôle de DPL :</p>

<OL>
<LI><B>DebConf :</B> je démissionnerai du comité de DebConf, ce qui ne serait pas du tout une rupture importante dans la mesure où la tâche récurrente la plus importante est la décision de candidature pour la DebConf suivante qui est maintenant prise pour cette année. Je prévois de rester impliqué dans d'autres domaines de la DebConf. Je me retirerai des bourses après DC19, mais je travaillerai aux éventualités pour l'année suivante.
</LI>
<LI><B>Entretien de paquets :</B> je prévois de renoncer à tous les bogues d'ITP (intention d'empaquetage) que j'ai remplis et ne prendrai en charge aucun nouveau travail d'empaquetage envahissant. Je continuerai à entretenir mes paquets existants.</LI>
<LI><B>Parrainage de paquets :</B> c'est une activité que je mème à temps perdu et je continuerai à le faire, mais probablement moins.</LI>
<LI><B>Faire l'idiot :</B> les DPL sont supposés être toujours sérieux, non ?</LI>
</OL>

<H2> 4. Quelques critères à évaluer. </H2>

<H3> 4.1. Les raisons pour me prendre en considération comme DPL </H3>

<OL>
<LI><B>Une énergie nouvelle :</B> avec les projets exposés plus haut (ou semblables à ceux-là), je crois pouvoir insuffler une nouvelle énergie dans le projet Debian. Je pense que les développeurs Debian méritent un chef du projet qui puisse diffuser une énergie positive et rendre chacun fier d'appartenir au projet Debian.</LI>

<LI><B>Bon sens :</B> je ne vais pas introduire des idées farfelues dans le projet, mais plutôt, j'aspire à trouver le moyen de le garder ancré dans la réalité et de créer une base stable que nous puissions utiliser pour s'élever vers des idées plus importantes et plus ambitieuses.</LI>

<LI><B>Le bon moment :</B> cette année est la bonne pour moi pour devenir DPL. Ma vie personnelle est plutôt calme actuellement, et je n'ai pas de famille dont je doive m'occuper et j'ai une certaine souplesse du côté du travail sans être complètement surchargé de travail.</LI>
</OL>

<H3> 4.2. Les faiblesses de ma campagne </H3>

<OL>
<LI><B>Expérience</B> : Je n'ai pas l'expérience de la conduite d'un projet aussi grand et complexe (ou même compliqué) que Debian. Je ne suis développeur Debian que depuis relativement peu de temps. Néanmoins, je crois que d'en être conscient m'évitera au moins de voler trop près du soleil, le but principal de mon mandat sera de réduire les frictions existantes et d'atténuer ou résoudre les problèmes existants.</LI>
<LI><B>Apparitions publiques :</B> je vis dans une partie relativement éloignée du monde, et à cause des besoins de visa et du temps de trajet, je ne pourrai pas voyage pour parler de  Debian autant que pouvaient le faire les précédents DPL. Je pourrais atténuer cela en restant dans une région pour un temps un peu plus long et faire une série de conférences à la fois, je peux souvent travailler à distance ce qui peut aider à rendre cela possible.
</LI>
<LI><B>Tempérament :</B> Je peux être très émotif et parfois, j'explose un peu. Avec le temps, j'ai appris à canaliser mon énergie émotionnelle en quelque chose de plus positif. Je vise à adopter une attitude stoïcienne en tant que DPL et à passer un certain temps à étudier la diplomatie, si je suis élu.</LI>
</OL>

<H2> 5. Remerciements </H2>

<UL>
<LI>J'ai utilisé la <a href="https://www.debian.org/vote/2010/platforms/zack">structure du programme de Zack</a> comme base de la mienne.</LI>
<LI>Merci à Laura Arjona Reina et Martin Michlmayer qui tous les deux ont corrigé de nombreuses fautes et problèmes de langage dans ce programme.</LI>
</UL>

<H2> A. Réfutations </H2>

<P>Tout d'abord, j'ai personnellement croisé Joerg, Sam et Martin, et c'est un honneur et un privilège de mener une campagne de DPL avec eux.</P>

<P>Bien que je les apprécie tous les trois, je pense qu'il est important d'être concret et d'aborder certains problèmes dans leurs programmes.</P>

<P>Au moment où j'écris, Simon n'a encore rien envoyé depuis sa candidature, aussi, pour le moment, je ne peux pas discuter son programme.</P>

<H3> A.1. Joerg Jaspert </H3>

<P><a href="https://www.debian.org/vote/2019/platforms/joerg">https://www.debian.org/vote/2019/platforms/joerg</a></P>

<P>Joerg a produit de nombreuses contributions avec le temps et porte encore plusieurs casquettes, je lui suis reconnaissant de tout le travail qu'il a réalisé.</P>

<P>Il pose quelques questions importantes dans son programme, mais je trouve que les détails sur le moyen d'y répondre étaient un peu légers. Le rôle de DPL qu'il décrit me semble quelque peu passif et réactif. À mon avis, Debian a besoin maintenant d'un DPL proactif, qui n'esquive pas les problèmes difficiles, les prenne à bras le corps à l'intérieur de la communauté et des équipes concernées.</P>

<P>Il mentionne dans son programme qu'il souhaite travailler avec tous ceux qui souhaitent améliorer les processus dans Debian, ce qui est très bien, mais à mon avis, là où nous en sommes maintenant, le DPL devrait aussi conduire certains processus sans attendre n'importe quelle sorte de pression externe pour le faire.</P>

<H3> A.2. Sam Hartman </H3>

<P><a href="https://www.debian.org/vote/2019/platforms/hartmans">https://www.debian.org/vote/2019/platforms/hartmans</a></P>

<P>J'apprécie que Sam reconnaisse la médiation comme un aspect important ainsi que son avis sur un cadre de communication non-violent. Beaucoup d'autres sujets dont il parle me touchent.</P>

<P>Dans son programme, il évoque le nombre croissant de logiciels libres disponibles, mais je n'ai pu rien trouver dans le programme qui traite directement de la croissance de la communauté. Le monde du logiciel libre à l'extérieur est florissant, et nous devrions réfléchir au moyen de faire croître Debian avec lui. Bien que ce ne soit pas un rôle technique du DPL, je pense vraiment qu'il est important que le DPL garde cela à l'esprit sous tous ses aspects lorsqu'on examine la santé de la communauté.</P>

<P>Il mentionne qu'en tant que DPL il aurait un certain nombres d'outils disponibles pour orienter la prise de décision, mais il est très léger pour dire ce qu'ils seraient, en plus, il mentionne que les résolutions générales sont sous-exploitées. Je pense qu'elles devraient demeurer un dernier recours et qu'il existe de meilleurs moyens pour évaluer la position de la communauté sur un sujet quand cela est nécessaire. Si un sondage est nécessaire, c'est mieux de faire un sondage approprié que d'utiliser une résolution générale comme outil générique.</P>

<H3> A.3. Martin Michlmayr </H3>

<P><a href="https://www.debian.org/vote/2019/platforms/tbm">https://www.debian.org/vote/2019/platforms/tbm</a></P>

<P>J'aime que Martin souhaite aussi faciliter activement les discussions et faire parler de Debian. Les deux sont importants actuellement dans Debian ce qui est aussi pourquoi je traite de cela également dans mon programme.</P>

<P>Cela dit, je trouve ce programme un peu déprimant de façon générale. Je pense qu'il est d'une certaine manière un peu trop critique dans la mesure où il mentionne quelques points négatifs valables, mais, d'après mon expérience, j'ai eu aussi beaucoup d'expériences de contreparties positives qui vont avec et qui ne sont pas présentes dans son programme. À mon avis, il peint Debian sous une couleur bien plus négative qu'elle ne le mérite et je ne suis pas sûr que c'est une bonne qualité pour un DPL.</P>

<P>Je pense qu'il est possible que, parce qu'il n'a pas été actif dans Debian ces dernières années, il a pu rater beaucoup de bonnes choses dans Debian, et peut-être que beaucoup ont été améliorées depuis la période où il était plus actif.</P>

<P>Une partie de son projet de stabilité est de penser à financer les gens pour leur travail dans Debian. Alors que je pense que ce pourrait être un sujet sain à explorer, je pense également que ce serait un sujet beaucoup plus controversé qu'il ne le pense, et, pour être honnête, je crois que c'est un sujet que je souhaiterais reporter cette année. Je pense qu'il est plus important pour Debian de se concentrer sur les problèmes principaux maintenant, pour guérir, croître et partir du bon pied comme communité. Martin pense aussi <a href="https://lists.debian.org/msgid-search/20190320173829.GB3022@jirafa.cyrius.com">qu'il est temps de poser des questions comme « Pourquoi le poste de DPL n'est pas salarié ? »</a>, bien que, personnellement, je ne souhaite pas passer l'année sur ce sujet dans toutes les listes de discussions alors que je crois que nous pourrions tirer bénéfice d'explorer certains problèmes plus immédiats.</P>

<H2> B. Journal des modifications </H2>

<P> Ce programme est sous contrôle de versions dans un <a href="https://salsa.debian.org/jcc/dpl-platform">dépôt git.</a> </P>

<ul>
<LI><a href="https://salsa.debian.org/jcc/dpl-platform/tags/1.00">1.00</a> : version publique initiale.</LI>
<LI><a href="https://salsa.debian.org/jcc/dpl-platform/tags/1.01">1.01</a> : amélioration du sommaire sur les buts principaux.</LI>
<LI><a href="https://salsa.debian.org/jcc/dpl-platform/tags/1.02">1.02</a> : construction de versions HTML stylée, wml simple et texte simple, correction de la photo, pas de changement de texte.</LI>
<LI><a href="https://salsa.debian.org/jcc/dpl-platform/tags/1.03">1.03</a> : fusion <a href="https://salsa.debian.org/jcc/dpl-platform/merge_requests/1">MR#1</a> et <a href="https://salsa.debian.org/jcc/dpl-platform/merge_requests/2">MR#2</a>, corrections de diverses fautes. Correction d'un problème mineur de style, et formulation du journal des modifications.</LI>
<LI><a href="https://salsa.debian.org/jcc/dpl-platform/tags/1.04">1.04</a> : ajout des réfutations, correction de nouvelles fautes.</LI>
</ul>

<BR>

</DIV> <!-- END MAIN -->
