#use wml::debian::template title="Programme de Felix Lechner" BARETITLE="true" NOHEADER="true"
#include "$(ENGLISHDIR)/vote/style.inc"
#use wml::debian::translation-check translation="88f603fac87a12843fb46c6c57072aaa7e9ee2cf" maintainer="Jean-Pierre Giraud"

<h1>Soyez heureux !</h1>

<p>
Bonjours, bienvenue sur mon programme de candidat comme chef du projet Debian
en 2022. J'aimerais vous offrir des compétences démontrées de direction pour
accroître le bonheur et le bien être des tous les Debianistes pour l'année à
venir.</p>

<h2>
Compassion des uns pour les autres
</h2>

<p>
Notre travail est souvent éprouvant et solitaire, mais ensemble, nous pouvons
changer les choses. D'un point de vue technique, Debian fait déjà cela pour des
millions d'utilisateurs tous les jours. Sur le plan humain néanmoins, nous ne
sommes pas à la hauteur. Peut-être que certains sont amers parce qu'ils n'ont
pas acquis de bitcoin, ou peut-être ils l'ont fait et leurs êtres chers sont
partis (je ne possède pas de crypto-monnaies.)
Dans tous les cas, le
<a href="https://www.youtube.com/watch?v=36m1o-tM05g">bonheur</a> est le plus
précieux des cadeaux.
Faisons ensemble de Debian un endroit heureux.
</p>

<h2>
Une fondation solide
</h2>

<p>
Mon approche est éclairée par tout un tas d'idées « new-age » comme celles
d'Eckhart Tolle
(<a href="https://en.wikipedia.org/wiki/The_Power_of_Now">ici</a>
et
<a href="https://en.wikipedia.org/wiki/A_New_Earth">là</a>)
associées à un peu de bonne vieille religion.
Dans un certain sens, je cherche à rajeunir Debian, ce qui est une chose étrange
à écrire dans la mesure où je serai probablement le plus vieux chef de projet.
Ce que je veux dire, toutefois, c'est qu'il faut laisser de côté le cynisme et
la colère qui ont parfois occupé le devant de la scène dans Debian.
Laissez-moi être votre professeur !
</p>

<p>
Pourquoi me croire ? C'est facile.
</p>

<h2>
Un chef respecté
</h2>

<p>
Pendant près de neufs ans, j'ai été un petit responsable municipal &mdash; un
<a href="https://www.fremont.gov/1480/Library-Advisory-Commission">\
membre de la commission de la bibliothèque</a> pour être exact &mdash; à un
endroit particulier. L'endroit où j'ai choisi de m'établir, Fremont en
Californie, la 
<a href="https://pluralism.org/fremont-usa">ville la plus cosmopolite</a> en
Amérique. Ma femme est originaire de Chine. Mon meilleur ami vient de l'Inde.
D'une certaine façon, Fremont est un modèle de ce que Debian pourrait être.
</p>

<p>
Pour couronner le tout, Fremont a été pendant de nombreuses années la
<a href="https://wallethub.com/edu/happiest-places-to-live/32619">communauté la plus heureuse</a>
en Amérique du Nord (même s'il faut encore que je voie une comparaison avec le
Danemark).
Une cause probable ? Fremont dispose d'un système municipal de conseils et de
commissions qui signifie que tout le monde a voix au chapitre.
</p>

<p>
Si vous m'élisez, voilà ce que nous ferons ensemble pour Debian. Nous offrirons
une république à Debian !
</p>

<h2>
Appelez s'il vous plaît
</h2>

<p>
En tant que chef du projet, je m'efforcerai d'être le responsable le plus
disponible que jamais.
Même si j'aime écrire, je préfère les appels téléphoniques et les rencontres en
personne. J'espère être disponible pratiquement tous les jours sur
<a href="https://jitsi.debian.social/">Jitsi</a> (mais jamais le samedi).
Sinon, appelez-moi ou envoyez-moi un texto.
Mon numéro est le <a href="tel:+16172295005">+1-617-229-5005</a>.
Tout le monde est le bienvenu !
</p>

<h2>
Un outsider à vie
</h2>

<p>
J'ai été un étranger presque toute ma vie. Après le lycée, j'ai quitté Berlin où
je suis né, espérant étudier la physique théorique. Au lieu de cela, j'ai
terminé mes études avec une maîtrise en génie électrique et en système
informatique de l'université d'Harvard. Ma spécialité était la conception de
puces. J'ai vécu en Suisse, en Chine et en Grande-Bretagne. J'ai toujours eu
besoin de m'intégrer, et d'une certaine manière, je ne l'ai jamais fait.
</p>

<h2>
Votre clé va bientôt expirer
</h2>

<p>
Dans Debian, le service que j'apprécie le plus, c'est, sans aucun doute, mon
<a href="https://bugs.debian.org/892058">rappel d'expiration</a> de clé.
Avec un nombre considérable de
<a href="https://salsa.debian.org/groups/lintian/-/group_members">contributeurs</a>,
je contribue à la maintenance de
<a href="https://salsa.debian.org/lintian/lintian">Lintian</a> et de
<a href="https://lintian.debian.org/">son site web</a>, mais les gens
apprécient beaucoup moins cette tâche
(David Bremner m'a permis de me sentir mieux quand il a gentiment rappelé
à quelqu'un que Lintian n'était pas leur patron).
Je suis aussi le responsable de <code>mdadm</code>, <code>gocryptfs</code>,
<code>wolfssl</code> et de quelques autres paquets plus petits.
</p>

<h2>
Complètement opérationnel
</h2>

<p>
Plus récemment, j'ai donné un
<a href="https://bugs.debian.org/1002296">système de construction</a> Debhelper
à Haskell et j'ai pris le relais de Joachim Breitner pour Hackage,
l'<a href="https://hackage.debian.net/">outil de suivi de version</a> des
paquets Haskell. En tant que chef du projet, ce sera ma principale distraction.
Je suis obsédé par Haskell, ce qui est un peu regrettable parce que je ne
suis pas très bon dans ce langage &mdash; mais comme Clint Adams peut vous le
dire, j'apprends.
</p>

<h2>
Se battre pour Debian
</h2>

<p>
Grâce à la confiance de Sam Hartman et Brian Gupta, j'ai fait partie de
<a href="https://www.debian.org/trademark">l'équipe des marques déposées</a>,
avec un rôle de délégué. Dans ce rôle, j'ai travaillé dur pour protéger la
<a href="https://tsdr.uspto.gov/#caseNumber=86037470&amp;caseType=SERIAL_NO&amp;searchType=statusSearch">marque</a>
Debian contre sa récupération par les logiciels propriétaires du marché,
Debian compris.
</p>

<h2>
Mon programme
</h2>

<p>
En tant que chef du projet, je travaillerai inlassablement à réduire les
conflits au sein du projet. À l'égard du monde extérieur, je ferai tous les
efforts possibles pour aider les utilisateurs et en particulier pour que les
communautés partout dans le monde tombent à nouveau amoureuses de Debian. Nous
devrions être la meilleure plateforme de développement pour tous les écosystèmes
de programmation.
</p>

<p>
J'espère en outre avancer sur un large éventail de défis à long terme pour
le projet, renouveler un effectif vieillissant et gérer la prolifération de
gestionnaires de paquets spécifiques à un langage (autrement dit, le problème
de l'incorporation des dépendances « vendoring »). Grâce à votre aide, j'espère
faire prendre une bonne direction à Debian pour les dix prochaines années.
Travaillons ensemble !
</p>

<h2>
À propos des réfutations
</h2>

<p>
Je ne suis pas encore sûr de m'engager avec les
<a href="https://www.debian.org/vote/2022/vote_002">autres candidats</a> sur le
terrain des réfutations. C'est probablement trop
<a href="https://www.youtube.com/watch?v=6TmA2XSlQfk">négatif</a>.
Tous feront du bon travail.
Debian est forte.
Si vous voulez que les choses restent telles qu'elles sont, vous avez juste
à voter pour eux.
</p>

<h2>
Une chanson pour vous
</h2>

<p>
Si vous trouvez ce message ennuyeux, regardez ma 
<a href="https://www.youtube.com/watch?v=RsrxxURtde4">vidéo de campagne</a>.
Ce message exprime tout ce que j'aurais aimé écrire ici, quoi qu'il en soit.
Amusez-vous bien !
</p>

<p>
Merci d'avoir lu ce programme !
</p>

<img alt="Felix Lechner" src="felix.jpg">

