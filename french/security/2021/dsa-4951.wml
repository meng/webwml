#use wml::debian::translation-check translation="6aa682f4f094fddd41be2462f4e493e841ffb1ac" maintainer=" Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans Bluez, la pile du
protocole Bluetooth de Linux.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26558">CVE-2020-26558</a>

<p>/ <a href="https://security-tracker.debian.org/tracker/CVE-2021-0129">CVE-2021-0129</a></p>

<p> Bluez ne vérifie pas correctement les permissions durant l'opération
d'appairage, ce qui pourrait permettre à un attaquant d'usurper l'identité
du périphérique effectuant l'initialisation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27153">CVE-2020-27153</a>

<p>Jay LV a découvert un défaut de libération de zone de mémoire dans la
routine disconnect_cb() de gattool. Un attaquant distant peut tirer
avantage de ce défaut durant la découverte de service pour un déni de
service, ou, éventuellement, l'exécution de code arbitraire.</p></li>

</ul>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 5.50-1.2~deb10u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bluez.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de bluez, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/bluez">\
https://security-tracker.debian.org/tracker/bluez</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4951.data"
# $Id: $
