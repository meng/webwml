#use wml::debian::translation-check translation="ddc3ad478a9252fb562d0e611a65f886beddbf1f" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Deux problèmes de sécurité ont été découverts dans libssh, une petite
bibliothèque SSH en C :</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1667">CVE-2023-1667</a>

<p>Philip Turnbull a découvert a déréférencement de pointeur NULL qui
pouvait avoir pour conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-2283">CVE-2023-2283</a>

<p>Kevin Backhouse a découvert que pki_verify_data_signature() pouvait
échouer à valider correctement l'authentification dans des situations de
pression sur la mémoire.</p></li>

</ul>

<p>Pour la distribution stable (Bullseye), ces problèmes ont été corrigés
dans la version 0.9.7-0+deb11u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libssh.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libssh, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libssh">\
https://security-tracker.debian.org/tracker/libssh</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5409.data"
# $Id: $
