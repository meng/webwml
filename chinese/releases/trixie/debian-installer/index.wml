#use wml::debian::template title="Debian &ldquo;trixie&rdquo; 安装信息" NOHEADER="true"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/trixie/release.data"
#use wml::debian::translation-check translation="aa916b5ed417b0116ff9fa62a2607e688980d881"

<h1>安装 Debian <current_release_trixie></h1>

<if-stable-release release="forky">
<p><strong>Debian 13 已被 \
<a href="../../forky/">Debian 14（<q>forky</q>）</a>取代。\
以下的某些安装映像可能已不可用，或不能工作，建议您转而安装 forky。
</strong></p>
</if-stable-release>

<if-stable-release release="bookworm">
<p>有关<q>trixie</q>（当前的测试版）\
的安装映像及如何安装的文档，\
请参阅 <a href="$(HOME)/devel/debian-installer/">Debian 安装程序页面</a>。</p>
</if-stable-release>

<if-stable-release release="trixie">
<p>
<strong>要安装 Debian</strong> <current_release_trixie>\
（<em>trixie</em>），请下载以下映像中的一个（所有 i386 和 amd64 CD/DVD 映\
像均可以在 [CN:U 盘:][TW:USB 随身碟:][HK:USB 手指:]上使用）：
</p>

<div class="line">
<div class="item col50">
	<p><strong>网络安装 CD 映像（通常 150-280 MB）</strong></p>
		<netinst-images />
</div>


</div>

<div class="line">
<div class="item col50">
	<p><strong>完整 CD 映像集</strong></p>
		<full-cd-images />
</div>

<div class="item col50 lastcol">
	<p><strong>完整 DVD 映像集</strong></p>
		<full-dvd-images />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD（通过 <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>）</strong></p>
<full-cd-torrent />
</div>

<div class="item col50 lastcol">
<p><strong>DVD（通过 <a href="$(HOME)/CD/torrent-cd">BitTorrent</a>）</strong></p>
<full-dvd-torrent />
</div>

</div>

<div class="line">
<div class="item col50">
<p><strong>CD（通过 <a href="$(HOME)/CD/jigdo-cd">jigdo</a>）</strong></p>
<full-cd-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>DVD（通过 <a href="$(HOME)/CD/jigdo-cd">jigdo</a>）</strong></p>
<full-dvd-jigdo />
</div>


</div>

<div class="line">
<div class="item col50">
<p><strong>蓝光（通过 <a href="$(HOME)/CD/jigdo-cd">jigdo</a>）</strong></p>
<full-bluray-jigdo />
</div>

<div class="item col50 lastcol">
<p><strong>其他映像（网络启动、灵活的 [CN:U 盘:][TW:USB 随身碟:][HK:USB 手指:]，等等）</strong></p>
<other-images />
</div>
</div>



<p>
<strong>注意</strong>
</p>
<ul>
    <li>下载完整的 CD 和 DVD 映像，推荐使用 BitTorrent 或 jigdo。</li>
    <li>对于不常见的架构，CD 和 DVD 映像集中，只有有限的映像\
提供 ISO [CN:文件:][HKTW:档案:] 或 BitTorrent 下载。\
完整的映像集只能通过 jigdo 下载。</li>
    <li>多架构 <em>CD</em> 映像支持 i386/amd64；\
安装过程与从单架构的网络安装映像安装相似。</li>
    <li>多架构 <em>DVD</em> 映像支持 i386/amd64；\
安装过程与从单架构的完整 CD 映像安装相似；\
DVD 也包含了所有包含的软件包的源代码。</li>
    <li>对于安装映像，校验[CN:文件:][HKTW:档:]（<tt>SHA256SUMS</tt>、\
<tt>SHA512SUMS</tt> 和其他校验和）可以在映像的同一目录下找到。</li>
</ul>


<h1>文档</h1>

<p>
<strong>如果您在安装前只想阅读一份文档</strong>，请阅读我们的\
<a href="../i386/apa">安装指南</a>\
，这是一份安装过程的简要介绍。其他有用的文档包括：
</p>

<ul>
<li><a href="../installmanual">Trixie 安装手册</a><br />
详细的安装步骤</li>
<li><a href="https://wiki.debian.org/DebianInstaller/FAQ">Debian 安装程序 FAQ</a> 和 <a href="$(HOME)/CD/faq/">Debian CD FAQ</a><br />
常见问题和解答</li>
<li><a href="https://wiki.debian.org/DebianInstaller">Debian 安装程序 Wiki</a><br />
由[CN:社区:][HKTW:社群:]维护的文档</li>
</ul>

<h1 id="errata">勘误</h1>

<p>
这是 Debian <current_release_trixie> 安装程序的已知问题列表。\
如果您在安装 Debian 时遇到了这里没有列出的问题，请发一份\
<a href="$(HOME)/releases/stable/amd64/ch05s04#submit-bug">安装报告</a>给我们，\
描述所遇到的问题，或者\
<a href="https://wiki.debian.org/DebianInstaller/BrokenThings">访问维基</a>\
查看其他已知问题。
</p>

## Translators: copy/paste from devel/debian-installer/errata
<h3 id="errata-r0">Errata for release 13.0</h3>

<dl class="gloss">

<!--
     <dt>Desktop installations may not work using CD#1 alone</dt>

     <dd>Due to space constraints on the first CD, not all of the
     expected GNOME desktop packages fit on CD#1. For a successful
     installation, use extra package sources (e.g. a second CD or a
     network mirror) or use a DVD instead.

     <br /> <b>Status:</b> It is unlikely more efforts can be made to
     fit more packages on CD#1. </dd>
-->
</dl>

<p>
给下个 Debian 发布版本使用的改进版本的安装系统正在开发中，\
它也可用于安装 trixie。详情请见 \
<a href="$(HOME)/devel/debian-installer/">Debian 安装程序项目主页</a>。
</p>
</if-stable-release>
