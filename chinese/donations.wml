#use wml::debian::template title="如何向 Debian 计划进行捐赠" MAINPAGE="true"
#use wml::debian::translation-check translation="77730b1058b58e76cb29910f6acf353ec5bf9847"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#spi">Software in the Public Interest</a></li>
  <li><a href="#debianfrance">Debian France</a></li>
  <li><a href="#debianch">debian.ch</a></li>
  <li><a href="#debian">Debian</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> 感谢所有向 Debian 捐赠设备或服务的捐赠者：<a href="https://db.debian.org/machines.cgi">主机和硬件赞助者</a>、<a href="mirror/sponsors">镜像站赞助者</a>、<a href="partners/">开发和服务合作伙伴</a>。<p>
</aside>

<p>
捐赠由 <a href="$(HOME)/devel/leader">Debian 项目领导者</a>（DPL）管理，这些捐赠能使 Debian 购置<a href="https://db.debian.org/machines.cgi">机器</a>、<a href="https://wiki.debian.org/Teams/DSA/non-DSA-HW">其他硬件</a>、域名、加密证书，举行 <a href="https://www.debconf.org">Debian 会议</a>、<a href="https://wiki.debian.org/MiniDebConf">Debian 微型会议</a>、<a href="https://wiki.debian.org/Sprints">开发冲刺活动</a>，参加其他活动，以及处理其他事务。
</p>

<p id="default">
多个<a href="https://wiki.debian.org/Teams/Treasurer/Organizations">组织</a>受托管理 Debian 的资产，并代表 Debian 接受捐赠。本页面列出了向 Debian 计划进行捐赠的几种不同的方法。给 Debian 捐款的最简单方法是通过 PayPal 捐款到 <a href="https://www.spi-inc.org/" title="SPI">Software in the Public Interest</a>，这是一家受托管理 Debian 的资产的非营利组织。
</p>

<p style="text-align:center"><button type="button"><span class="fab fa-paypal fa-2x"></span> <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=86AHSZNRR29UU">通过 PayPal 捐款</a></button></p>

<aside class="light">
  <span class="fas fa-gifts fa-5x"></span>
</aside>

<table>
<tr>
<th>组织</th>
<th>方式</th>
<th>注释</th>
</tr>
<tr>
<td><a href="#spi"><acronym title="Software in the Public Interest">SPI</acronym></a></td>
<td>
 <a href="#spi-paypal">PayPal</a>，\
<a href="#spi-click-n-pledge">Click &amp; Pledge</a>，\
<a href="#spi-cheque">支票</a>，\
<a href="#spi-other">其他</a>
</td>
<td>美国，免税非营利</td>
</tr>
<tr>
<td><a href="#debianfrance">Debian France</a></td>
<td>
 <a href="#debianfrance-bank">电汇</a>，\
<a href="#debianfrance-paypal">PayPal</a>
</td>
<td>法国，免税非营利</td>
</tr>
<tr>
<td><a href="#debianch">debian.ch</a></td>
<td>
 <a href="#debianch-bank">电汇</a>，\
<a href="#debianch-other">其他</a>
</td>
<td>瑞士，非盈利</td>
</tr>
<tr>
<td><a href="#debian">Debian</a></td>
<td>
 <a href="#debian-equipment">设备</a>，\
<a href="#debian-time">时间</a>，\
<a href="#debian-other">其他</a>
</td>
<td></td>
</tr>
# Template:
#<tr>
#<td><a href="#"><acronym title=""></acronym></a></td>
#<td>
# <a href="#"></a>,
# <a href="#"></a> (allows recurring donations),
# <a href="#cheque"></a> (CUR)
#</td>
#<td>, tax-exempt non-profit</td>
#</tr>

</table>

<h2 id="spi">Software in the Public Interest</h2>

<p>
<a href="https://www.spi-inc.org/" title="SPI">Software in the Public Interest, Inc.</a>
是一家位于美国的免税非营利机构，\
由 Debian 人员于 1997 年创立，以帮助自由软件/硬件组织。
</p>

<h3 id="spi-paypal">PayPal</h3>

<p>
可以通过 PayPal 网站上的 <a href="https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&amp;hosted_button_id=86AHSZNRR29UU">SPI 页面</a>单次或定期捐款。要定期捐款，请勾选 <em>Make this a monthly donation</em> 框。
</p>

<h3 id="spi-click-n-pledge">Click &amp; Pledge</h3>

<p>
可以通过 Click &amp; Pledge 网站上的 <a href="https://co.clickandpledge.com/advanced/default.aspx?wid=34115">SPI 页面</a>单次或定期捐款。要定期捐款，请在右侧（<em>Repeat payment</em>）选择您捐款的频率，向下滚动到 <em>Debian Project Donation</em>，输入您要捐赠的金额，点击 <em>Add to cart</em>，然后完成剩余过程。
</p>

<h3 id="spi-cheque">支票</h3>

<p>
可以通过支票或汇票捐款，支持 <abbr title="美元">USD</abbr> 和 <abbr title="加元">CAD</abbr>。请在备忘录字段中填入 Debian，然后将支票寄送至 SPI，地址列于 <a href="https://www.spi-inc.org/donations/">SPI 捐赠页面</a>。
</p>

<h3 id="spi-other">其他</h3>

<p>
还可以通过电汇和其他方式捐款。对于世界上的某些地方，向 Software in the Public Interest 的合作组织之一捐赠可能更容易。欲了解更多详情，请访问 <a href="https://www.spi-inc.org/donations/">SPI 捐赠页面</a>。
</p>

<h2 id="debianfrance">Debian France</h2>

<p>
<a href="https://france.debian.net/">Debian 法国协会</a>是根据<q>1901 年法案</q>在法国注册的组织，以在法国支持和宣传 Debian 计划。
</p>

<h3 id="debianfrance-bank">电汇</h3>

<p>
可以通过电汇捐款，账户在 <a href="https://france.debian.net/soutenir/#compte">Debian France 捐赠页面</a>上列出。捐款收据可以发送电子邮件至 <a href="mailto:donation@france.debian.net">donation@france.debian.net</a> 索取。
</p>

<h3 id="debianfrance-paypal">PayPal</h3>

<p>
可通过 <a href="https://france.debian.net/galette/plugins/paypal/form">Debian France PayPal 页面</a>进行捐款。捐款可指定直接用于 Debian France 或是用于 Debian 计划整体。
</p>

<h2 id="debianch">debian.ch</h2>

<p>
<a href="https://debian.ch/">debian.ch</a> 在瑞士和列支敦士登公国代表 Debian 计划。
</p>

<h3 id="debianch-bank">电汇</h3>

<p>
可以通过瑞士和国际银行电汇捐款，账户在 <a href="https://debian.ch/">debian.ch 网站</a>上列出。
</p>

<h3 id="debianch-other">其他</h3>

<p>
通过联系<a href="https://debian.ch/">网站</a>上列出的捐款地址，可以通过其他方式捐款。
</p>

# Template:
#<h3 id=""></h3>
#
#<p>
#</p>
#
#<h4 id=""></h4>
#
#<p>
#</p>

<h2 id="debian">Debian</h2>

<p>
Debian 可以接受直接<a href="#debian-equipment">設備</a>捐赠，但目前暂无法接受<a href="#debian-other">其他</a>类型的捐赠。
</p>

<h3 id="debian-equipment">设备和服务</h3>

<p>
Debian 还依靠来自个人、公司、大学等捐赠的设备和服务，讓 Debian 能夠持續跟世界連繫。
</p>

<p>
如果您的公司有閒置的[CN:計算機:][HKTW:電腦:]，[CN:或者:][HKTW:或是:]多餘的設備（如[CN:硬盤:][HKTW:硬碟:]、SCSI 控制卡、[CN:網絡:][HKTW:網路:]卡等），請考慮將它們捐獻給 Debian。請與我們的<a href="mailto:hardware-donations@debian.org">[CN:硬件:][HKTW:硬體:]捐獻處理代表</a>[CN:聯繫:][HKTW:接洽:]。
</p>

<p>
Debian 计划为内部的服务和团体维护着一份<a href="https://wiki.debian.org/Hardware/Wanted">硬件需求列表</a>。
</p>

<h3 id="debian-time">时间</h3>

<p>
有很多方法可以利用您的个人或工作时间<a href="$(HOME)/intro/help">协助 Debian</a>。
</p>

<h3 id="debian-other">其他</h3>

<p>
Debian 目前暂不能接受任何加密货币，但我们正在寻求支持这种捐赠方式。
# Brian Gupta requested we discuss this before including it:
#If you have cryptocurrency to donate, or insights to share, please
#get in touch with <a href="mailto:madduck@debian.org">Martin f. krafft</a>.
</p>
