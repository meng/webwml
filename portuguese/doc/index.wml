#use wml::debian::template title="Documentação"
#use wml::debian::translation-check translation="0352601c5e0d6e960ffa0c05a4c5dc054d1897ba"

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">

<ul class="toc">
  <li><a href="#quick_start">Início rápido</a></li>
  <li><a href="#manuals">Manuais</a></li>
  <li><a href="#other">Outros documentos (mais curtos)</a></li>
</ul>

<p>
A criação de um sistema operacional livre de alta qualidade também inclui
escrever manuais técnicos que descrevam a operação e o uso dos programas. O
projeto Debian dedica todos os esforços para fornecer a todos(as) os(às)
usuários(as) uma boa documentação em um formato facilmente acessível. Esta
página contém uma coleção de links que levam a guias de instalação, HOWTOs,
FAQs, notas de lançamento, nossa wiki e muito mais.
</p>

<h2><a id="quick_start">Início rápido</a></h2>

<p>
Se você é novato(a) no Debian, recomendamos que comece com dois guias:
</p>

<aside class="light">
  <span class="fas fa-fast-forward fa-5x"></span>
</aside>

<ul>
  <li><a href="$(HOME)/releases/stable/installmanual">Guia de instalação</a></li>
  <li><a href="manuals/debian-faq/">FAQ do Debian GNU/Linux</a></li>
</ul>

<p>
Tenha estes à mão quando fizer sua primeira instalação Debian, eles
provavelmente responderão a várias questões e ajudarão a trabalhar com seu
novo sistema Debian.
</p>

<p>
A seguir, você pode passar por estes documentos:
</p>

<ul>
  <li><a href="manuals/debian-reference/">Referência Debian</a>,
      um guia do(a) usuário(a) conciso com foco em linha de comando shell</li>
  <li><a href="$(HOME)/releases/stable/releasenotes">Notas de lançamento</a>,
      geralmente publicadas com atualizações do Debian, visando usuários(as)
      que estão atualizando a distribuição</li>
  <li><a href="https://wiki.debian.org/">Wiki do Debian</a>, a wiki oficial do
      Debian e uma boa fonte de informação para os(as) recém-chegados(as)</li>
</ul>

<p style="text-align:center"><button type="button"><span class="fas fa-print fa-2x"></span> <a href="https://www.debian.org/doc/manuals/refcard/refcard">Imprima o cartão de referência do Debian GNU/Linux</a></button></p>

<h2><a id="manuals">Manuais</a></h2>

<p>
A maior parte da documentação incluída no Debian foi escrita para GNU/Linux em
geral, mas também existe alguma documentação escrita especificamente para o
Debian. Basicamente, os documentos são classificados nessas categorias:
</p>

<ul>
  <li>Manuais: esses guias se assemelham a livros, pois descrevem os principais
  tópicos de forma abrangente. Muitos manuais listados abaixo estão disponíveis
  tanto on-line como em pacotes Debian. De fato, a maioria dos manuais no site
  web são extraídos de seus respectivos pacotes Debian. Escolha um manual
  abaixo para o nome do seu pacote e/ou links para as versões on-line.</li>
  <li>HOWTOs: como o nome indica, os
  <a href="https://tldp.org/HOWTO/HOWTO-INDEX/categories.html">documentos HOWTO</a>
  descrevem <em>como</em> fazer uma coisa em particular, ou seja, oferecem
  conselhos detalhados e práticos sobre como fazer algo.</li>
  <li>FAQs: compilamos vários documentos respondendo a
  <em>perguntas frequentes</em>. As perguntas relacionadas ao Debian são
  respondidas no <a href="manuals/debian-faq/">FAQ do Debian</a>. Além disso,
  há um <a href="../CD/faq/">FAQ separado sobre imagens de CD/DVD do Debian</a>
  respondendo a todos os tipos de perguntas sobre mídia de instalação.</li>
  <li>Outros documentos (mais curtos): verifique também a
  <a href="#other">lista</a> de instruções mais curtas.</li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Para uma lista completa de
manuais Debian e outras documentações, por favor visite a página do
<a href="ddp">projeto de documentação Debian</a>. Além disso, vários manuais
orientados ao(à) usuário(a) escritos para Debian GNU/Linux estão disponíveis
como <a href="books">livros impressos</a>.</p>
</aside>

<h3>Manuais específicos do Debian</h3>

<div class="line">
  <div class="item col50">

    <h4><a href="user-manuals">Manuais para usuários(as)</a></h4>
    <ul>
      <li><a href="user-manuals#faq">FAQ do Debian GNU/Linux</a></li>
      <li><a href="user-manuals#install">Guia de instalação Debian</a></li>
      <li><a href="user-manuals#relnotes">Notas de lançamento do Debian</a></li>
      <li><a href="user-manuals#refcard">Cartão de referência Debian</a></li>
      <li><a href="user-manuals#debian-handbook">O manual do(a) administrador(a) Debian</a></li>
      <li><a href="user-manuals#quick-reference">Referência Debian</a></li>
      <li><a href="user-manuals#securing">Manual de segurança Debian</a></li>
      <li><a href="user-manuals#aptitude">Manual do(a) usuário(a) do aptitude</a></li>
      <li><a href="user-manuals#apt-guide">Manual do(a) usuário(a) do APT</a></li>
      <li><a href="user-manuals#apt-offline">Usando o APT offline</a></li>
      <li><a href="user-manuals#java-faq">FAQ do Debian GNU/Linux e Java</a></li>
      <li><a href="user-manuals#hamradio-maintguide">Manual do(a) mantenedor(a)
      dos pacotes de Rádio Amador Debian</a></li>
    </ul>

  </div>

  <div class="item col50 lastcol">

    <h4><a href="devel-manuals">Manuais para desenvolvedores(as)</a></h4>
    <ul>
      <li><a href="devel-manuals#policy">Manual de políticas Debian</a></li>
      <li><a href="devel-manuals#devref">Referência dos(as) desenvolvedores(as)
      Debian</a></li>
      <li><a href="devel-manuals#debmake-doc">Guia dos(as) mantenedores(as)
      Debian</a></li>
      <li><a href="devel-manuals#packaging-tutorial">Introdução ao empacotamento
      Debian</a></li>
      <li><a href="devel-manuals#menu">Sistema de menu Debian</a></li>
      <li><a href="devel-manuals#d-i-internals">Instaladores internos do
      Debian</a></li>
      <li><a href="devel-manuals#dbconfig-common">Guia para mantenedores(as) de
      pacotes que usam banco de dados</a></li>
      <li><a href="devel-manuals#dbapp-policy">Políticas para pacotes que usam
      banco de dados</a></li>
    </ul>

  </div>
</div>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="misc-manuals#history">Leia a história do projeto Debian</a></button></p>

<h2><a id="other">Outros documentos (mais curtos)</a></h2>

<p>Os documentos seguintes incluem instruções mais rápidas e curtas:</p>

<p>
<dl>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Se você verificou os recursos
acima e ainda não consegue encontrar respostas para suas perguntas ou soluções
para os seus problemas em relação ao Debian, dê uma olhada na nossa
<a href="../support">página de suporte</a>.</p>
</aside>


  <dt><strong><a href="https://tldp.org/docs.html#man">Páginas de manual (manpages)</a></strong></dt>
    <dd>Tradicionalmente, todos os programas Unix são documentados com
    <em>páginas de manual</em>, manuais de referência disponíveis através do
        comando <tt>man</tt>. Eles normalmente não são para novatos(as), mas
        documentam todos os recursos e funções de um comando. O repositório
        completo de todas páginas de manual disponíveis no Debian estão on-line:
        <a href="https://manpages.debian.org/cgi-bin/man.cgi">https://manpages.debian.org/</a>.
    </dd>

  <dt><strong><a href="https://www.gnu.org/software/texinfo/manual/texinfo/html_node/index.html">Arquivos info</a></strong></dt>
    <dd>Muitos programas GNU são documentados através de <em>arquivos info</em>
        em vez de páginas de manual. Esses arquivos incluem informações
        detalhadas sobre o programa, opções e exemplos de uso, e estão
        disponíveis através do comando <tt>info</tt>.
    </dd>

  <dt><strong>Arquivos LEIA-ME (README)</strong></dt>
    <dd>Os arquivos LEIA-ME são arquivos de texto simples que descrevem um
        item, normalmente um pacote. Você pode encontrar muitos deles nos
        subdiretórios em <tt>/usr/share/doc/</tt> no seu sistema Debian.
        Adicionalmente ao arquivo LEIA-ME, alguns desses diretórios incluem
        exemplos de configuração. Por favor note que a documentação de programas
        maiores é geralmente fornecida em um pacote separado (com o mesmo nome
        do pacote original, mas terminado com <em>-doc</em>).
    </dd>

  <dt><strong><a href="https://www.debian.org/doc/manuals/refcard/refcard">Cartão de referência rápida</a></strong></dt>
    <dd>Cartões de referência rápida são resumos bem curtos de um (sub)sistema
        específico e geralmente listam os comandos mais usados em um único
        pedaço de papel. É necessário, pelo menos, um conhecimento básico de
        arquivos, diretórios e linha de comando. Usuários(as) novatos(as) podem
        preferir ler primeiro a
        <a href="user-manuals#quick-reference">referência Debian</a>.
    </dd>

</dl>
</p>
