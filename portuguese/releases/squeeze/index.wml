#use wml::debian::template title="Informações de lançamento do Debian &ldquo;squeeze&rdquo;"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/squeeze/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="58e81746146e641fc4f31e21e147c76abd0b7c5a"

<p>O Debian <current_release_squeeze> foi lançado em
<a href="$(HOME)/News/<current_release_newsurl_squeeze/>"><current_release_date_squeeze></a>.
<ifneq "6.0.0" "<current_release>"
  "Debian 6.0.0 was initially released on <:=spokendate('2011-02-06'):>."
/>
O lançamento incluiu várias grandes mudanças, descritas em
nosso <a href="$(HOME)/News/2011/20110205a">comunicado à imprensa</a> e
nas <a href="releasenotes">notas de lançamento</a>.</p>

<p><strong>O Debian 6 foi substituído pelo
<a href="../wheezy/">Debian 7 (<q>wheezy</q>)</a>.
Atualizações de segurança foram descontinuadas em <:=spokendate('2014-05-31'):>.
</strong></p>

<p><strong>O squeeze se beneficiou do suporte de longo prazo (LTS - Long Term
Support) até o final de fevereiro de 2016. O LTS era limitado ao i386 e amd64.
Para mais informações, consulte a <a
href="https://wiki.debian.org/LTS">seção LTS da wiki do Debian</a>.
</strong></p>

<p>Para obter e instalar o Debian, veja a página
<a href="debian-installer/">informações de instalação</a> e o
<a href="installmanual">guia de instalação</a>. Para atualizar a partir de uma
versão mais antiga do Debian, veja as instruções nas
<a href="releasenotes">notas de lançamento</a>.</p>

<p>As seguintes arquiteturas de computadores são suportadas nesta versão:</p>

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Ao contrário do que desejamos, pode haver alguns problemas existentes na
versão, mesmo que ela seja declarada <em>estável (stable)</em>. Nós fizemos
<a href="errata">uma lista dos principais problemas conhecidos</a>, e
você sempre pode nos
<a href="reportingbugs">relatar outros problemas</a>.</p>

<p>Por último, mas não menos importante, nós temos uma lista de <a
href="credits">pessoas que merecem crédito</a> por fazer este
lançamento acontecer.</p>
