#use wml::debian::translation-check translation="26497e59c7b2d67a683e46d880d05456d453f794"
<define-tag pagetitle>Opdateret Debian 10: 10.2 udgivet</define-tag>
<define-tag release_date>2019-11-16</define-tag>
<define-tag frontpage>yes</define-tag>

#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.2</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>Debian-projektet er stolt over at kunne annoncere den anden opdatering af 
dets stabile distribution, Debian <release> (kodenavn <q><codename></q>).
Denne opdatering indeholder primært rettelser af sikkerhedsproblemer i den 
stabile udgave, sammen med nogle få rettelser af alvorlige problemer.  
Sikkerhedsbulletiner er allerede udgivet separat og der vil blive refereret til 
dem, hvor de er tilgængelige.</p>

<p>Bemærk at denne opdatering ikke er en ny udgave af Debian GNU/Linux
<release>, den indeholder blot opdateringer af nogle af de medfølgende pakker.  
Der er ingen grund til at smide gamle <q><codename></q>-medier væk.  Efter en 
installering, kan pakkerne opgradere til de aktuelle versioner ved hjælp af et 
ajourført Debian-filspejl.</p>

<p>Dem der hyppigt opdaterer fra security.debian.org, behøver ikke at opdatere 
ret mange pakker, og de fleste opdateringer fra security.debian.org er indeholdt 
i denne opdatering.</p>

<p>Nye installeringsfilaftryk vil snart være tilgængelige fra de sædvanlige 
steder.</p>

<p>Opdatering af en eksisterende installation til denne revision, kan gøres ved 
at lade pakkehåndteringssystemet pege på et af Debians mange HTTP-filspejle. En 
omfattende liste over filspejle er tilgængelig på:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Forskellige fejlrettelser</h2>

<p>Denne opdatering til den stabile udgave tilføjer nogle få vigtige rettelser 
til følgende pakker:</p>

<table border=0>
<tr><th>Pakke</th>				<th>Årsag</th></tr>
<correction aegisub 				"Retter nedbrud når der vælges et sprog fra bunden af listen <q>Spell checker language</q>; retter nedbrud når der højreklikkes i undertekster-tekstboksen">
<correction akonadi 				"Retter forskellige problemer med nedbrud og deadlock">
<correction base-files 				"Opdaterer /etc/debian_version til denne punktopdatering">
<correction capistrano 				"Retter mislykket fjernelse af gamle udgivelser når der var for mange">
<correction cron 				"Holder op med at anvende forældet SELinux-API">
<correction cyrus-imapd 			"Retter datatab ved opgradering fra version 3.0.0 eller tidligere">
<correction debian-edu-config 			"Håndterer nyere Firefox ESR-opsætningsfiler; tilføjer post-up-strofe til /etc/network/interfaces eth0-posten under visse omstændigheder">
<correction debian-installer 			"Retter ulæselige skrifttyper på hidpi-skærme i netboot-filaftryk som er boot'et med EFI">
<correction debian-installer-netboot-images 	"Genopbygger mod proposed-updates">
<correction distro-info-data 			"Tilføjer Ubuntu 20.04 LTS, Focal Fossa">
<correction dkimpy-milter 			"Ny stabil opstrømsudgave; retter sysvinit-understøttelse; fanger flere ASCII-kodningsfejl for at forbedre modstandskraften mod dårlige data; retter meddelelsesudtrækkelse således at signering i det samme forløb kommer igennem milter'en da verifikationen fungerer korrekt">
<correction emacs 				"Opdaterer EPLA-pakningsnøglen">
<correction fence-agents 			"Retter ufuldstændig fjernelse af fence_amt_ws">
<correction flatpak 				"Ny stabil opstrømsudgave">
<correction flightcrew 				"Sikkerhedsrettelser [CVE-2019-13032 CVE-2019-13241]">
<correction fonts-noto-cjk 			"Retter for aggressiv udvælgelse af Noto CJK-skrifttyper i moderne webbrowsere med kinesisk sprog">
<correction freetype 				"Håndterer på korrekt vis spøgelsespunkter for variable hintede skrifttyper">
<correction gdb 				"Genopbygger mod ny libbabeltrace, med højere versionsnummer for at undgå konflikt med tidligere upload">
<correction glib2.0 				"Sikrer at libdbus-klienter kan autentificere med en GDBusServer, som den i ibus">
<correction gnome-shell 			"Ny stabil opstrømsudgave; retter trunkering af lange meddeleser i Shell-modal-dialoger; undgår nedbrud ved genallokering af døde actors">
<correction gnome-sound-recorder 		"Retter nedbrud ved valg af en optagelse">
<correction gnustep-base 			"Deaktiverer gdomap-dæmon som ved en fejl blev aktiveret ved opgradering fra stretch">
<correction graphite-web 			"Fjerner ikke benyttet <q>send_email</q>-funktion [CVE-2017-18638]; undgår en fejl hver time i cron når der ikke er en whisper-database">
<correction inn2 				"Retter forhandling af DHE-ciphersuiter">
<correction libapache-mod-auth-kerb 		"Retter fejl vedrørende anvendelse efter frigivelse førende til nedbrud">
<correction libdate-holidays-de-perl 		"Markerer den internationale børnedag (Weltkindertag, 20. september) som en fridag i Thüringen fra 2019 og frem">
<correction libdatetime-timezone-perl 		"Opdaterer medfølgende data">
<correction libofx 				"Retter problem med nullpointerdereference [CVE-2019-9656]">
<correction libreoffice 			"Retter postgresql-driveren med PostgreSQL 12">
<correction libsixel 				"Retter flere sikkerhedsproblemer [CVE-2018-19756 CVE-2018-19757 CVE-2018-19759 CVE-2018-19761 CVE-2018-19762 CVE-2018-19763 CVE-2019-3573 CVE-2019-3574]">
<correction libxslt 				"Retter dinglende pointer i xsltCopyText [CVE-2019-18197]">
<correction lucene-solr 			"Deaktiverer forældet kald til ContextHandler i solr-jetty9.xml; retter Jetty-rettigheder i SOLR-indeks">
<correction mariadb-10.3 			"Ny stabil opstrømsudgave">
<correction modsecurity-crs 			"Retter uploadregler for PHP-skript [CVE-2019-13464]">
<correction mutter 				"Nu staboæ opstrømsudgave">
<correction ncurses 				"Retter flere sikkerhedsproblemer [CVE-2019-17594 CVE-2019-17595] og andre problemer i tic">
<correction ndppd 				"Undgår verdensskrivbar PID-fil, som gjorde at dæmoners initskripts ikke fungerede">
<correction network-manager 			"Retter filrettigheder for <q>/var/lib/NetworkManager/secret_key</q> og <q>/var/lib/NetworkManager</q>">
<correction node-fstream 			"Retter problem med overskrivning af vilkårlig fil [CVE-2019-13173]">
<correction node-set-value 			"Retter prototypeforurening [CVE-2019-10747]">
<correction node-yarnpkg 			"Gennemtvinger anvendelse af HTTPS til regulære registreringer">
<correction nx-libs 				"Retter regressioner opstået i tidligere upload, som påvirker x2go">
<correction open-vm-tools 			"Retter hukommelseslækager og fejlhåndtering">
<correction openvswitch 			"Opdaterer debian/ifupdown.sh så opsætning af MTU'en er tilladt; retter Python-afhængigheder til at anvende Python 3">
<correction picard 				"Opdaterer oversættelser for at rette nedbrud med spansk lokaltilpasning">
<correction plasma-applet-redshift-control 	"Retter manuel tilstand når den anvendes med redshift-versioner over 1.12">
<correction postfix 				"Ny stabil opstrømsudgave; omgår dårlig TCP-loopbackydeevne">
<correction python-cryptography 		"Retter testsuitefejl når opbygget mod nyere OpenSSL-versioner; retter en hukommelseslækage udløsbar ved fortolkning af x509-certifikatudvidelser så som AIA">
<correction python-flask-rdf 			"Tilføjer Depends af python{3,}-rdflib">
<correction python-oslo.messaging 		"Ny stabil opstrømsudgave; retter skift af forbindelsesmål når en rabbitmq-klyngenode forsvinder">
<correction python-werkzeug 			"Sikrer at Docker-containere har unikke debugger-PINs [CVE-2019-14806]">
<correction python2.7 				"Retter flere sikkerhedsproblemer [CVE-2018-20852 CVE-2019-10160 CVE-2019-16056 CVE-2019-16935 CVE-2019-9740 CVE-2019-9947]">
<correction quota 				"Retter rpc.rquotad som har et CPU-forbrug på 100 procent">
<correction rpcbind 				"Tillader at fjernkald kan være aktive på kørselstidspunktet">
<correction shelldap 				"Reparerer SASL-autentifikationer, tilføjer en <q>sasluser</q>-valgmulighed">
<correction sogo 				"Retter visning af PGP-signerede mails">
<correction spf-engine 				"Ny stabile opstrømsudgave; retter sysvinit-understøttelse">
<correction standardskriver 			"Retter advarsel om udfasning fra config.RawConfigParser; anvender ekstern <q>ip</q>-kommando frem for den udfasede <q>ifconfig</q>-kommando">
<correction swi-prolog 				"Anvender HTTPS når opstrøms packservere kontaktes">
<correction systemd 				"core: Viderefør aldrig genindlæsningsfejl til serviceresultat; retter sync_file_range-fejl i nspawn-containere på arm, ppc; retter at RootDirectory ikke fungerer når den anvendes sammen med User; sikrer at adgangskontroller på systemd-resolved's D-Bus-grænseflade håndhæves korrekt [CVE-2019-15718]; retter StopWhenUnneeded=true angående mountunits; gør at MountFlags=shared fungerer igen">
<correction tmpreaper 				"Forhindrer at systemd-tjenester som anvender PrivateTmp=true holder op med at virke">
<correction trapperkeeper-webserver-jetty9-clojure "Genindfører SSL-kompatibilitet med nyere Jetty-versioner">
<correction tzdata 				"Ny opstrømsudgave">
<correction ublock-origin 			"Ny opstrømsversion, kompatible med Firefox ESR68">
<correction uim 				"Genopliver libuim-data som en transitionspakke, retter nogle problemer efter opgradering til buster">
<correction vanguards 				"Ny stabil opstrømsudgave; forhindrer en genindlæsning af tors opsætning via SIGHUP, forårsagende et lammelsesangreb i vanguards beskyttelser">
</table>


<h2>Sikkerhedsopdateringer</h2>

<p>Denne revision tilføjer følgende sikkerhedsopdateringer til den stabile 
udgave.  Sikkerhedsteamet har allerede udgivet bulletiner for hver af de nævnte
opdateringer:</p>

<table border=0>
<tr><th>Bulletin-id</th>  <th>Pakke(r)</th></tr>
<dsa 2019 4509 apache2>
<dsa 2019 4511 nghttp2>
<dsa 2019 4512 qemu>
<dsa 2019 4514 varnish>
<dsa 2019 4515 webkit2gtk>
<dsa 2019 4516 firefox-esr>
<dsa 2019 4517 exim4>
<dsa 2019 4518 ghostscript>
<dsa 2019 4519 libreoffice>
<dsa 2019 4520 trafficserver>
<dsa 2019 4521 docker.io>
<dsa 2019 4523 thunderbird>
<dsa 2019 4524 dino-im>
<dsa 2019 4525 ibus>
<dsa 2019 4526 opendmarc>
<dsa 2019 4527 php7.3>
<dsa 2019 4528 bird>
<dsa 2019 4530 expat>
<dsa 2019 4531 linux-signed-amd64>
<dsa 2019 4531 linux-signed-i386>
<dsa 2019 4531 linux>
<dsa 2019 4531 linux-signed-arm64>
<dsa 2019 4532 spip>
<dsa 2019 4533 lemonldap-ng>
<dsa 2019 4534 golang-1.11>
<dsa 2019 4535 e2fsprogs>
<dsa 2019 4536 exim4>
<dsa 2019 4538 wpa>
<dsa 2019 4539 openssl>
<dsa 2019 4539 openssh>
<dsa 2019 4541 libapreq2>
<dsa 2019 4542 jackson-databind>
<dsa 2019 4543 sudo>
<dsa 2019 4544 unbound>
<dsa 2019 4545 mediawiki>
<dsa 2019 4547 tcpdump>
<dsa 2019 4549 firefox-esr>
<dsa 2019 4550 file>
<dsa 2019 4551 golang-1.11>
<dsa 2019 4553 php7.3>
<dsa 2019 4554 ruby-loofah>
<dsa 2019 4555 pam-python>
<dsa 2019 4556 qtbase-opensource-src>
<dsa 2019 4557 libarchive>
<dsa 2019 4558 webkit2gtk>
<dsa 2019 4559 proftpd-dfsg>
<dsa 2019 4560 simplesamlphp>
<dsa 2019 4561 fribidi>
<dsa 2019 4562 chromium>
</table>


<h2>Fjernede pakker</h2>

<p>Følgende pakker er blevet fjernet på grund af omstændigheder uden for vores 
kontrol:</p>

<table border=0>
<tr><th>Pakke</th>				<th>Årsag</th></tr>
<correction firefox-esr 			"[armel] Kan ikke længere understøttes på grund af opbygningsafhængighed af nodejs">
</table>


<h2>Debian Installer</h2>

Installeringsprogrammet er opdateret for at medtage rettelser indført i stable, 
i denne punktopdatering.


<h2>URL'er</h2>

<p>Den komplette liste over pakker, som er ændret i forbindelse med denne 
revision:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Den aktuelle stabile distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Foreslåede opdateringer til den stabile distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Oplysninger om den stabile distribution (udgivelsesbemærkninger, fejl, 
osv.):</p>

<div class="center">
  <a href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sikkerhedsannonceringer og -oplysninger:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>Om Debian</h2>

<p>Debian-projektet er en organisation af fri software-udviklere som frivilligt
bidrager med tid og kræfter, til at fremstille det helt frie styresystem Debian
GNU/Linux.</p>


<h2>Kontaktoplysninger</h2>

<p>For flere oplysninger, besøg Debians websider på 
<a href="$(HOME)/">https://www.debian.org/</a> eller send e-mail på engelsk til
&lt;press@debian.org&gt; eller kontakt holdet bag den stabile udgave på 
&lt;debian-release@debian.org&gt;.</p>
