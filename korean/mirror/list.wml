#use wml::debian::template title="Debian worldwide mirror sites" BARETITLE=true
#use wml::debian::translation-check translation="b554c22abdbc4a6253951c9bf9610405d0c4f2cd" maintainer="Sebul"

<p>
데비안은 인터넷의 수백 서버에서 배포(<em>mirrored</em>)됩니다. 
가까운 서버를 쓰면 다운로드 속도가 빨라질 것이고, 중심 서버 그리고 인터넷 전체의 부하를 줄일 겁니다.
</p>

<p class="centerblock">
데비안 미러는 여러 나라에 존재하며, 일부 나라에서는 <code>ftp.&lt;country&gt;.debian.org</code> alias를 추가했습니다. 
이 alias는 일반적으로 정기적으로 빠르게 동기화하고 데비안의 모든 아키텍처를 전달하는 미러를 가리킵니다. 
데비안 아카이브는 서버의 <code>/debian</code> 위치에서 <code>HTTP</code>를 통해 항상 사용할 수 있습니다.
</p>

<p class="centerblock">
다른 <strong>미러 사이트</strong>에는 공간 제한으로 인해 미러링하는 항목에 제한이 있을 수 있습니다. 
단지 사이트가 나라의 <code>ftp.&lt;country&gt;.debian.org</code>가 아니라고 해서 
<code>ftp.&lt;country&gt;.debian.org</code>보다 느리거나 최신 상태임을 뜻하지는 않습니다. 
사실 사용자로서 사용자의 아키텍처를 전달하고 더 가까운 곳에 있는 미러가, 더 멀리 있는 다른 미러보다 훨씬 빠릅니다.
</p>

<p>
국가별 미러 alias 여부에 관계없이 가장 빠른 다운로드를 위해 가장 가까운 사이트를 사용하십시오. 
<a href="https://packages.debian.org/stable/net/netselect">\
<code>netselect</code></a> 프로그램은 지연 시간이 가장 짧은 사이트를 확인하는 데 사용할 수 있으며, 
<a href="https://packages.debian.org/stable/web/wget">\
<code>wget</code></a> 또는 <a href="https://packages.debian.org/stable/net/rsync">\
<code>rsync</code></a> 와 같은 다운로드 프로그램을 사용하여 처리량이 가장 많은 사이트를 확인할 수 있습니다. 
지리적 근접성이 사용자에게 가장 적합한 기계를 결정하는 데 가장 중요한 요소가 아닌 경우가 많습니다.
</p>

<p>
시스템 이동이 많은 경우 글로벌 <abbr title="Content Delivery Network">CDN</abbr>이 지원하는 "미러"가 가장 적합합니다. 
데비안 프로젝트는 이러한 목적을 위해 <code>deb.debian.org</code>을 유지하며, 
자세한 내용은 sources.list에서 확인할 수 있습니다.&mdash; 
<a href="http://deb.debian.org/">서비스 웹 사이트</a>에 상의하세요.
</p>

<p>
데비안 미러에 대해 알고 싶은 다른 모든 것:
<url "https://www.debian.org/mirror/">.
</p>

<h2 class="center">데비안 나라별 alias</h2>

<table border="0" class="center">
<tr>
  <th>나라</th>
  <th>사이트</th>
  <th>아키텍처</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>

<h2 class="center">데비안 아카이브 미러 리스트</h2>

<table border="0" class="center">
<tr>
  <th>Host 이름</th>
  <th>HTTP</th>
  <th>아키텍처</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"
