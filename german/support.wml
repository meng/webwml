#use wml::debian::template title="Unterstützung unserer Benutzer" MAINPAGE="true"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="c7a64739b495b3bd084b972d820ef78fc30a3f8a"
# Translator: Philipp Frauenfelder <pfrauenf@debian.org>
# Updated by: Thimo Neubauer <thimo@debian.org>
# Updated by: Holger Wansing <linux@wansing-online.de>, 2011, 2016, 2018.
# Updated by: Holger Wansing <hwansing@mailbox.org>, 2019, 2020, 2022.
# Updated by: Fabian Baumanis <fabian.baumanis@mailbox.org>, 2022.

<link href="$(HOME)/font-awesome.css" rel="stylesheet" type="text/css">


<ul class="toc">
  <li><a href="#irc">IRC (Online Echtzeit-Hilfe)</a></li>
  <li><a href="#mail_lists">Mailinglisten</a></li>
  <li><a href="#usenet">Usenet Newsgroups</a></li>
  <li><a href="#forums">Debian Benutzerforen</a></li>
  <li><a href="#maintainers">Wie man einen Paketbetreuer kontaktiert</a></li>
  <li><a href="#bts">Fehlerdatenbank</a></li>
  <li><a href="#release">Bekannte Probleme</a></li>
</ul>

<aside>
<p><span class="fas fa-caret-right fa-3x"></span> Debians Benutzerunterstützung wird von einer Gemeinschaft
         Freiwilliger betrieben. Falls diese Unterstützung nicht Ihren Anforderungen entspricht und Sie
         auch in unserer <a href="doc/">Dokumentation</a> keine passenden Antworten auf Ihre Fragen finden,
         können Sie auch einen <a href="consultants/">Berater</a> konsultieren, der Ihnen Ihre Fragen
         beantwortet oder Funktionalität auf Ihrem Debian-System betreut oder hinzufügt.
</p>
</aside>

<h2><a id="irc">IRC (Online Echtzeit-Hilfe)</a></h2>

<p> <a href="http://www.irchelp.org/">IRC</a> (Internet Relay Chat) ist eine tolle Möglichkeit, in Echtzeit mit
    anderen Personen auf der gesamten Welt zu kommunizieren. Es ist ein textbasiertes Chat-System für
    Instant-Nachrichten. Bei IRC können Sie Chat-Räume (sogenannte Kanäle) betreten oder mit einzelnen
    Personen direkt über private Nachrichten kommunizieren.
</p>

<p>Debian betreffende IRC-Kanäle finden Sie auf der <a href="https://www.oftc.net/">OFTC</a>-Website.
   Eine vollständige Liste aller Debian-Kanäle finden Sie in unserem <a href="https://wiki.debian.org/IRC">Wiki</a>.
   Sie können auch eine <a href="https://netsplit.de/channels/index.en.php?net=oftc&chat=debian">Suchmaschine</a>
   benutzen, um Debian-betreffende Kanäle zu finden.
</p>

<h3>IRC-Clients</h3>

<p>Um sich mit dem IRC-Netzwerk zu verbinden, können Sie entweder das Web-Interface
   <a href="https://www.oftc.net/WebChat/">WebChat</a> von OFTC in Ihrem bevorzugten Browser aufrufen oder
   ein Client-Programm auf Ihrem Rechner installieren. Es gibt eine Reihe verschiedener Clients,
   einige davon mit einer grafischen Bedienoberfläche, andere für die Konsole. Einige der beliebtesten
   IRC-Clients sind für Debian paketiert worden, z.B.:
</p>

<ul>
  <li><a href="https://packages.debian.org/stable/net/irssi">irssi</a> (Textmodus)</li>
  <li><a href="https://packages.debian.org/stable/net/weechat-curses">WeeChat</a> (Textmodus)</li>
  <li><a href="https://packages.debian.org/stable/net/hexchat">HexChat (GTK)</a></li>
  <li><a href="https://packages.debian.org/stable/net/konversation">Konversation</a> (KDE)</li>
</ul> 

<p>Das Debian Wiki enthält eine umfangreichere <a href="https://wiki.debian.org/IrcClients">Liste von IRC-Clients</a>,
   die als Debian-Pakete verfügbar sind.
</p>

<h3>Sich mit dem Netzwerk verbinden</h3>

<p>Wenn Sie den Client installiert haben, müssen Sie ihm mitteilen, sich mit dem Server zu verbinden.
   In den meisten Clients erreichen Sie dies, in dem Sie Folgendes eingeben:
</p>

<pre>
/server irc.debian.org
</pre>

<p>Der Hostname irc.debian.org ist ein Alias für irc.oftc.net. In einigen Clients (wie z.B. irssi) müssen Sie
   stattdessen dies verwenden:</p>

<pre>
/connect irc.debian.org
</pre>

# Note to translators:
# You might want to insert here a paragraph stating which IRC channel is available
# for user support in your language and pointing to the English IRC channel.
<h3>Einem Kanal beitreten</h3>

<p>Sobald Sie verbunden sind, können Sie durch Eingabe von
</p>

<pre>
/join #debian.de
</pre>

<p>
dem Kanal <code>#debian.de</code> beitreten, um Unterstützung in deutscher Sprache zu erhalten.
</p>

<p>
Hilfe in englischer Sprache erhalten Sie auf dem Kanal <code>#debian</code> durch Eingabe von
</p>

<pre>
/join #debian
</pre>

<p>Beachten Sie: viele grafische Clients wie HexChat oder Konversation haben einen
   Knopf oder einen Menüeintrag für die Verbindung zu Servern und Kanälen.
</p>

<p style="text-align:center"><button type="button"><span class="fas fa-book-open fa-2x"></span> <a href="https://wiki.debian.org/DebianIRC">Lesen Sie unsere IRC-FAQ</a></button></p>

<h2><a id="mail_lists">Mailinglisten</a></h2>

<p>Mehr als tausend aktive <a href="intro/people.en.html#devcont">Entwickler</a>, verteilt über die ganze
   Welt, arbeiten in Ihrer Freizeit - und in Ihrer jeweiligen Zeitzone - an Debian. Daher kommunizieren wir
   primär über E-Mail. Entsprechend läuft die Kommunikation zwischen Debian-Entwicklern und -Benutzern
   überwiegend über verschiedene <a href="MailingLists/">Mailinglisten</a>:
</p>

# Note to translators:
# You might want to adapt the following paragraph, stating which list
# is available for user support in your language instead of English.

<ul>
  <li>Bezüglich der Unterstützung für Benutzer in deutscher Sprache kontaktieren Sie bitte die Mailingliste
      <a href="https://lists.debian.org/debian-user-german/">debian-user-german</a>.</li>
  <li>Falls Sie Unterstützung in einer anderen Sprache benötigen, suchen Sie bitte im
      <a href="https://lists.debian.org/users.html">Mailinglisten-Index</a> nach <q>user</q>.</li>
</ul>

<p>Sie können die <a href="https://lists.debian.org/">Archive unserer Mailinglisten</a> durchstöbern oder
   <a href="https://lists.debian.org/search.html">durchsuchen</a>, ohne die Listen abonnieren zu müssen.
</p>

<p>Es gibt natürlich noch viele andere Mailinglisten, die sich jeweils unterschiedlichen Aspekten des
   weitläufigen Linux-Ökosystems widmen und nicht Debian-spezifisch sind. Verwenden Sie bitte Ihre bevorzugte
   Suchmaschine, um die für Ihre Zwecke am besten geeignete Liste zu finden.
</p>

<h2><a id="usenet">Usenet Newsgroups</a></h2>

<p>Viele unserer <a href="#mail_lists">Mailinglisten</a> können als Newsgroups gelesen werden, suchen Sie dazu
   in der <kbd>linux.debian.*</kbd> Hierarchie.
</p>

<h2><a id="forums">Debian Benutzerforen</h2>

# Note to translators:
# If there is a specific Debian forum for your language you might want to
# insert here a paragraph stating which list is available for user support
# in your language and pointing to the English forums.
# <p><a href="http://someforum.example.org/">someforum</a> is a web portal
# on which you can use your language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>
#
# <p><a href="https://forums.debian.net">Debian User Forums</a> is a web portal
# on which you can use the English language to discuss Debian-related topics,
# submit questions about Debian, and have them answered by other users.</p>

<p><a href="https://debianforum.de/forum/">Debianforum.de</a> ist ein
   Webforum, in dem Sie in deutscher Sprache Themen mit Bezug zu Debian
   diskutieren sowie Fragen über Debian stellen können und diese von anderen
   Benutzern beantwortet bekommen.
</p>

<p><a href="https://forums.debian.net">Debian User Forums</a> ist ein Webportal
   mit tausenden Nutzern und ähnlichen Zielsetzungen, jedoch wird dort die englische
   Sprache genutzt. Sie können alle Foren lesen, ohne sich registrieren zu müssen.
   Wenn Sie sich an Diskussionen beteiligen möchten, registrieren Sie sich bitte
   und loggen Sie sich ein.
</p>

<h2><a id="maintainers">Wie man einen Paketbetreuer kontaktiert</a></h2>

<p>Es gibt grundsätzlich zwei Wege, den Betreuer eines Pakets zu erreichen:
</p>

<ul>
  <li>Wenn Sie dem Betreuer einen Fehler in seinem Paket melden möchten, füllen Sie einfach einen <a href="Bugs/Reporting">Fehlerbericht</a>
      aus. Der Betreuer erhält automatisch eine Kopie des Fehlerberichts.</li>
  <li>Falls Sie dem Betreuer einfach nur eine E-Mail schicken möchten, können Sie die speziellen Mail-Alias-Adressen nutzen,
      die für jedes Paket eingerichtet sind: &lt;<em>paketname</em>&gt;@packages.debian.org</li>
</ul>

<h2><a id="bts">Fehlerdatenbank</a></h2>

<p>Die Debian-Distribution betreibt einen <a href="Bugs/">Bug Tracker (eine Fehlerdatenbank)</a> mit Fehlerberichten, die von
   Entwicklern oder Benutzern gemeldet wurden. Jeder Fehler erhält eine eindeutige Nummer und bleibt solange im System aktiv,
   bis der Fehler als behoben erklärt wird. Es gibt zwei verschiedene Wege, einen Fehler zu melden:
</p>

<ul>
  <li>Es wird primär empfohlen, das Debian-Paket <em>reportbug</em> zu nutzen.</li>
  <li>Alternativ können Sie auch eine E-Mail schicken, wie auf dieser <a href="Bugs/Reporting">Seite beschrieben</a>.</li>
</ul>

<h2><a id="release">Bekannte Probleme</a></h2>

<p>Einschränkungen und gravierende Probleme der aktuellen Stable-Distribution (sofern vorhanden) werden auf den
   <a href="releases/stable/">Seiten zur Stable-Veröffentlichung</a> aufgeführt.
</p>

<p>Sie sollten speziell die <a href="releases/stable/releasenotes">Veröffentlichungshinweise</a>
   und die <a href="releases/stable/errata">Errata</a> (Auflistung bekannter Fehler) lesen.
</p>
