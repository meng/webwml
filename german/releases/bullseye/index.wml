#use wml::debian::template title="Informationen zur Debian-<q>Bullseye</q>-Veröffentlichung"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/bullseye/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="4c728c46ce7c43bebb50236f2439e9bc75f33a36"

<p>Debian <current_release_bullseye> wurde am <a href="$(HOME)/News/<current_release_newsurl_bullseye/>"><current_release_date_bullseye></a>
veröffentlicht.
<ifneq "11.0" "<current_release>"
"(Debian 11.0 wurde ursprünglich am <:=spokendate('2021-08-14'):> freigegeben.)"
/>
Diese Veröffentlichung enthält größere Änderungen, die in
unserer <a href="$(HOME)/News/2021/20210814">Pressemitteilung</a> und
in den <a href="releasenotes">Veröffentlichungshinweisen</a> beschrieben
sind.</p>

<p><strong>Debian 11 wurde durch
<a href="../bookworm/">Debian 12 (<q>Bookworm</q>)</a> abgelöst.
#Sicherheitsaktualisierungen wurden am <:=spokendate('xxxx-xx-xx'):> eingestellt.
#</strong></p>

### This paragraph is orientative, please review before publishing!
#<p><strong>Bullseye profitiert jedoch zusätzlich bis Ende Juni 2026 vom Long Term Support (LTS), d. h.
#bis zu diesem Zeitpunkt werden Sicherheits-Updates für Bullseye bereitgestellt.
#Dies ist allerdings beschränkt auf i386, amd64, armel, armhf und arm64. Weitere Informationen
#hierzu finden Sie im <a
#href="https://wiki.debian.org/LTS">LTS-Abschnitt des Debian-Wikis</a>.
#</strong></p>

<p>Um Debian zu beschaffen und zu installieren, lesen Sie die
<a href="debian-installer/">Webseite zum Debian-Installer</a> und die <a
href="installmanual">Installationsanleitung</a>. Wenn Sie ein Upgrade von einer
älteren Debian-Veröffentlichung durchführen möchten, lesen Sie die Anleitung in den
<a href="releasenotes">Veröffentlichungshinweisen</a>.</p>

### Activate the following when LTS period starts.
#<p>Während der Long-Term-Support-Periode unterstützte Architekturen:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Zur ursprünglichen Freigabe von Bullseye unterstützte Architekturen:</p>

<ul>
<:
foreach $arch (@arches) {
print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>Entgegen unseren Wünschen könnte es in der Veröffentlichung noch einige
Probleme geben, obwohl sie als <em>stabil</em> deklariert
wurde. Wir haben <a href="errata">eine Liste bekannter größerer Probleme</a>
erstellt und Sie können uns jederzeit <a href="reportingbugs">weitere
Probleme berichten</a>.</p>

<p>Zu guter Letzt finden Sie hier auch <a href="credits">eine Liste der Personen,
denen Dank dafür gebührt</a>, dass diese Veröffentlichung erfolgen konnte.</p>

