#
# Trần Ngọc Quân <vnwildman@gmail.com>, 2015, 2016.
#
msgid ""
msgstr ""
"Project-Id-Version: webwml templates\n"
"POT-Creation-Date: \n"
"PO-Revision-Date: 2022-03-02 14:15+0700\n"
"Last-Translator: Trần Ngọc Quân <vnwildman@gmail.com>\n"
"Language-Team: Vietnamese <debian-l10n-vietnamese@lists.debian.org>\n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ../../english/search.xml.in:7
msgid "Debian website"
msgstr "Trang thông tin điện tử Debian"

#: ../../english/search.xml.in:9
msgid "Search the Debian website."
msgstr "Tìm kiếm trang thông tin điện tử Debian."

#: ../../english/template/debian/basic.wml:19
#: ../../english/template/debian/navbar.wml:11
msgid "Debian"
msgstr "Debian"

#: ../../english/template/debian/basic.wml:48
msgid "Debian website search"
msgstr "Tìm kiếm trên trang thông tin điện tử Debian"

#: ../../english/template/debian/common_translation.wml:4
msgid "Yes"
msgstr "Có"

#: ../../english/template/debian/common_translation.wml:7
msgid "No"
msgstr "Không"

#: ../../english/template/debian/common_translation.wml:10
msgid "Debian Project"
msgstr "Dự án Debian"

#: ../../english/template/debian/common_translation.wml:13
msgid ""
"Debian is an operating system and a distribution of Free Software. It is "
"maintained and updated through the work of many users who volunteer their "
"time and effort."
msgstr ""
"Debian là hệ điều hành và là một bản phân phối của Phần mềm Tự do. Nó được "
"bảo trì và cập nhật thông qua việc làm của nhiều người những người mà tình "
"nguyện dành thời gian và công sức để đóng góp."

#: ../../english/template/debian/common_translation.wml:16
msgid "debian, GNU, linux, unix, open source, free, DFSG"
msgstr "debian, GNU, linux, unix, open source, nguồn mở, free, tự do, DFSG"

#: ../../english/template/debian/common_translation.wml:19
msgid "Back to the <a href=\"m4_HOME/\">Debian Project homepage</a>."
msgstr "Quay trở lại <a href=\"m4_HOME/\">trang chủ Dự án Debian</a>."

#: ../../english/template/debian/common_translation.wml:22
#: ../../english/template/debian/links.tags.wml:149
msgid "Home"
msgstr "Trang chủ"

#: ../../english/template/debian/common_translation.wml:25
msgid "Skip Quicknav"
msgstr "Ẩn bảng điều hướng"

#: ../../english/template/debian/common_translation.wml:28
msgid "About"
msgstr "Giới thiệu"

#: ../../english/template/debian/common_translation.wml:31
msgid "About Debian"
msgstr "Giới thiệu về Debian"

#: ../../english/template/debian/common_translation.wml:34
msgid "Contact Us"
msgstr "Liên hệ với chúng tôi"

#: ../../english/template/debian/common_translation.wml:37
msgid "Legal Info"
msgstr "Thông tin pháp luật"

#: ../../english/template/debian/common_translation.wml:40
msgid "Data Privacy"
msgstr "Riêng tư dữ liệu"

#: ../../english/template/debian/common_translation.wml:43
msgid "Donations"
msgstr "Quyên góp"

#: ../../english/template/debian/common_translation.wml:46
msgid "Events"
msgstr "Sự kiện"

#: ../../english/template/debian/common_translation.wml:49
msgid "News"
msgstr "Tin tức"

#: ../../english/template/debian/common_translation.wml:52
msgid "Distribution"
msgstr "Bản phân phối"

#: ../../english/template/debian/common_translation.wml:55
msgid "Support"
msgstr "Hỗ trợ"

#: ../../english/template/debian/common_translation.wml:58
msgid "Pure Blends"
msgstr "Pure Blends"

#: ../../english/template/debian/common_translation.wml:61
#: ../../english/template/debian/links.tags.wml:46
msgid "Developers' Corner"
msgstr "Góc phát triển"

#: ../../english/template/debian/common_translation.wml:64
msgid "Documentation"
msgstr "Tài liệu"

#: ../../english/template/debian/common_translation.wml:67
msgid "Security Information"
msgstr "Thông tin bảo mật"

#: ../../english/template/debian/common_translation.wml:70
msgid "Search"
msgstr "Tìm kiếm"

#: ../../english/template/debian/common_translation.wml:73
msgid "none"
msgstr "không"

#: ../../english/template/debian/common_translation.wml:76
msgid "Go"
msgstr "Đi"

#: ../../english/template/debian/common_translation.wml:79
msgid "worldwide"
msgstr "toàn cầu"

#: ../../english/template/debian/common_translation.wml:82
msgid "Site map"
msgstr "Sơ đồ trang"

#: ../../english/template/debian/common_translation.wml:85
msgid "Miscellaneous"
msgstr "Lặt vặt"

#: ../../english/template/debian/common_translation.wml:88
#: ../../english/template/debian/links.tags.wml:104
msgid "Getting Debian"
msgstr "Lấy Debian"

#: ../../english/template/debian/common_translation.wml:91
msgid "The Debian Blog"
msgstr "Nhật ký Debian"

#: ../../english/template/debian/common_translation.wml:94
msgid "Debian Micronews"
msgstr "Debian Micronews"

#: ../../english/template/debian/common_translation.wml:97
msgid "Debian Planet"
msgstr "Debian Planet"

#: ../../english/template/debian/common_translation.wml:100
msgid "Last Updated"
msgstr "Cập nhật lần cuối"

#: ../../english/template/debian/ddp.wml:6
msgid ""
"Please send all comments, criticisms and suggestions about these web pages "
"to our <a href=\"mailto:debian-doc@lists.debian.org\">mailing list</a>."
msgstr ""
"Vui lòng gửi tất cả các lời bình luận, chỉ trích và gợi ý liên quan đến "
"trang thông tin điện tử đến <a href=\"mailto:debian-doc@lists.debian.org"
"\">bó thư</a> của chúng tôi."

#: ../../english/template/debian/fixes_link.wml:11
msgid "not needed"
msgstr "không cần thiết"

#: ../../english/template/debian/fixes_link.wml:14
msgid "not available"
msgstr "không sẵn có"

#: ../../english/template/debian/fixes_link.wml:17
msgid "N/A"
msgstr "N/A"

#: ../../english/template/debian/fixes_link.wml:20
msgid "in release 1.1"
msgstr "trong bản phát hành 1.1"

#: ../../english/template/debian/fixes_link.wml:23
msgid "in release 1.3"
msgstr "trong bản phát hành 1.3"

#: ../../english/template/debian/fixes_link.wml:26
msgid "in release 2.0"
msgstr "trong bản phát hành 2.0"

#: ../../english/template/debian/fixes_link.wml:29
msgid "in release 2.1"
msgstr "trong bản phát hành 2.1"

#: ../../english/template/debian/fixes_link.wml:32
msgid "in release 2.2"
msgstr "trong bản phát hành 2.2"

#: ../../english/template/debian/footer.wml:84
msgid ""
"See our <a href=\"m4_HOME/contact\">contact page</a> to get in touch. Web "
"site source code is <a href=\"https://salsa.debian.org/webmaster-team/webwml"
"\">available</a>."
msgstr ""

#: ../../english/template/debian/footer.wml:87
msgid "Last Modified"
msgstr "Lần sửa cuối"

#: ../../english/template/debian/footer.wml:90
msgid "Last Built"
msgstr "Biên dịch lần cuối"

#: ../../english/template/debian/footer.wml:93
msgid "Copyright"
msgstr "Bản quyền"

#: ../../english/template/debian/footer.wml:96
msgid "<a href=\"https://www.spi-inc.org/\">SPI</a> and others;"
msgstr "<a href=\"https://www.spi-inc.org/\">SPI</a> và các chủ thể khác;"

#: ../../english/template/debian/footer.wml:99
msgid "See <a href=\"m4_HOME/license\" rel=\"copyright\">license terms</a>"
msgstr ""
"Xem <a href=\"m4_HOME/license\" rel=\"copyright\">các điều khoản giấy phép</"
"a>"

#: ../../english/template/debian/footer.wml:102
msgid ""
"Debian is a registered <a href=\"m4_HOME/trademark\">trademark</a> of "
"Software in the Public Interest, Inc."
msgstr ""
"Debian là <a href=\"m4_HOME/trademark\">nhãn hiệu</a> đã đăng ký của "
"Software in the Public Interest, Inc."

#: ../../english/template/debian/languages.wml:196
#: ../../english/template/debian/languages.wml:232
msgid "This page is also available in the following languages:"
msgstr "Trang này cũng sẵn có bằng các ngôn ngữ sau:"

#: ../../english/template/debian/languages.wml:265
msgid "How to set <a href=m4_HOME/intro/cn>the default document language</a>"
msgstr "Làm sao để đặt <a href=m4_HOME/intro/cn>ngôn ngữ tài liệu mặc định</a>"

#: ../../english/template/debian/languages.wml:323
msgid "Browser default"
msgstr "Trình duyệt mặc định"

#: ../../english/template/debian/languages.wml:323
msgid "Unset the language override cookie"
msgstr "Bỏ đặt cookie ghi đè ngôn ngữ"

#: ../../english/template/debian/links.tags.wml:4
msgid "Debian International"
msgstr "Debian quốc tế"

#: ../../english/template/debian/links.tags.wml:7
msgid "Partners"
msgstr "Đối tác"

#: ../../english/template/debian/links.tags.wml:10
msgid "Debian Weekly News"
msgstr "Tin tức hàng tuần Debian"

#: ../../english/template/debian/links.tags.wml:13
msgid "Weekly News"
msgstr "Tin tức tuần"

#: ../../english/template/debian/links.tags.wml:16
msgid "Debian Project News"
msgstr "Tin tức về dự án Debian"

#: ../../english/template/debian/links.tags.wml:19
msgid "Project News"
msgstr "Tin tức dự án"

#: ../../english/template/debian/links.tags.wml:22
msgid "Release Info"
msgstr "Thông tin phát hành"

#: ../../english/template/debian/links.tags.wml:25
msgid "Debian Packages"
msgstr "Các gói Debian"

#: ../../english/template/debian/links.tags.wml:28
msgid "Download"
msgstr "Tải về"

#: ../../english/template/debian/links.tags.wml:31
msgid "Debian&nbsp;on&nbsp;CD"
msgstr "Debian&nbsp;trên&nbsp;CD"

#: ../../english/template/debian/links.tags.wml:34
msgid "Debian Books"
msgstr "Sách về Debian"

#: ../../english/template/debian/links.tags.wml:37
msgid "Debian Wiki"
msgstr "Wiki về Debian"

#: ../../english/template/debian/links.tags.wml:40
msgid "Mailing List Archives"
msgstr "Phần lưu trữ bó thư"

#: ../../english/template/debian/links.tags.wml:43
msgid "Mailing Lists"
msgstr "Bó thư"

#: ../../english/template/debian/links.tags.wml:49
msgid "Social Contract"
msgstr "Khế ước xã hội"

#: ../../english/template/debian/links.tags.wml:52
msgid "Code of Conduct"
msgstr "Quy tắc ứng xử"

#: ../../english/template/debian/links.tags.wml:55
msgid "Debian 5.0 - The universal operating system"
msgstr "Debian 5.0 - Hệ điều hành toàn cầu"

#: ../../english/template/debian/links.tags.wml:58
msgid "Site map for Debian web pages"
msgstr "Sơ đồ trang thông tin điện tử Debian"

#: ../../english/template/debian/links.tags.wml:61
msgid "Developer Database"
msgstr "Cơ sở dữ liệu nhà phát triển"

#: ../../english/template/debian/links.tags.wml:64
msgid "Debian FAQ"
msgstr "Debian FAQ"

#: ../../english/template/debian/links.tags.wml:67
msgid "Debian Policy Manual"
msgstr "Hướng dẫn chính sách Debian"

#: ../../english/template/debian/links.tags.wml:70
msgid "Developers' Reference"
msgstr "Tham khảo cho nhà phát triển"

#: ../../english/template/debian/links.tags.wml:73
msgid "New Maintainers' Guide"
msgstr "Hướng dẫn người bảo trì mới"

#: ../../english/template/debian/links.tags.wml:76
msgid "Release Critical Bugs"
msgstr "Các lỗi phát hành nghiêm trọng"

#: ../../english/template/debian/links.tags.wml:79
msgid "Lintian Reports"
msgstr "Báo cáo kiểm chuẩn"

#: ../../english/template/debian/links.tags.wml:83
msgid "Archives for users' mailing lists"
msgstr "Lưu trữ cho các bó thư người dùng"

#: ../../english/template/debian/links.tags.wml:86
msgid "Archives for developers' mailing lists"
msgstr "Lưu trữ cho các bó thư nhà phát triển"

#: ../../english/template/debian/links.tags.wml:89
msgid "Archives for i18n/l10n mailing lists"
msgstr "Lưu trữ cho các bó thư i18n/l10n"

#: ../../english/template/debian/links.tags.wml:92
msgid "Archives for ports' mailing lists"
msgstr "Lưu trữ cho các bó thư chuyển đổi để chạy trên các hệ thống khác"

#: ../../english/template/debian/links.tags.wml:95
msgid "Archives for mailing lists of the Bug tracking system"
msgstr "Lưu trữ cho các bó thư của hệ thống theo dõi lỗi"

#: ../../english/template/debian/links.tags.wml:98
msgid "Archives for miscellaneous mailing lists"
msgstr "Lưu trữ cho các bó thư lặt vặt"

#: ../../english/template/debian/links.tags.wml:101
msgid "Free Software"
msgstr "Phần mềm tự do"

#: ../../english/template/debian/links.tags.wml:107
msgid "Development"
msgstr "Phát triển"

#: ../../english/template/debian/links.tags.wml:110
msgid "Help Debian"
msgstr "Hỗ trợ Debian"

#: ../../english/template/debian/links.tags.wml:113
msgid "Bug reports"
msgstr "Báo cáo lỗi"

#: ../../english/template/debian/links.tags.wml:116
msgid "Ports/Architectures"
msgstr "Bản chuyển/Kiến trúc"

#: ../../english/template/debian/links.tags.wml:119
msgid "Installation manual"
msgstr "Hướng dẫn cài đặt"

#: ../../english/template/debian/links.tags.wml:122
msgid "CD vendors"
msgstr "Nhà cung cấp CD"

#: ../../english/template/debian/links.tags.wml:125
msgid "CD/USB ISO images"
msgstr "Ảnh ISO CD/USB"

#: ../../english/template/debian/links.tags.wml:128
msgid "Network install"
msgstr "Cài đặt qua mạng"

#: ../../english/template/debian/links.tags.wml:131
msgid "Pre-installed"
msgstr "Cài sẵn"

#: ../../english/template/debian/links.tags.wml:134
msgid "Debian-Edu project"
msgstr "Dự án Debian-Edu"

#: ../../english/template/debian/links.tags.wml:137
msgid "Salsa &ndash; Debian Gitlab"
msgstr "Salsa &ndash; Debian Gitlab"

#: ../../english/template/debian/links.tags.wml:140
msgid "Quality Assurance"
msgstr "Đảm bảo chất lượng"

#: ../../english/template/debian/links.tags.wml:143
msgid "Package Tracking System"
msgstr "Hệ thống theo dõi gói"

#: ../../english/template/debian/links.tags.wml:146
msgid "Debian Developer's Packages Overview"
msgstr "Tổng quan các gói của nhà phát triển Debian"

#: ../../english/template/debian/navbar.wml:10
msgid "Debian Home"
msgstr "Trang chủ Debian"

#: ../../english/template/debian/recent_list.wml:7
msgid "No items for this year."
msgstr "Không có mục tin nào cho năm nay."

#: ../../english/template/debian/recent_list.wml:11
msgid "proposed"
msgstr "dự kiến"

#: ../../english/template/debian/recent_list.wml:15
msgid "in discussion"
msgstr "đang tranh cãi"

#: ../../english/template/debian/recent_list.wml:19
msgid "voting open"
msgstr "đang bỏ phiếu"

#: ../../english/template/debian/recent_list.wml:23
msgid "finished"
msgstr "đã hoàn tất"

#: ../../english/template/debian/recent_list.wml:26
msgid "withdrawn"
msgstr "rút lại"

#: ../../english/template/debian/recent_list.wml:30
msgid "Future events"
msgstr "Sự kiện sắp tới"

#: ../../english/template/debian/recent_list.wml:33
msgid "Past events"
msgstr "Sự kiện đã qua"

#: ../../english/template/debian/recent_list.wml:37
msgid "(new revision)"
msgstr "(phiên bản mới)"

#: ../../english/template/debian/recent_list.wml:329
msgid "Report"
msgstr "Báo cáo"

#: ../../english/template/debian/redirect.wml:6
msgid "Page redirected to <newpage/>"
msgstr "Trang được chuyển hướng đến <newpage/>"

#: ../../english/template/debian/redirect.wml:14
msgid ""
"This page has been renamed to <url <newpage/>>, please update your links."
msgstr ""
"Trang đã được đổi tên thành <url <newpage/>>, vui lòng cập nhật các liên kết "
"của bạn."

#. given a manual name and an architecture, join them
#. if you need to reorder the two, use "%2$s ... %1$s", cf. printf(3)
#: ../../english/template/debian/release.wml:7
msgid "<void id=\"doc_for_arch\" />%s for %s"
msgstr "<void id=\"doc_for_arch\" />%s dành cho %s"

#: ../../english/template/debian/translation-check.wml:37
msgid ""
"<em>Note:</em> The <a href=\"$link\">original document</a> is newer than "
"this translation."
msgstr ""
"<em>Chú ý:</em> <a href=\"$link\">Tài liệu gốc</a> mới hơn bản dịch này."

#: ../../english/template/debian/translation-check.wml:43
msgid ""
"Warning! This translation is too out of date, please see the <a href=\"$link"
"\">original</a>."
msgstr ""
"Cảnh báo! Bản dịch này đã lạc hậu, vui lòng xem <a href=\"$link\">bản gốc</"
"a>."

#: ../../english/template/debian/translation-check.wml:49
msgid ""
"<em>Note:</em> The original document of this translation no longer exists."
msgstr "<em>Chú ý:</em> Tài liệu gốc của bản dịch này không còn tồn tại nữa."

#: ../../english/template/debian/translation-check.wml:56
msgid "Wrong translation version!"
msgstr "Sai phiên bản bản dịch!"

#: ../../english/template/debian/url.wml:4
msgid "URL"
msgstr "URL"

#: ../../english/template/debian/users.wml:12
msgid "Back to the <a href=\"../\">Who's using Debian? page</a>."
msgstr "Quay trở về trang <a href=\"../\">Ai đang dùng Debian?</a>."

#~ msgid ""
#~ "To report a problem with the web site, please e-mail our publicly "
#~ "archived mailing list <a href=\"mailto:debian-www@lists.debian.org"
#~ "\">debian-www@lists.debian.org</a> in English.  For other contact "
#~ "information, see the Debian <a href=\"m4_HOME/contact\">contact page</a>. "
#~ "Web site source code is <a href=\"https://salsa.debian.org/webmaster-team/"
#~ "webwml\">available</a>."
#~ msgstr ""
#~ "Để báo cáo các lỗi trong trang thông tin điện tử, gửi thư điện tử bằng "
#~ "tiếng Anh đến bó thư được lưu trữ công khai <a href=\"mailto:debian-"
#~ "www@lists.debian.org\">debian-www@lists.debian.org</a>. Gửi báo cáo lỗi "
#~ "dịch tiếng Việt cho nhóm dịch <a href=\"mailto:debian-l10n-"
#~ "vietnamese@lists.debian.org\">debian-l10n-vietnamese@lists.debian.org</"
#~ "a>. Để biết thêm thông tin liên hệ khác, vui lòng xem <a href=\"m4_HOME/"
#~ "contact\">trang liên hệ</a> Debian. Mã nguồn của trang thông tin điện tử "
#~ "này cũng <a href=\"https://salsa.debian.org/webmaster-team/webwml\">sẵn "
#~ "có</a>."

#~ msgid "Visit the site sponsor"
#~ msgstr "Viếng thăm trang của nhà đỡ đầu"
