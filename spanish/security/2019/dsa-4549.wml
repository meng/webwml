#use wml::debian::translation-check translation="a7762d7ea18afc6d77cb26d372f94053b56575af"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han encontrado múltiples problemas de seguridad en el navegador web Mozilla
Firefox que podrían, potencialmente, dar lugar a ejecución de código
arbitrario, a revelación de información, a ejecución de scripts entre sitios («cross-site scripting») o a denegación de servicio.</p>

<p>Debian sigue las versiones con soporte extendido (ESR, por sus siglas en inglés) de Firefox. El soporte
para la serie 60.x ha terminado, por lo que a partir de esta actualización
seguiremos las versiones 68.x.</p>

<p>Para la distribución «antigua estable» (stretch) son necesarias algunas modificaciones
de configuración adicionales en la red buildd (para proporcionar el nuevo conjunto de herramientas Rust
que necesita ESR68). Los paquetes estarán disponibles cuando estén listas dichas modificaciones.</p>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 68.2.0esr-1~deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de firefox-esr.</p>

<p>Para información detallada sobre el estado de seguridad de firefox-esr, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/firefox-esr">\
https://security-tracker.debian.org/tracker/firefox-esr</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4549.data"
