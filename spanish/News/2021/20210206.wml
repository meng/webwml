#use wml::debian::translation-check translation="156615cc19b61bffcfb93a5ff5e5e300fcbc9492"
<define-tag pagetitle>Debian 10 actualizado: publicada la versión 10.8</define-tag>
<define-tag release_date>2021-02-06</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>buster</define-tag>
<define-tag revision>10.8</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la octava actualización de su
distribución «estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad
junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos medios de instalación de <q><codename></q>. Tras la instalación de Debian,
los paquetes instalados pueden pasarse a las nuevas versiones utilizando una réplica Debian
actualizada.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevas imágenes de instalación en los sitios habituales.</p>

<p>Puede actualizar una instalación existente a esta nueva versión haciendo
que el sistema de gestión de paquetes apunte a una de las muchas réplicas HTTP de Debian.
En la dirección siguiente puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «estable» añade unas pocas correcciones importantes a los paquetes siguientes:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction atftp "Corrige problema de denegación de servicio [CVE-2020-6097]">
<correction base-files "Actualiza /etc/debian_version para la versión 10.8">
<correction ca-certificates "Actualiza el lote de CA de Mozilla a la 2.40, añade a la lista negra el expirado <q>AddTrust External Root</q>">
<correction cacti "Corrige problema de inyección de SQL [CVE-2020-35701] y problema de XSS directo">
<correction cairo "Corrige uso de la máscara («mask») en image-compositor [CVE-2020-35492]">
<correction choose-mirror "Actualiza lista de réplicas">
<correction cjson "Corrige bucle infinito en cJSON_Minify">
<correction clevis "Corrige creación de initramfs; clevis-dracut: desencadena la creación de initramfs en la instalación">
<correction cyrus-imapd "Corrige comparación de la versión en script cron">
<correction debian-edu-config "Mueve el código de limpieza de tablas de claves («keytabs») de máquinas desde gosa-modify-host a un script independiente, reduciendo las llamadas LDAP a una única consulta">
<correction debian-installer "Usa la ABI del núcleo Linux 4.19.0-14; recompilado contra proposed-updates">
<correction debian-installer-netboot-images "Recompilado contra proposed-updates">
<correction debian-installer-utils "Soporta particiones en dispositivos USB UAS">
<correction device-tree-compiler "Corrige violación de acceso en <q>dtc -I fs /proc/device-tree</q>">
<correction didjvu "Añade dependencia en tiempo de compilación con tzdata, que faltaba">
<correction dovecot "Corrige caída al buscar en buzones de correo que contienen mensajes MIME mal construidos">
<correction dpdk "Nueva versión «estable» del proyecto original">
<correction edk2 "CryptoPkg/BaseCryptLib: corrige desreferencia NULL [CVE-2019-14584]">
<correction emacs "No cae con identificativos de usuario OpenPGP que no contienen dirección de correo electrónico">
<correction fcitx "Corrige soporte de método de entrada en Flatpaks">
<correction file "Aumenta la profundidad por omisión de recursión de nombres a 50">
<correction geoclue-2.0 "Comprueba el máximo nivel de precisión permitido incluso en aplicaciones del sistema; hace que la clave de la API de Mozilla sea configurable y usa por omisión una clave específica de Debian; corrige visualización del indicador de uso">
<correction gnutls28 "Corrige error en la colección de pruebas provocado por certificado expirado">
<correction grub2 "Al actualizar grub-pc de forma no interactiva, abandona si falla grub-install; comprueba explícitamente si existe el dispositivo objetivo antes de ejecutar grub-install; grub-install: añade obtención de copia de seguridad y su restauración; no llama a grub-install en instalaciones nuevas de grub-pc">
<correction highlight.js "Corrige contaminación de prototipo [CVE-2020-26237]">
<correction intel-microcode "Actualiza varios microcódigos">
<correction iproute2 "Corrige fallos en salida JSON; corrige condición de carrera que provoca denegación de servicio en el sistema al utilizar ip netns add en el arranque">
<correction irssi-plugin-xmpp "No desencadena prematuramente situaciones de exceso de tiempo («timeout») al conectar al núcleo de irssi, corrigiendo así las conexiones mediante STARTTLS">
<correction libdatetime-timezone-perl "Actualizado para la nueva versión de tzdata">
<correction libdbd-csv-perl "Corrige fallo de prueba con libdbi-perl 1.642-1+deb10u2">
<correction libdbi-perl "Corrección de seguridad [CVE-2014-10402]">
<correction libmaxminddb "Corrige lectura de memoria dinámica («heap») fuera de límites [CVE-2020-28241]">
<correction lttng-modules "Corrige compilación en núcleos con versiones &gt;= 4.19.0-10">
<correction m2crypto "Corrige compatibilidad con OpenSSL 1.1.1i y posteriores">
<correction mini-buildd "builder.py: llamada a sbuild: utiliza '--no-arch-all' explícitamente">
<correction net-snmp "snmpd: añade los indicadores cacheTime y execType a EXTEND-MIB">
<correction node-ini "No permite cadenas de caracteres inválidas y peligrosas como nombres de sección [CVE-2020-7788]">
<correction node-y18n "Corrige problema de contaminación de prototipo [CVE-2020-7774]">
<correction nvidia-graphics-drivers "Nueva versión del proyecto original; corrige posibles denegación de servicio y revelación de información [CVE-2021-1056]">
<correction nvidia-graphics-drivers-legacy-390xx "Nueva versión del proyecto original; corrige posibles denegación de servicio y revelación de información [CVE-2021-1056]">
<correction pdns "Correcciones de seguridad [CVE-2019-10203 CVE-2020-17482]">
<correction pepperflashplugin-nonfree "Se transforma en un paquete ficticio cuidando de eliminar la extensión («plugin») previamente instalada (ya no es funcional ni está soportada)">
<correction pngcheck "Corrige desbordamiento de memoria [CVE-2020-27818]">
<correction postgresql-11 "Nueva versión «estable» del proyecto original; correcciones de seguridad [CVE-2020-25694 CVE-2020-25695 CVE-2020-25696]">
<correction postsrsd "Antes de decodificar una etiqueta de marca temporal («timestamp»), se asegura de que no sea demasiado larga [CVE-2020-35573]">
<correction python-bottle "Deja de aceptar <q>;</q> como separador de consultas [CVE-2020-28473]">
<correction python-certbot "En renovaciones usa automáticamente la API ACMEv2 para evitar problemas con la eliminación de la API ACMEv1">
<correction qxmpp "Corrige potencial violación de acceso en errores de conexión">
<correction silx "python(3)-silx: añade dependencia con python(3)-scipy">
<correction slirp "Corrige desbordamientos de memoria [CVE-2020-7039 CVE-2020-8608]">
<correction steam "Nueva versión del proyecto original">
<correction systemd "journal: no desencadena aserción cuando se le pasa NULL a journal_file_close()">
<correction tang "Evita condición de carrera entre keygen y update">
<correction tzdata "Nueva versión del proyecto original; actualiza los datos de zonas horarias incluidos">
<correction unzip "Aplica más correcciones para CVE-2019-13232">
<correction wireshark "Corrige varias caídas, bucles infinitos y fugas de contenido de la memoria [CVE-2019-16319 CVE-2019-19553 CVE-2020-11647 CVE-2020-13164 CVE-2020-15466 CVE-2020-25862 CVE-2020-25863 CVE-2020-26418 CVE-2020-26421 CVE-2020-26575 CVE-2020-28030 CVE-2020-7045 CVE-2020-9428 CVE-2020-9430 CVE-2020-9431]">
</table>


<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución «estable».
El equipo de seguridad ya ha publicado un aviso para cada una de estas
actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>
<dsa 2020 4797 webkit2gtk>
<dsa 2020 4801 brotli>
<dsa 2020 4802 thunderbird>
<dsa 2020 4803 xorg-server>
<dsa 2020 4804 xen>
<dsa 2020 4805 trafficserver>
<dsa 2020 4806 minidlna>
<dsa 2020 4807 openssl>
<dsa 2020 4808 apt>
<dsa 2020 4809 python-apt>
<dsa 2020 4810 lxml>
<dsa 2020 4811 libxstream-java>
<dsa 2020 4812 xen>
<dsa 2020 4813 firefox-esr>
<dsa 2020 4814 xerces-c>
<dsa 2020 4815 thunderbird>
<dsa 2020 4816 mediawiki>
<dsa 2020 4817 php-pear>
<dsa 2020 4818 sympa>
<dsa 2020 4819 kitty>
<dsa 2020 4820 horizon>
<dsa 2020 4821 roundcube>
<dsa 2021 4822 p11-kit>
<dsa 2021 4823 influxdb>
<dsa 2021 4824 chromium>
<dsa 2021 4825 dovecot>
<dsa 2021 4827 firefox-esr>
<dsa 2021 4828 libxstream-java>
<dsa 2021 4829 coturn>
<dsa 2021 4830 flatpak>
<dsa 2021 4831 ruby-redcarpet>
<dsa 2021 4832 chromium>
<dsa 2021 4833 gst-plugins-bad1.0>
<dsa 2021 4834 vlc>
<dsa 2021 4835 tomcat9>
<dsa 2021 4837 salt>
<dsa 2021 4838 mutt>
<dsa 2021 4839 sudo>
<dsa 2021 4840 firefox-esr>
<dsa 2021 4841 slurm-llnl>
<dsa 2021 4843 linux-latest>
<dsa 2021 4843 linux-signed-amd64>
<dsa 2021 4843 linux-signed-arm64>
<dsa 2021 4843 linux-signed-i386>
<dsa 2021 4843 linux>
</table>


<h2>Paquetes eliminados</h2>

<p>Se han eliminado los paquetes listados a continuación por circunstancias ajenas a nosotros:</p>

<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction compactheader "Incompatible con versiones actuales de Thunderbird">

</table>

<h2>Instalador de Debian</h2>
<p>Se ha actualizado el instalador para incluir las correcciones incorporadas
por esta nueva versión en la distribución «estable».</p>

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta versión:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «estable» actual:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Actualizaciones propuestas a la distribución «estable»:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Información sobre la distribución «estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>

<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>
