#use wml::debian::template title="Debian Med"
#use wml::debian::recent_list
#use wml::debian::translation-check translation="b9967d1f4d930716e9646a92bda776ced3c44cce"
# $Id$

<h2>Descripción del proyecto</h2>

<p>Debian Med es una 
   "<a href="https://blends.debian.org/blends/">Mezcla Pura de Debian</a>"
   desarrollada para implementar Debian en equipos que 
   necesiten una configuración especialmente adecuada para 
   la práctica médica y la investigación biomédica.
   La finalidad de Debian Med es la creación de un sistema de software
   libre para todas las tareas en atención médica y en investigación.
   Para ello, Debian Med integra, aparte de los paquetes generales,
   software de código abierto para tratamiento de imagen médico,
   bioinformática, control de infraestructuras clínicas y similares.
</p>


<p>De esta forma, Debian Med incluye una serie de metapaquetes que
   declararán las dependencias de otros paquetes Debian, quedando el sistema
   preparado para resolver tareas específicas. Para una visión general
   del proyecto Debian Med, puede consultar la 
   <a href="https://blends.debian.org/med/tasks/">página de tareas</a>.
</p>

<p>Para una visión en profundidad, hay disponibles 
   <a href="https://people.debian.org/~tille/talks/">varias
   presentaciones sobre Debian Med y las Mezclas Puras de Debian en
   general</a>, tanto en formato diapositiva como, parcialmente,
   en formato vídeo.
</p>


<h2>Contacto e información para desarrolladores</h2>

<p>
La <a href="mailto:debian-med@lists.debian.org">lista de correo de Debian Med</a> es la central de comunicación para Debian Med. Funciona como un foro tanto para aquellos usuarios que están planteándose utilizar Debian Med como para los que ya lo están usando para la práctica médica. De manera adicional, se utiliza para coordinar los esfuerzos de desarrollo en varios temas de medicina. Puede suscribirse o darse de baja de la lista de distribución desde la
<a href="https://lists.debian.org/debian-med/">página web de la lista</a>,
donde también encontrará la lista de archivos.
</p>
<p>
Algunos sitios de información importantes para desarrolladores son:
</p>
<ol>
  <li><a href="https://wiki.debian.org/DebianMed">La Wiki.</a></li>
  <li><a href="https://blends.debian.org/med/">La página de las mezclas de Debian.</a></li>
  <li>La <a href="https://med-team.pages.debian.net/policy/">política de Debian Med</a> donde se explican las reglas del equipo respecto a los paquetes.</li>
  <li><a href="https://salsa.debian.org/med-team/">Los repositorios Git de paquetes Debian Med en Salsa.</a></li>
</ol>

<h2>Proyectos de software incluidos</h2>

El sistema de <a href="https://blends.debian.org/blends">Mezclas puras de Debian</a> proporciona una visión general generada automáticamente de todo el software incluido en una mezcla.
Para ello, solo es necesario echar un vistazo a las
<b><a href="https://blends.debian.org/med/tasks/">páginas de tareas de Debian Med</a></b> para aprender sobre el software incluido en la mezcla y sobre los proyectos que se encuentran en la lista de tareas pendientes a incluir en Debian.


<h2>Objetivos del proyecto</h2>

<ul>
  <li>La construcción de una base de software sólido para la atención médico-sanitaria con énfasis en la facilidad de instalación y mantenimiento y en la seguridad.</li>
  <li>Animar a la cooperación entre autores de proyectos de creación de software con objetivos similares.</li>
  <li>Servir como banco de pruebas para evaluar la calidad del software médico.</li>
  <li>Proporcionar información y documentación del software médico.</li>
  <li>Ayudar a los autores originales a empaquetar sus productos para Debian.</li>
  <li>Mostrar a compañías de software comercial (con ánimo de lucro) la fortaleza de un sistema sólido que les haga plantearse la posibilidad de portar su software a Linux o incluso cambiar al desarrollo de software libre.</li>
</ul>


<h2>¿Cómo puedo ayudar?</h2>

<p>
   Existe una <a href="https://wiki.debian.org/DebianMedTodo">página en la Wiki con un listado de cosas</a> que se pueden hacer para ayudar al proyecto.
</p>

<h2>Marketing y prensa</h2>

<p>Tan pronto como tengamos algo que mostrar de este proyecto (incluso en las fases iniciales) estaremos expuestos a ojos del mundo. Es por ello por lo que necesitaremos trabajar conjuntamente con press@debian.org para ayudar a dar a Debian en general y al proyecto en particular la visibilidad que queremos. Para ello, se harán una colección de diapositivas para charlas sobre Debian Med.
</p>


<h2>Enlaces</h2>

<ul>
  <li>Debian Med trabaja de manera conjunta con
      <a href="http://nebc.nerc.ac.uk/tools/bio-linux/bio-linux-6.0">Bio-Linux</a>.
      Mientras que Bio-Linux está basado en Ubuntu LTS, los paquetes de enfoque biológico son responsabilidad de Debian a través del equipo de Debian Med.
  </li>
</ul>
