msgid ""
msgstr ""
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../english/intro/organization.data:15
msgid "delegation mail"
msgstr ""

#: ../../english/intro/organization.data:16
msgid "appointment mail"
msgstr ""

#. One male delegate
#: ../../english/intro/organization.data:18
#. Pronoun tags with delegate combinations
#: ../../english/intro/organization.data:22
msgid "<void id=\"male\"/>delegate"
msgstr ""

#. One female delegate
#: ../../english/intro/organization.data:20
#: ../../english/intro/organization.data:23
msgid "<void id=\"female\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:22
#: ../../english/intro/organization.data:25
msgid "<void id=\"he_him\"/>he/him"
msgstr ""

#: ../../english/intro/organization.data:23
#: ../../english/intro/organization.data:26
msgid "<void id=\"she_her\"/>she/her"
msgstr ""

#: ../../english/intro/organization.data:24
msgid "<void id=\"gender_neutral\"/>delegate"
msgstr ""

#: ../../english/intro/organization.data:24
#: ../../english/intro/organization.data:27
msgid "<void id=\"they_them\"/>they/them"
msgstr ""

#: ../../english/intro/organization.data:30
#: ../../english/intro/organization.data:32
msgid "current"
msgstr ""

#: ../../english/intro/organization.data:34
#: ../../english/intro/organization.data:36
msgid "member"
msgstr ""

#: ../../english/intro/organization.data:39
msgid "manager"
msgstr ""

#: ../../english/intro/organization.data:41
msgid "Stable Release Manager"
msgstr ""

#: ../../english/intro/organization.data:41
msgid "SRM"
msgstr ""

#: ../../english/intro/organization.data:43
msgid "wizard"
msgstr ""

#. we only use the chair tag once, for techctte, I wonder why it's here.
#: ../../english/intro/organization.data:45
msgid "chair"
msgstr ""

#: ../../english/intro/organization.data:48
msgid "assistant"
msgstr ""

#: ../../english/intro/organization.data:50
msgid "secretary"
msgstr ""

#: ../../english/intro/organization.data:52
msgid "representative"
msgstr ""

#: ../../english/intro/organization.data:54
msgid "role"
msgstr ""

#: ../../english/intro/organization.data:62
msgid "In the following list, <q>current</q> is used for positions that are\ntransitional (elected or appointed with a certain expiration date)."
msgstr ""

#: ../../english/intro/organization.data:70
#: ../../english/intro/organization.data:82
msgid "Officers"
msgstr ""

#: ../../english/intro/organization.data:71
#: ../../english/intro/organization.data:104
msgid "Distribution"
msgstr ""

#: ../../english/intro/organization.data:72
#: ../../english/intro/organization.data:195
msgid "Communication and Outreach"
msgstr ""

#: ../../english/intro/organization.data:74
#: ../../english/intro/organization.data:198
msgid "Data Protection team"
msgstr ""

#: ../../english/intro/organization.data:75
#: ../../english/intro/organization.data:203
msgid "Publicity team"
msgstr ""

#: ../../english/intro/organization.data:77
#: ../../english/intro/organization.data:270
msgid "Membership in other organizations"
msgstr ""

#: ../../english/intro/organization.data:78
#: ../../english/intro/organization.data:293
msgid "Support and Infrastructure"
msgstr ""

#: ../../english/intro/organization.data:85
msgid "Leader"
msgstr ""

#: ../../english/intro/organization.data:87
msgid "Technical Committee"
msgstr ""

#: ../../english/intro/organization.data:99
msgid "Secretary"
msgstr ""

#: ../../english/intro/organization.data:107
msgid "Development Projects"
msgstr ""

#: ../../english/intro/organization.data:108
msgid "FTP Archives"
msgstr ""

#: ../../english/intro/organization.data:110
msgid "FTP Masters"
msgstr ""

#: ../../english/intro/organization.data:116
msgid "FTP Assistants"
msgstr ""

#: ../../english/intro/organization.data:122
msgid "FTP Wizards"
msgstr ""

#: ../../english/intro/organization.data:125
msgid "Backports"
msgstr ""

#: ../../english/intro/organization.data:127
msgid "Backports Team"
msgstr ""

#: ../../english/intro/organization.data:131
msgid "Release Management"
msgstr ""

#: ../../english/intro/organization.data:133
msgid "Release Team"
msgstr ""

#: ../../english/intro/organization.data:143
msgid "Quality Assurance"
msgstr ""

#: ../../english/intro/organization.data:144
msgid "Installation System Team"
msgstr ""

#: ../../english/intro/organization.data:145
msgid "Debian Live Team"
msgstr ""

#: ../../english/intro/organization.data:146
msgid "CD/DVD/USB Images"
msgstr ""

#: ../../english/intro/organization.data:148
msgid "Production"
msgstr ""

#: ../../english/intro/organization.data:156
msgid "Testing"
msgstr ""

#: ../../english/intro/organization.data:158
msgid "Cloud Team"
msgstr ""

#: ../../english/intro/organization.data:160
msgid "Autobuilding infrastructure"
msgstr ""

#: ../../english/intro/organization.data:162
msgid "Wanna-build team"
msgstr ""

#: ../../english/intro/organization.data:169
msgid "Buildd administration"
msgstr ""

#: ../../english/intro/organization.data:186
msgid "Documentation"
msgstr ""

#: ../../english/intro/organization.data:190
msgid "Work-Needing and Prospective Packages list"
msgstr ""

#: ../../english/intro/organization.data:206
msgid "Press Contact"
msgstr ""

#: ../../english/intro/organization.data:208
msgid "Web Pages"
msgstr ""

#: ../../english/intro/organization.data:216
msgid "Planet Debian"
msgstr ""

#: ../../english/intro/organization.data:221
msgid "Outreach"
msgstr ""

#: ../../english/intro/organization.data:226
msgid "Debian Women Project"
msgstr ""

#: ../../english/intro/organization.data:234
msgid "Community"
msgstr ""

#: ../../english/intro/organization.data:241
msgid "To send a private message to all the members of the Community Team, use the GPG key <a href=\"community-team-pubkey.txt\">817DAE61E2FE4CA28E1B7762A89C4D0527C4C869</a>."
msgstr ""

#: ../../english/intro/organization.data:243
msgid "Events"
msgstr ""

#: ../../english/intro/organization.data:250
msgid "DebConf Committee"
msgstr ""

#: ../../english/intro/organization.data:257
msgid "Partner Program"
msgstr ""

#: ../../english/intro/organization.data:261
msgid "Hardware Donations Coordination"
msgstr ""

#: ../../english/intro/organization.data:276
msgid "GNOME Foundation"
msgstr ""

#: ../../english/intro/organization.data:278
msgid "Linux Professional Institute"
msgstr ""

#: ../../english/intro/organization.data:279
msgid "Linux Magazine"
msgstr ""

#: ../../english/intro/organization.data:281
msgid "Linux Standards Base"
msgstr ""

#: ../../english/intro/organization.data:282
msgid "Free Standards Group"
msgstr ""

#: ../../english/intro/organization.data:283
msgid "OASIS: Organization\n      for the Advancement of Structured Information Standards"
msgstr ""

#: ../../english/intro/organization.data:286
msgid "OVAL: Open Vulnerability\n      Assessment Language"
msgstr ""

#: ../../english/intro/organization.data:289
msgid "Open Source Initiative"
msgstr ""

#: ../../english/intro/organization.data:296
msgid "Bug Tracking System"
msgstr ""

#: ../../english/intro/organization.data:301
msgid "Mailing Lists Administration and Mailing List Archives"
msgstr ""

#: ../../english/intro/organization.data:309
msgid "New Members Front Desk"
msgstr ""

#: ../../english/intro/organization.data:318
msgid "Debian Account Managers"
msgstr ""

#: ../../english/intro/organization.data:324
msgid "To send a private message to all DAMs, use the GPG key 57731224A9762EA155AB2A530CA8D15BB24D96F2."
msgstr ""

#: ../../english/intro/organization.data:325
msgid "Keyring Maintainers (PGP and GPG)"
msgstr ""

#: ../../english/intro/organization.data:329
msgid "Security Team"
msgstr ""

#: ../../english/intro/organization.data:341
msgid "Policy"
msgstr ""

#: ../../english/intro/organization.data:344
msgid "System Administration"
msgstr ""

#: ../../english/intro/organization.data:345
msgid "This is the address to use when encountering problems on one of Debian's machines, including password problems or you need a package installed."
msgstr ""

#: ../../english/intro/organization.data:355
msgid "If you have hardware problems with Debian machines, please see <a href=\"https://db.debian.org/machines.cgi\">Debian Machines</a> page, it should contain per-machine administrator information."
msgstr ""

#: ../../english/intro/organization.data:356
msgid "LDAP Developer Directory Administrator"
msgstr ""

#: ../../english/intro/organization.data:357
msgid "Mirrors"
msgstr ""

#: ../../english/intro/organization.data:360
msgid "DNS Maintainer"
msgstr ""

#: ../../english/intro/organization.data:361
msgid "Package Tracking System"
msgstr ""

#: ../../english/intro/organization.data:363
msgid "Treasurer"
msgstr ""

#: ../../english/intro/organization.data:368
msgid "<a name=\"trademark\" href=\"m4_HOME/trademark\">Trademark</a> use requests"
msgstr ""

#: ../../english/intro/organization.data:371
msgid "Salsa administrators"
msgstr ""

