<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that xerces-c, a validating XML parser library for
C++, did not correctly scan DTDs. The use-after-free vulnerability
resulting from this issue would allow a remote attacker to leverage a
specially crafted XML file in order to crash the application or
potentially execute arbitrary code.
Please note that the patch fixing this issue comes at the expense of a
newly introduced memory leak.</p>

<p>For the stable distribution (buster), this problem has been fixed in
version 3.2.2+debian-1+deb10u1.</p>

<p>We recommend that you upgrade your xerces-c packages.</p>

<p>For the detailed security status of xerces-c please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/xerces-c">\
https://security-tracker.debian.org/tracker/xerces-c</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4814.data"
# $Id: $
