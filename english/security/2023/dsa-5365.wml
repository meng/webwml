<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Patrick Monnerat discovered that Curl's support for <q>chained</q> HTTP
compression algorithms was susceptible to denial of service.</p>

<p>For the stable distribution (bullseye), this problem has been fixed in
version 7.74.0-1.3+deb11u7. This update also fixes a regression in
the previously released fix for
<a href="https://security-tracker.debian.org/tracker/CVE-2022-27774">CVE-2022-27774</a>.</p>

<p>We recommend that you upgrade your curl packages.</p>

<p>For the detailed security status of curl please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/curl">\
https://security-tracker.debian.org/tracker/curl</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5365.data"
# $Id: $
