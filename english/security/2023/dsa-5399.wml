<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in odoo, a suite of web based
open source business apps.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44775">CVE-2021-44775</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2021-26947">CVE-2021-26947</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2021-45071">CVE-2021-45071</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2021-26263">CVE-2021-26263</a>

  <p>XSS allowing remote attacker to inject arbitrary commands.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45111">CVE-2021-45111</a>

  <p>Incorrect access control allowing authenticated remote user to
  create user accounts and access restricted data.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-44476">CVE-2021-44476</a>,
<a href="https://security-tracker.debian.org/tracker/CVE-2021-23166">CVE-2021-23166</a>

  <p>Incorrect access control allowing authenticated remote administrator
  to access local files on the server.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23186">CVE-2021-23186</a>

  <p>Incorrect access control allowing authenticated remote administrator
  to modify database contents of other tenants.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23178">CVE-2021-23178</a>

  <p>Incorrect access control allowing authenticated remote user to
  use another user's payment method.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23176">CVE-2021-23176</a>

  <p>Incorrect access control allowing authenticated remote user to
  access accounting information.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-23203">CVE-2021-23203</a>

  <p>Incorrect access control allowing authenticated remote user to
  access arbitrary documents via PDF exports.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 14.0.0+dfsg.2-7+deb11u1.</p>

<p>We recommend that you upgrade your odoo packages.</p>

<p>For the detailed security status of odoo please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/odoo">\
https://security-tracker.debian.org/tracker/odoo</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2023/dsa-5399.data"
# $Id: $
