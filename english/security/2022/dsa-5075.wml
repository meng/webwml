<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in Minetest, a sandbox video game
and game creation system. These issues may allow attackers to manipulate game
mods and grant them an unfair advantage over other players. These flaws could
also be abused for a denial of service attack against a Minetest server or if
user input is passed directly to minetest.deserialize without serializing it
first, then a malicious user could run Lua code in the server environment.</p>

<p>For the oldstable distribution (buster), these problems have been fixed
in version 0.4.17.1+repack-1+deb10u1.</p>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 5.3.0+repack-2.1+deb11u1.</p>

<p>We recommend that you upgrade your minetest packages.</p>

<p>For the detailed security status of minetest please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/minetest">\
https://security-tracker.debian.org/tracker/minetest</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5075.data"
# $Id: $
