<define-tag description>security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities have been discovered in the Linux kernel that may
lead to privilege escalation, denial of service or information leaks:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-33655">CVE-2021-33655</a>

    <p>A user with access to a framebuffer console driver could cause a
    memory out-of-bounds write via the FBIOPUT_VSCREENINFO ioctl.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-2318">CVE-2022-2318</a>

    <p>A use-after-free in the Amateur Radio X.25 PLP (Rose) support may
    result in denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-26365">CVE-2022-26365</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-33740">CVE-2022-33740</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-33741">CVE-2022-33741</a>,
    <a href="https://security-tracker.debian.org/tracker/CVE-2022-33742">CVE-2022-33742</a>

    <p>Roger Pau Monne discovered that Xen block and network PV device
    frontends don't zero out memory regions before sharing them with the
    backend, which may result in information disclosure. Additionally it
    was discovered that the granularity of the grant table doesn't permit
    sharing less than a 4k page, which may also result in information
    disclosure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-33743">CVE-2022-33743</a>

    <p>Jan Beulich discovered that incorrect memory handling in the Xen
    network backend may lead to denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-33744">CVE-2022-33744</a>

    <p>Oleksandr Tyshchenko discovered that ARM Xen guests can cause a denial
    of service to the Dom0 via paravirtual devices.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-34918">CVE-2022-34918</a>

    <p>Arthur Mongodin discovered a heap buffer overflow in the Netfilter
    subsystem which may result in local privilege escalation.</p></li>

</ul>

<p>For the stable distribution (bullseye), these problems have been fixed in
version 5.10.127-2.</p>

<p>We recommend that you upgrade your linux packages.</p>

<p>For the detailed security status of linux please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/linux">\
https://security-tracker.debian.org/tracker/linux</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2022/dsa-5191.data"
# $Id: $
