<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were discovered in qemu-kvm, a full
virtualization solution for Linux hosts on x86 hardware with x86 guests.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3710">CVE-2016-3710</a>

    <p>Wei Xiao and Qinghao Tang of 360.cn Inc discovered an out-of-bounds
    read and write flaw in the QEMU VGA module. A privileged guest user
    could use this flaw to execute arbitrary code on the host with the
    privileges of the hosting QEMU process.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-3712">CVE-2016-3712</a>

    <p>Zuozhi Fzz of Alibaba Inc discovered potential integer overflow
    or out-of-bounds read access issues in the QEMU VGA module. A
    privileged guest user could use this flaw to mount a denial of
    service (QEMU process crash).</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.1.2+dfsg-6+deb7u13.</p>

<p>We recommend that you upgrade your qemu-kvm packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-539.data"
# $Id: $
