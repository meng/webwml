<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Franziskus Kiefer reported that the existing mitigations for
some timing side-channel attacks were insufficient:
<a href="https://www.mozilla.org/en-US/security/advisories/mfsa2016-90/#">https://www.mozilla.org/en-US/security/advisories/mfsa2016-90/#</a><a href="https://security-tracker.debian.org/tracker/CVE-2016-9074">CVE-2016-9074</a></p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
2:3.26-1+debu7u2.</p>

<p>We recommend that you upgrade your nss packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-759.data"
# $Id: $
