<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several cross-site scripting vulnerabilities were discovered in moin, a
Python clone of WikiWiki. A remote attacker can conduct cross-site
scripting attacks via the GUI editor's attachment dialogue
(<a href="https://security-tracker.debian.org/tracker/CVE-2016-7146">CVE-2016-7146</a>) 
and the GUI editor's link dialogue (<a href="https://security-tracker.debian.org/tracker/CVE-2016-9119">CVE-2016-9119</a>).</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
1.9.4-8+deb7u3.</p>

<p>We recommend that you upgrade your moin packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-717.data"
# $Id: $
