<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>At several places in the code a wrong length of ACSE data structures
received over the network can cause overflows or underflows when
processing those data structures. Related checks have been added at
various places in order to prevent such (possible) attacks. Thanks to
Kevin Basista for the report.</p>

<p>The bug will indeed affect all DCMTK-based server applications that
accept incoming DICOM network connections that are using the
dcmtk-3.6.0 and earlier versions.</p>

<p>(From: <a href="http://zeroscience.mk/en/vulnerabilities/ZSL-2016-5384.php)">http://zeroscience.mk/en/vulnerabilities/ZSL-2016-5384.php)</a></p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
3.6.0-12+deb7u1.</p>

<p>We recommend that you upgrade your dcmtk packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-755.data"
# $Id: $
