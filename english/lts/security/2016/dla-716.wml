<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple memory corruption issues have been identified in libtiff
and its associated tools.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9273">CVE-2016-9273</a>

    <p>Heap buffer overflow in cpStrips().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9297">CVE-2016-9297</a>

    <p>Read outside buffer in _TIFFPrintField().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-9532">CVE-2016-9532</a>

    <p>Heap buffer overflow via writeBufferToSeparateStrips().</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.0.2-6+deb7u8.</p>

<p>We recommend that you upgrade your tiff packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-716.data"
# $Id: $
