<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>An issue has been found in libxml2, the GNOME XML library.
This issue is called "Parameter Laughs"-attack and is related to parameter
entities expansion.
It is similar to the "Billion Laughs"-attacks found earlier in libexpat.
More information can be found at [1]</p>

<p>[1] <a href="https://blog.hartwork.org/posts/cve-2021-3541-parameter-laughs-fixed-in-libxml2-2-9-11/">https://blog.hartwork.org/posts/cve-2021-3541-parameter-laughs-fixed-in-libxml2-2-9-11/</a></p>


<p>For Debian 9 stretch, this problem has been fixed in version
2.9.4+dfsg1-2.2+deb9u5.</p>

<p>We recommend that you upgrade your libxml2 packages.</p>

<p>For the detailed security status of libxml2 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libxml2">https://security-tracker.debian.org/tracker/libxml2</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2669.data"
# $Id: $
