<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was an overflow issue in runc, the runtime for
the Open Container Project, often used with Docker.</p>

<p>The Netlink 'bytemsg' length field could have allowed an attacker to
override Netlink-based container configurations. This vulnerability required
the attacker to have some control over the configuration of the container, but
would have allowed the attacker to bypass the namespace restrictions of the
container by simply adding their own Netlink payload which disables all
namespaces.</p>

<ul>
<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-43784">CVE-2021-43784</a></li>
</ul>

<p>For Debian 9 <q>Stretch</q>, these problems have been fixed in version
0.1.1+dfsg1-2+deb9u3.</p>

<p>We recommend that you upgrade your runc packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2841.data"
# $Id: $
