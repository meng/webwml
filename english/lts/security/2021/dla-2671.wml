<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>rxvt-unicode allow (potentially remote) code execution because of
improper handling of certain escape sequences (ESC G Q). A response is
terminated by a newline.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
9.22-1+deb9u1.</p>

<p>We recommend that you upgrade your rxvt-unicode packages.</p>

<p>For the detailed security status of rxvt-unicode please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/rxvt-unicode">https://security-tracker.debian.org/tracker/rxvt-unicode</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2671.data"
# $Id: $
