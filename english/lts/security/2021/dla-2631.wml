<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities were discovered in Zabbix, a network
monitoring solution. An attacker may enumerate valid users and
redirect to external links through the zabbix web frontend.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15132">CVE-2019-15132</a>

    <p>Zabbix allows User Enumeration. With login requests, it is
    possible to enumerate application usernames based on the
    variability of server responses (e.g., the <q>Login name or password
    is incorrect</q> and <q>No permissions for system access</q> messages, or
    just blocking for a number of seconds). This affects both
    api_jsonrpc.php and index.php.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15803">CVE-2020-15803</a>

    <p>Zabbix allows stored XSS in the URL Widget. This fix was
    mistakenly dropped in previous upload 1:3.0.31+dfsg-0+deb9u1.</p></li>

</ul>

<p>This update also includes several other bug fixes and
improvements. For more information please refer to the upstream
changelog file.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
1:3.0.32+dfsg-0+deb9u1.</p>

<p>We recommend that you upgrade your zabbix packages.</p>

<p>For the detailed security status of zabbix please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/zabbix">https://security-tracker.debian.org/tracker/zabbix</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2631.data"
# $Id: $
