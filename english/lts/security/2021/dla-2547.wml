<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were fixed in Wireshark, a network sniffer.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13619">CVE-2019-13619</a>

    <p>ASN.1 BER and related dissectors crash</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-16319">CVE-2019-16319</a>

    <p>The Gryphon dissector could go into an infinite loop.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-19553">CVE-2019-19553</a>

    <p>The CMS dissector could crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-7045">CVE-2020-7045</a>

    <p>The BT ATT dissector could crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9428">CVE-2020-9428</a>

    <p>The EAP dissector could crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9430">CVE-2020-9430</a>

    <p>The WiMax DLMAP dissector could crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-9431">CVE-2020-9431</a>

    <p>The LTE RRC dissector could leak memory.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-11647">CVE-2020-11647</a>

    <p>The BACapp dissector could crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-13164">CVE-2020-13164</a>

    <p>The NFS dissector could crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15466">CVE-2020-15466</a>

    <p>The GVCP dissector could go into an infinite loop.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25862">CVE-2020-25862</a>

    <p>The TCP dissector could crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-25863">CVE-2020-25863</a>

    <p>The MIME Multipart dissector could crash.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26418">CVE-2020-26418</a>

    <p>Memory leak in the Kafka protocol dissector.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26421">CVE-2020-26421</a>

    <p>Crash in USB HID protocol dissector.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-26575">CVE-2020-26575</a>

    <p>The Facebook Zero Protocol (aka FBZERO) dissector could enter an infinite loop.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-28030">CVE-2020-28030</a>

    <p>The GQUIC dissector could crash.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
2.6.20-0+deb9u1.</p>

<p>We recommend that you upgrade your wireshark packages.</p>

<p>For the detailed security status of wireshark please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/wireshark">https://security-tracker.debian.org/tracker/wireshark</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2547.data"
# $Id: $
