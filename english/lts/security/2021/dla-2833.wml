<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>In rsync, a remote file-copying tool, remote attackers were able to
bypass the argument-sanitization protection mechanism by passing
additional --protect-args.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
3.1.2-1+deb9u3.</p>

<p>We recommend that you upgrade your rsync packages.</p>

<p>For the detailed security status of rsync please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/rsync">https://security-tracker.debian.org/tracker/rsync</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2833.data"
# $Id: $
