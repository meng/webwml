<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two issues have been found in bluez, a package with Bluetooth tools and
daemons. One issue is about a man-in-the-middle attack during secure
pairing, the  other is about information disclosure due to improper access
control.</p>

<p>In order to completely fix both issues, you need an updated kernel as
well! For Debian 9 Stretch this has been uploaded some days ago.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
5.43-2+deb9u4.</p>

<p>We recommend that you upgrade your bluez packages.</p>

<p>For the detailed security status of bluez please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/bluez">https://security-tracker.debian.org/tracker/bluez</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2692.data"
# $Id: $
