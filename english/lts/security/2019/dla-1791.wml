<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been found in faad2, the Freeware Advanced Audio
Coder:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20194">CVE-2018-20194</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-20197">CVE-2018-20197</a>

    <p>Improper handling of implicit channel mapping reconfiguration leads to
    multiple heap based buffer overflow issues. These flaws might be leveraged
    by remote attackers to cause DoS.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20198">CVE-2018-20198</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2018-20362">CVE-2018-20362</a>

    <p>Insufficient user input validation in the sbr_hfadj module leads to
    stack-based buffer underflow issues. These flaws might be leveraged by
    remote attackers to cause DoS or any other unspecified impact.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.7-8+deb8u2.</p>

<p>We recommend that you upgrade your faad2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1791.data"
# $Id: $
