<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several security vulnerabilities were fixed in opensc, a set of
libraries and utilities to access smart cards that support
cryptographic operations.</p>

<p>Out-of-bounds reads, buffer overflows and double frees could be used
by attackers able to supply crafted smart cards to cause a denial of
service (application crash) or possibly have unspecified other impact.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
0.16.0-3+deb8u1.</p>

<p>We recommend that you upgrade your opensc packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1916.data"
# $Id: $
