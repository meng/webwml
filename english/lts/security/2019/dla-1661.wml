<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It has been found that the mumble-server mishandles multiple
concurrent requests that are persisted in the database, which allows
remote attackers to cause a denial of service (daemon hang or crash)
via a message flood. With the new security update a rate limiter is
added with Leaky-Bucket algorithm.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.2.8-2+deb8u1.</p>

<p>We recommend that you upgrade your mumble packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1661.data"
# $Id: $
