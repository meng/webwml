<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>A change introduced in libssh 0.6.3-4+deb8u4 (which got released as DLA
2038-1) has broken x2goclient's way of scp'ing session setup files from
client to server, resulting in an error message shown in a GUI error
dialog box during session startup (and session resuming).</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in x2goclient version
4.0.3.1-4+deb8u1.</p>

<p>We recommend that you upgrade your x2goclient packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-2038-2.data"
# $Id: $
