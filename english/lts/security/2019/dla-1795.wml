<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in graphicsmagick, the image
processing toolkit:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11473">CVE-2019-11473</a>

    <p>The WriteMATLABImage function (coders/mat.c) is affected by a heap-based
    buffer overflow. Remote attackers might leverage this vulnerability to
    cause denial of service or any other unspecified impact via crafted Matlab
    matrices.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11474">CVE-2019-11474</a>

    <p>The WritePDBImage function (coders/pdb.c) is affected by a heap-based
    buffer overflow. Remote attackers might leverage this vulnerability to
    cause denial of service or any other unspecified impact via a crafted Palm
    Database file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11505">CVE-2019-11505</a>, <a href="https://security-tracker.debian.org/tracker/CVE-2019-11506">CVE-2019-11506</a>

    <p>The XWD module (coders/xwd.c) is affected by multiple heap-based
    buffer overflows and arithmetic exceptions. Remote attackers might leverage
    these various flaws to cause denial of service or any other unspecified
    impact via crafted XWD files.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
1.3.20-3+deb8u7.</p>

<p>We recommend that you upgrade your graphicsmagick packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1795.data"
# $Id: $
