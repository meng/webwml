<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>A security issue was discovered in the poppler PDF rendering shared
library.</p>

<p>The Poppler shared library had a heap-based buffer over-read in the
CairoRescaleBox.cc downsample_row_box_filter function.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
0.26.5-2+deb8u9.</p>

<p>We recommend that you upgrade your poppler packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1752.data"
# $Id: $
