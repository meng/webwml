<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several security issues have been corrected in multiple demuxers and
decoders of the libav multimedia library.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-9987">CVE-2017-9987</a>

    <p>In Libav, there was a heap-based buffer overflow in the function
    hpel_motion in mpegvideo_motion.c. A crafted input could have lead to
    a remote denial of service attack.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-5766">CVE-2018-5766</a>

    <p>In Libav there was an invalid memcpy in the av_packet_ref function of
    libavcodec/avpacket.c. Remote attackers could have leveraged this
    vulnerability to cause a denial of service (segmentation fault) via a
    crafted avi file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-11102">CVE-2018-11102</a>

    <p>A read access violation in the mov_probe function in
    libavformat/mov.c allowed remote attackers to cause a denial of
    service (application crash), as demonstrated by avconv.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14372">CVE-2019-14372</a>

    <p>In Libav, there was an infinite loop in the function
    wv_read_block_header() in the file wvdec.c.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14442">CVE-2019-14442</a>

    <p>In mpc8_read_header in libavformat/mpc8.c, an input file could have
    resulted in an avio_seek infinite loop and hang, with 100% CPU
    consumption. Attackers could have leveraged this vulnerability to
    cause a denial of service via a crafted file.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
6:11.12-1~deb8u8.</p>

<p>We recommend that you upgrade your libav packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1907.data"
# $Id: $
