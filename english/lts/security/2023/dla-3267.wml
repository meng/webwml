<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>XStream serializes Java objects to XML and back again. Versions prior to
1.4.11.1-1+deb10u4 may allow a remote attacker to terminate the application
with a stack overflow error, resulting in a denial of service only via
manipulation of the processed input stream. The attack uses the hash code
implementation for collections and maps to force recursive hash calculation
causing a stack overflow. This update handles the stack overflow and raises an
InputManipulationException instead.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.4.11.1-1+deb10u4.</p>

<p>We recommend that you upgrade your libxstream-java packages.</p>

<p>For the detailed security status of libxstream-java please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/libxstream-java">https://security-tracker.debian.org/tracker/libxstream-java</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3267.data"
# $Id: $
