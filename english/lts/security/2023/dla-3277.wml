<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Powerline Gitstatus, a status line plugin for the VIM editor, allows
arbitrary code execution. Git repositories can contain per-repository
configuration that changes the behavior of git, including running arbitrary
commands. When using powerline-gitstatus, changing to a directory
automatically runs git commands in order to display information about the
current repository in the prompt. If an attacker can convince a user to
change their current directory to one controlled by the attacker, such as
in a shared filesystem or extracted archive, powerline-gitstatus will run
arbitrary commands under the attacker's control.</p>

<p>For Debian 10 buster, this problem has been fixed in version
1.3.2-0+deb10u1.</p>

<p>We recommend that you upgrade your powerline-gitstatus packages.</p>

<p>For the detailed security status of powerline-gitstatus please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/powerline-gitstatus">https://security-tracker.debian.org/tracker/powerline-gitstatus</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3277.data"
# $Id: $
