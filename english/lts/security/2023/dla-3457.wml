<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>MaraDNS is a small and lightweight cross-platform open-source DNS server.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-30256">CVE-2022-30256</a>

    <p>A revoked domain name (so called <q>Ghost</q> domain name) can still be
    resolvable for a long time by staying in the cache longer than
    max_ttl allows. <q>Ghost</q> domain names includes expired domains
    and taken-down malicious domains.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-31137">CVE-2023-31137</a>

    <p>The authoritative server in MaraDNS had an issue where it is
    possible to remotely terminate the MaraDNS process with a
    specialy crafted packet (so called <q>packet of death</q>).</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2.0.13-1.2+deb10u1.</p>

<p>We recommend that you upgrade your maradns packages.</p>

<p>For the detailed security status of maradns please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/maradns">https://security-tracker.debian.org/tracker/maradns</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3457.data"
# $Id: $
