<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple issues were found in modsecurity-crs, a set of generic attack
detection rules for use with ModSecurity or compatible web application
firewalls, which allows remote attackers to bypass the web applications
firewall.</p>

<p>If you are using modsecurity-crs with apache2 / libapache2-modsecurity, please
make sure to review your modsecurity configuration, usually
/etc/modsecurity/modsecurity.conf, against the updated recommended
configration, available in /etc/modsecurity/modsecurity.conf-recommended:
Some of the changes to the recommended rules are required to avoid WAF bypasses
in certain circumstances.</p>

<p>Please note that <a href="https://security-tracker.debian.org/tracker/CVE-2022-39956">CVE-2022-39956</a> requires an updated modsecurity-apache packge,
which has been previously uploaded to buster-security, see Debian LTS Advisory
DLA-3283-1 for details.</p>

<p>If you are using some other solution in connection with the
modsecurity-ruleset, for example one that it is using libmodsecurity3, your
solution might error out with an error message like "Error creating rule:
Unknown variable: MULTIPART_PART_HEADERS". In this case you can disable the
mitigation for <a href="https://security-tracker.debian.org/tracker/CVE-2022-29956">CVE-2022-29956</a> by removing the rule file
REQUEST-922-MULTIPART-ATTACK.conf.  However, be aware that this will disable
the protection and could allow attackers to bypass your Web Application
Firewall.</p>

<p>There is no package in Debian which depends on libmodsecurity3, so if you are
only using software which is available from Debian, you are not affected by
this limitation.</p>

<p>Kudos to @airween for the support and help while perparing the update.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16384">CVE-2018-16384</a>

    <p>A SQL injection bypass (aka PL1 bypass) exists in OWASP ModSecurity Core Rule
    Set (owasp-modsecurity-crs) through v3.1.0-rc3 via {`a`b} where a is a special
    function name (such as <q>if</q>) and b is the SQL statement to be executed.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-22669">CVE-2020-22669</a>

    <p>Modsecurity owasp-modsecurity-crs 3.2.0 (Paranoia level at PL1) has a SQL
    injection bypass vulnerability. Attackers can use the comment characters and
    variable assignments in the SQL syntax to bypass Modsecurity WAF protection and
    implement SQL injection attacks on Web applications.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39955">CVE-2022-39955</a>

    <p>The OWASP ModSecurity Core Rule Set (CRS) is affected by a partial rule set
    bypass by submitting a specially crafted HTTP Content-Type header field that
    indicates multiple character encoding schemes. A vulnerable back-end can
    potentially be exploited by declaring multiple Content-Type <q>charset</q> names and
    therefore bypassing the configurable CRS Content-Type header <q>charset</q> allow
    list. An encoded payload can bypass CRS detection this way and may then be
    decoded by the backend. The legacy CRS versions 3.0.x and 3.1.x are affected,
    as well as the currently supported versions 3.2.1 and 3.3.2. Integrators and
    users are advised to upgrade to 3.2.2 and 3.3.3 respectively.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39956">CVE-2022-39956</a>

    <p>The OWASP ModSecurity Core Rule Set (CRS) is affected by a partial rule set
    bypass for HTTP multipart requests by submitting a payload that uses a
    character encoding scheme via the Content-Type or the deprecated
    Content-Transfer-Encoding multipart MIME header fields that will not be decoded
    and inspected by the web application firewall engine and the rule set. The
    multipart payload will therefore bypass detection. A vulnerable backend that
    supports these encoding schemes can potentially be exploited. The legacy CRS
    versions 3.0.x and 3.1.x are affected, as well as the currently supported
    versions 3.2.1 and 3.3.2. Integrators and users are advised upgrade to 3.2.2
    and 3.3.3 respectively. The mitigation against these vulnerabilities depends on
    the installation of the latest ModSecurity version (v2.9.6 / v3.0.8).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39957">CVE-2022-39957</a>

    <p>The OWASP ModSecurity Core Rule Set (CRS) is affected by a response body
    bypass. A client can issue an HTTP Accept header field containing an optional
    <q>charset</q> parameter in order to receive the response in an encoded form.
    Depending on the <q>charset</q>, this response can not be decoded by the web
    application firewall. A restricted resource, access to which would ordinarily
    be detected, may therefore bypass detection. The legacy CRS versions 3.0.x and
    3.1.x are affected, as well as the currently supported versions 3.2.1 and
    3.3.2. Integrators and users are advised to upgrade to 3.2.2 and 3.3.3
    respectively.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39958">CVE-2022-39958</a>

    <p>The OWASP ModSecurity Core Rule Set (CRS) is affected by a response body bypass
    to sequentially exfiltrate small and undetectable sections of data by
    repeatedly submitting an HTTP Range header field with a small byte range. A
    restricted resource, access to which would ordinarily be detected, may be
    exfiltrated from the backend, despite being protected by a web application
    firewall that uses CRS. Short subsections of a restricted resource may bypass
    pattern matching techniques and allow undetected access. The legacy CRS
    versions 3.0.x and 3.1.x are affected, as well as the currently supported
    versions 3.2.1 and 3.3.2. Integrators and users are advised to upgrade to 3.2.2
    and 3.3.3 respectively and to configure a CRS paranoia level of 3 or higher.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
3.2.3-0+deb10u3.</p>

<p>We recommend that you upgrade your modsecurity-crs packages.</p>

<p>For the detailed security status of modsecurity-crs please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/modsecurity-crs">https://security-tracker.debian.org/tracker/modsecurity-crs</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3293.data"
# $Id: $
