<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>David Marchand discovered that Open vSwitch, a multilayer, software-based,
Ethernet virtual switch, was vulnerable to crafted IP packets with ip proto
set to 0, potentially causing a denial of service.</p>

<p>Triggering the vulnerability requires an attacker to send a crafted
IP packet with protocol field set to <code>0</code> and the flow rules
to contain <code>set</code> actions on other fields in the IP protocol
header.  The resulting flows will omit required actions, and fail to
mask the IP protocol field, resulting in a large bucket which captures
all IP packets.</p>

<p>For Debian 10 buster, this problem has been fixed in version
2.10.7+ds1-0+deb10u4.</p>

<p>We recommend that you upgrade your openvswitch packages.</p>

<p>For the detailed security status of openvswitch please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/openvswitch">https://security-tracker.debian.org/tracker/openvswitch</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3410.data"
# $Id: $
