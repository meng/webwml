<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were fixed in the network traffic analyzer Wireshark.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1161">CVE-2023-1161</a>

    <p>ISO 15765 dissector crash</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1992">CVE-2023-1992</a>

    <p>RPCoRDMA dissector crash</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1993">CVE-2023-1993</a>

    <p>LISP dissector large loop vulnerability</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2023-1994">CVE-2023-1994</a>

    <p>GQUIC dissector crash</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
2.6.20-0+deb10u6.</p>

<p>We recommend that you upgrade your wireshark packages.</p>

<p>For the detailed security status of wireshark please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/wireshark">https://security-tracker.debian.org/tracker/wireshark</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3402.data"
# $Id: $
