<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities have been discovered in Asterisk, an Open
Source Private Branch Exchange. Buffer overflows and other programming errors
could be exploited for launching a denial of service attack or the execution of
arbitrary code.</p>

<p>For Debian 10 buster, these problems have been fixed in version
1:16.28.0~dfsg-0+deb10u2.</p>

<p>We recommend that you upgrade your asterisk packages.</p>

<p>For the detailed security status of asterisk please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/asterisk">https://security-tracker.debian.org/tracker/asterisk</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3335.data"
# $Id: $
