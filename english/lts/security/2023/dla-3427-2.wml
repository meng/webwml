<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that the patch to fix <a href="https://security-tracker.debian.org/tracker/CVE-2023-32700">CVE-2023-32700</a> in texlive-bin, released
as DLA-3427-1, was incomplete and caused an error when running the lualatex
command.</p>

<p>The following security vulnerability has been addressed as well.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-18604">CVE-2019-18604</a>

     <p>A flaw was found in axohelp in axodraw2. The sprintf function is
     mishandled which may cause a stack overflow error.</p></li>

</ul>

<p>For Debian 10 buster, this problem has been fixed in version
2018.20181218.49446-1+deb10u2.</p>

<p>We recommend that you upgrade your texlive-bin packages.</p>

<p>For the detailed security status of texlive-bin please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/texlive-bin">https://security-tracker.debian.org/tracker/texlive-bin</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3427-2.data"
# $Id: $
