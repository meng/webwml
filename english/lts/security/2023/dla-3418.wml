<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>NVIDIA has released a software security update for the NVIDIA GPU Display
Driver R390 linux driver branch. This update addresses issues that may lead to
denial of service, escalation of privileges, information disclosure, data
tampering or undefined behavior.</p>

<p></p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-34670">CVE-2022-34670</a>

    <p>NVIDIA GPU Display Driver for Linux contains a vulnerability in the
    kernel mode layer handler, where an unprivileged regular user can
    cause truncation errors when casting a primitive to a primitive of
    smaller size causes data to be lost in the conversion, which may
    lead to denial of service or information disclosure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-34674">CVE-2022-34674</a>

    <p>NVIDIA GPU Display Driver for Linux contains a vulnerability in the
    kernel mode layer handler, where a helper function maps more
    physical pages than were requested, which may lead to undefined
    behavior or an information leak.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-34675">CVE-2022-34675</a>

    <p>NVIDIA Display Driver for Linux contains a vulnerability in the
    Virtual GPU Manager, where it does not check the return value from a
    null-pointer dereference, which may lead to denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-34677">CVE-2022-34677</a>

    <p>NVIDIA GPU Display Driver for Linux contains a vulnerability in the
    kernel mode layer handler, where an unprivileged regular user can
    cause an integer to be truncated, which may lead to denial of
    service or data tampering.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-34680">CVE-2022-34680</a>

    <p>NVIDIA GPU Display Driver for Linux contains a vulnerability in the
    kernel mode layer handler, where an integer truncation can lead to
    an out-of-bounds read, which may lead to denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42257">CVE-2022-42257</a>

    <p>NVIDIA GPU Display Driver for Linux contains a vulnerability in the
    kernel mode layer (nvidia.ko), where an integer overflow may lead to
    information disclosure, data tampering or denial of service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42258">CVE-2022-42258</a>

    <p>NVIDIA GPU Display Driver for Linux contains a vulnerability in the
    kernel mode layer (nvidia.ko), where an integer overflow may lead to
    denial of service, data tampering, or information disclosure.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-42259">CVE-2022-42259</a>

    <p>NVIDIA GPU Display Driver for Linux contains a vulnerability in the
    kernel mode layer (nvidia.ko), where an integer overflow may lead to
    denial of service.</p></li>

</ul>

<p>For Debian 10 buster, these problems have been fixed in version
390.157-1~deb10u1.</p>

<p>We recommend that you upgrade your nvidia-graphics-drivers-legacy-390xx packages.</p>

<p>For the detailed security status of
nvidia-graphics-drivers-legacy-390xx please refer to its security
tracker page at:
<a href="https://security-tracker.debian.org/tracker/nvidia-graphics-drivers-legacy-390xx">https://security-tracker.debian.org/tracker/nvidia-graphics-drivers-legacy-390xx</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3418.data"
# $Id: $
