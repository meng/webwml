<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple security vulnerabilities have been discovered in netatalk, the
Apple Filing Protocol service, which allow remote attackers to disclose
sensitive information, cause a denial of service or execute arbitrary code.</p>

<p>For Debian 10 buster, these problems have been fixed in version
3.1.12~ds-3+deb10u1.</p>

<p>We recommend that you upgrade your netatalk packages.</p>

<p>For the detailed security status of netatalk please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/netatalk">https://security-tracker.debian.org/tracker/netatalk</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2023/dla-3426.data"
# $Id: $
