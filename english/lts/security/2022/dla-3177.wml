<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there were multiple vulnerabilies in Django, a
popular Python-based development framework:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-28346">CVE-2022-28346</a>:
An issue was discovered in Django 2.2 before 2.2.28, 3.2 before 3.2.13, and 4.0
before 4.0.4. QuerySet.annotate(), aggregate(), and extra() methods are subject
to SQL injection in column aliases via a crafted dictionary (with dictionary
expansion) as the passed **kwargs.</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45115">CVE-2021-45115</a>:
An issue was discovered in Django 2.2 before 2.2.26, 3.2 before 3.2.11, and 4.0
before 4.0.1. UserAttributeSimilarityValidator incurred significant overhead in
evaluating a submitted password that was artificially large in relation to the
comparison values. In a situation where access to user registration was
unrestricted, this provided a potential vector for a denial-of-service
attack.</li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-45116">CVE-2021-45116</a>:
An issue was discovered in Django 2.2 before 2.2.26, 3.2 before 3.2.11, and 4.0
before 4.0.1. Due to leveraging the Django Template Language's variable
resolution logic, the dictsort template filter was potentially vulnerable to
information disclosure, or an unintended method call, if passed a suitably
crafted key.</li>

</ul>

<p>For Debian 10 <q>Buster</q>, these problems have been fixed in version
1:1.11.29-1+deb10u3.</p>

<p>We recommend that you upgrade your python-django packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3177.data"
# $Id: $
