<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a potential arbitrary file read
vulnerability in twig, a PHP templating library. It was caused by insufficient
validation of template names in 'source' and 'include' statements.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2022-39261">CVE-2022-39261</a>

    <p>Twig is a template language for PHP. Versions 1.x prior to 1.44.7, 2.x
    prior to 2.15.3, and 3.x prior to 3.4.3 encounter an issue when the
    filesystem loader loads templates for which the name is a user input. It is
    possible to use the `source` or `include` statement to read arbitrary files
    from outside the templates' directory when using a namespace like
    `@somewhere/../some.file`. In such a case, validation is bypassed. Versions
    1.44.7, 2.15.3, and 3.4.3 contain a fix for validation of such template
    names. There are no known workarounds aside from upgrading.</p></li>

</ul>

<p>For Debian 10 <q>Buster</q>, this problem has been fixed in version
2.6.2-2+deb10u1.</p>

<p>We recommend that you upgrade your twig packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3147.data"
# $Id: $
