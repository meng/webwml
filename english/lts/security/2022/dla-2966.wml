<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>libgc, a conservative garbage collector, is vulnerable to integer
overflows in multiple places. In some cases, when asked to allocate a huge
quantity of memory, instead of failing the request, it will return a
pointer to a small amount of memory possibly tricking the application into
a buffer overwrite.</p>


<p>For Debian 9 stretch, this problem has been fixed in version
1:7.4.2-8+deb9u1.</p>

<p>We recommend that you upgrade your libgc packages.</p>

<p>For the detailed security status of libgc please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libgc">https://security-tracker.debian.org/tracker/libgc</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-2966.data"
# $Id: $
