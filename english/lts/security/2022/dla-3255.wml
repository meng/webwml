<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several issues have been found in mplayer, a movie player for Unix-like
systems.
They are basically related to buffer overflows, divide by zero or out of
bounds read in different parts of the code.</p>


<p>For Debian 10 buster, these problems have been fixed in version
2:1.3.0-8+deb10u1.</p>

<p>We recommend that you upgrade your mplayer packages.</p>

<p>For the detailed security status of mplayer please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/mplayer">https://security-tracker.debian.org/tracker/mplayer</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3255.data"
# $Id: $
