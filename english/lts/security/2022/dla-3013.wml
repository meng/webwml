<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Jakub Wilk discovered a local privilege escalation in needrestart, a
utility to check which daemons need to be restarted after library
upgrades. Regular expressions to detect the Perl, Python, and Ruby
interpreters are not anchored, allowing a local user to escalate
privileges when needrestart tries to detect if interpreters are using
old source files.</p>

<p>For Debian 9 stretch, this problem has been fixed in version
2.11-3+deb9u2.</p>

<p>We recommend that you upgrade your needrestart packages.</p>

<p>For the detailed security status of needrestart please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/needrestart">https://security-tracker.debian.org/tracker/needrestart</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2022/dla-3013.data"
# $Id: $
