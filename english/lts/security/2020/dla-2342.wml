<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Several vulnerabilities were fixed in libjackson-json-java,
a Java JSON processor.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-7525">CVE-2017-7525</a>

    <p>Jackson Deserializer security vulnerability.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15095">CVE-2017-15095</a>

    <p>Block more JDK types from polymorphic deserialization.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-10172">CVE-2019-10172</a>

    <p>XML external entity vulnerabilities.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
1.9.2-8+deb9u1.</p>

<p>We recommend that you upgrade your libjackson-json-java packages.</p>

<p>For the detailed security status of libjackson-json-java please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/libjackson-json-java">https://security-tracker.debian.org/tracker/libjackson-json-java</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2342.data"
# $Id: $
