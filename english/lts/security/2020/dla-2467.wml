<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-19787">CVE-2018-19787</a>

    <p>It was discovered that there was a XSS injection vulnerability in
    the LXML HTML/XSS manipulation library for Python.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-27783">CVE-2020-27783</a>

    <p>javascript escaping through the &lt;noscript&gt; and &lt;style&gt; combinations.</p></li>

</ul>

<p>For Debian 9 stretch, these problems have been fixed in version
3.7.1-1+deb9u1.</p>

<p>We recommend that you upgrade your lxml packages.</p>

<p>For the detailed security status of lxml please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/lxml">https://security-tracker.debian.org/tracker/lxml</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2467.data"
# $Id: $
