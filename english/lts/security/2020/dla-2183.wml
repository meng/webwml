<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>It was discovered that there was a null pointer deference exploit in
libgsf, a I/O abstraction library for GNOME.</p>

<p>An error within the "tar_directory_for_file()" function could be exploited
to trigger a null pointer dereference and subsequently cause a crash via a
crafted TAR file.</p>


<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
1.14.30-2+deb8u1.</p>

<p>We recommend that you upgrade your libgsf packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2183.data"
# $Id: $
