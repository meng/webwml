<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Two issues have been found in golang-1.7, a Go programming language
compiler version 1.7</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-15586">CVE-2020-15586</a>

      <p>Using the 100-continue in HTTP headers received by a net/http/Server
      can lead to a data race involving the connection's buffered writer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-16845">CVE-2020-16845</a>

      <p>Certain invalid inputs to ReadUvarint or ReadVarint could cause those
      functions to read an unlimited number of bytes from the ByteReader
      argument before returning an error.</p>


<p>For Debian 9 stretch, these problems have been fixed in version
1.7.4-2+deb9u2.</p>

<p>We recommend that you upgrade your golang-1.7 packages.</p>

<p>For the detailed security status of golang-1.7 please refer to
its security tracker page at:
<a href="https://security-tracker.debian.org/tracker/golang-1.7">https://security-tracker.debian.org/tracker/golang-1.7</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p></li>

</ul>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2459.data"
# $Id: $
