<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Peter Winter-Smith of NCC Group discovered that libssh, a tiny C SSH
library, contains an authentication bypass vulnerability in the server
code. An attacker can take advantage of this flaw to successfully
authenticate without any credentials by presenting the server an
SSH2_MSG_USERAUTH_SUCCESS message in place of the
SSH2_MSG_USERAUTH_REQUEST message which the server would expect to
initiate authentication.</p>

<p>For Debian 8 <q>Jessie</q>, this problem has been fixed in version
0.6.3-4+deb8u3.</p>

<p>We recommend that you upgrade your libssh packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1548.data"
# $Id: $
