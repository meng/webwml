<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Two vulnerabilities were discovered in wordpress, a web blogging
tool. The Common Vulnerabilities and Exposures project identifies the
following issues.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-5836">CVE-2016-5836</a>

    <p>The oEmbed protocol implementation in WordPress before 4.5.3 allows
    remote attackers to cause a denial of service via unspecified
    vectors.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-12895">CVE-2018-12895</a>

    <p>A vulnerability was discovered in Wordpress, a web blogging tool. It
    allowed remote attackers with specific roles to execute arbitrary
    code.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
4.1+dfsg-1+deb8u18.</p>

<p>We recommend that you upgrade your wordpress packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1452.data"
# $Id: $
