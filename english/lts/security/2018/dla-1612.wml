<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Daniel Axtens discovered a double-free and use-after-free vulnerability
in libarchive's RAR decoder that can result in a denial-of-service
(application crash) or may have other unspecified impact when a
malformed RAR archive is processed.</p>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
3.1.2-11+deb8u6.</p>

<p>We recommend that you upgrade your libarchive packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1612.data"
# $Id: $
