<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Multiple vulnerabilities have been discovered in openjpeg2, the
open-source JPEG 2000 codec.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-6616">CVE-2018-6616</a>

    <p>Excessive iteration in the opj_t1_encode_cblks function (openjp2/t1.c).
    Remote attackers could leverage this vulnerability to cause a denial
    of service via a crafted bmp file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-14423">CVE-2018-14423</a>

    <p>Division-by-zero vulnerabilities in the functions pi_next_pcrl,
    pi_next_cprl, and pi_next_rpcl in (lib/openjp3d/pi.c). Remote attackers
    could leverage this vulnerability to cause a denial of service
    (application crash).</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.1.0-2+deb8u6.</p>

<p>We recommend that you upgrade your openjpeg2 packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1614.data"
# $Id: $
